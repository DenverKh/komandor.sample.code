//
//  CryptoUtils.c
//  Komandor
//
//  Created by Denis Khlopin on 20/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

#include "CryptoUtils.hpp"
#include "Base64.hpp"
#include <sstream>
/// локальная функция конвертирования string to char * с выделением памяти
/// WARNING: need to deallocate it
char * allocateCharsFromString(std::string str) {
    char* output = new char[str.size() + 1];
    std::strcpy(output, str.c_str());
    return output;
}
/// конвертирование времени FILETIME в double
double timeToDouble(FILETIME ft)
{
    int64_t* val = (int64_t*) &ft;
    return static_cast<double>(*val) / 10000000.0 - 11644473600.0;   // epoch is Jan. 1, 1601: 134774 days to Jan. 1, 1970
}
///
/// выводит последнюю ошибку в консоль
void PrintLastError()
{
    DWORD errorMessageID = CSP_GetLastError();
    if (errorMessageID == 0) return; //No error message has been recorded
    LPSTR messageBuffer = NULL;
    CSP_FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                      NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);
    printf("error: %s \n", messageBuffer);
    //Free the buffer.
    LocalFree(messageBuffer);
}
///
ALG_ID GetHashAlgorithm(const char * hashAlgorithmObjId)
{
    if (strcmp(hashAlgorithmObjId, "1.2.643.2.2.3") == 0) {
        return CALG_GR3411;
    }
    if (strcmp(hashAlgorithmObjId, "1.2.643.7.1.1.3.2") == 0) {
        return CALG_GR3411_2012_256;
    }
    if (strcmp(hashAlgorithmObjId, "1.2.643.7.1.1.3.3") == 0) {
        return CALG_GR3411_2012_512;
    }
    return 0;
}
///
ALG_ID GetEphemKeyAlgorithmByCert(const PCCERT_CONTEXT pCertContext)
{
    if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_12_256) == 0)
    {
        return CALG_DH_GR3410_12_256_EPHEM;
    }
    if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_12_512) == 0)
    {
        return CALG_DH_GR3410_12_512_EPHEM;
    }
    return CALG_DH_EL_EPHEM;
}
///
ProvKeyInfo ProvKeyInfoFromB64(const char *keyId)
{
    size_t keySize = strlen(keyId);
    size_t rawSize = 0;
    unsigned char * rawData = base64_decode(keyId, keySize, &rawSize);
    
    ProvKeyInfo returnValue;
    memcpy(&returnValue, rawData, rawSize);
    //printf("ProvKeyInfoFromB64: %d %d", (int)keySize, (int)rawSize);
    return returnValue;
}
///
char * ProvKeyInfoToB64(const ProvKeyInfo pkInfo)
{
    size_t datalen = sizeof(HCRYPTPROV) * 2 + sizeof(DWORD) + pkInfo.IVLen;
    size_t keySize;
    char * key = base64_encode((unsigned char *)&pkInfo, (unsigned int)datalen, &keySize);
    char * result = (char *)malloc(keySize + 1);
    memcpy(result, key, keySize);
    result[keySize] = 0;
    //printf("ProvKeyInfoToB64: %d %d", (int)keySize, (int)datalen);
    return result;
}
/// WARNING: need to deallocate it
char * InvByteToHexStr(void *pvData, DWORD cbData)
{
    BYTE *pb = ((BYTE *)pvData) + cbData;
    char b;
    std::stringstream ss;
    
    for (DWORD i = 0; i < cbData; i++)
    {
        pb--;
        b = (*pb & 0xF0) >> 4;
        ss << ((b <= 9) ? char(b + '0') : char((b - 10) + 'A'));
        b = *pb & 0x0F;
        ss << ((b <= 9) ? char(b + '0') : char((b - 10) + 'A'));
    }
    std::string result = ss.str();
    return allocateCharsFromString(result);
}
/// WARNING: need to deallocate it
char * ByteToHexStr(void *pvData, DWORD cbData)
{
    BYTE *pb = (BYTE *)pvData;
    char b;
    std::stringstream ss;
    
    for (DWORD i = 0; i < cbData; i++)
    {
        b = (*pb & 0xF0) >> 4;
        ss << ((b <= 9) ? char(b + '0') : char((b - 10) + 'A'));
        b = *pb & 0x0F;
        ss << ((b <= 9) ? char(b + '0') : char((b - 10) + 'A'));
        pb++;
    }
    //return ss.str();
    std::string result = ss.str();
    return allocateCharsFromString(result);
}
///
/// WARNING: need to deallocate it
char * GetCertThumbprint(PCCERT_CONTEXT pCertContext)
{
    DWORD dwDataLen = 0;
    void *pData = NULL;
    if (CertGetCertificateContextProperty(pCertContext, CERT_SHA1_HASH_PROP_ID, NULL, &dwDataLen)) {
        pData = malloc(dwDataLen);
        if (CertGetCertificateContextProperty(pCertContext, CERT_SHA1_HASH_PROP_ID, pData, &dwDataLen)) {
            char * result = ByteToHexStr(pData, dwDataLen);
            free(pData);
            return result;
        }
    }
    return NULL;
}
