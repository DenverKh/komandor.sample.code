//
//  Base64.hpp
//  Komandor
//
//  Created by Denis Khlopin on 24/01/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

#ifndef Base64_hpp
#define Base64_hpp

#include "define.h"

#ifdef __cplusplus
extern "C" {
#endif
void build_decoding_table();
char *base64_encode(const unsigned char *data, size_t input_length, size_t *output_length);
char *base64_encode_str(const unsigned char *data, size_t input_length);
unsigned char *base64_decode(const char *data, size_t input_length, size_t *output_length);
#ifdef __cplusplus
}
#endif
#endif /* Base64_hpp */
