//
//  CryptoKeys.cpp
//  Komandor
//
//  Created by Denis Khlopin on 20/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

#include "CryptoKeys.hpp"
#include "CryptoUtils.hpp"
#include "Base64.hpp"
#include <vector>

/*SessionKey CreateSessionKey() {
    SessionKey sessionKey;
    sessionKey.errorCode = 0;
    sessionKey.key = NULL;
    sessionKey.keySize = 0;

}*/

/// создаем ключ
char * cppCreateProvKey() {
    ProvKeyInfo provKey;
    provKey.key = NULL;
    provKey.provider = 0;
    provKey.IVLen = sizeof(provKey.IV);
    // получаем контекст
    if (CryptAcquireContext(&provKey.provider, NULL, NULL, PROV_TYPE, CRYPT_VERIFYCONTEXT | CRYPT_SILENT)) {
        // получаем ключ CALG_G28147
        if (CryptGenKey(provKey.provider, CALG_G28147, CRYPT_EXPORTABLE, &provKey.key)) {
            // получаем длину IV
            if (CryptGetKeyParam(provKey.key, KP_IV, NULL, &provKey.IVLen, 0)) {
                // проверяем длину IV
                if (provKey.IVLen <= sizeof(provKey.IV)) {
                    // генерируем IV
                    if (CryptGenRandom(provKey.provider, provKey.IVLen, provKey.IV)) {
                        if (CryptSetKeyParam(provKey.key, KP_IV, provKey.IV, 0)) {
                            //все отлично, можно возвращать
                            return ProvKeyInfoToB64(provKey);
                        }
                    }
                }
            }
        }
    }
    ///освобождаем provKey
    freeProvKey(provKey);
    return NULL;
}
/// освобождение провайдера из памяти
void freeProvKey(ProvKeyInfo provKey) {
    if (provKey.key) {
        CryptDestroyKey(provKey.key);
    }
    if (provKey.provider) {
        CryptReleaseContext(provKey.provider, 0);
    }
}
void freeProvKeyId(const char * keyId) {
    ProvKeyInfo pkInfo = ProvKeyInfoFromB64(keyId);
    freeProvKey(pkInfo);    
}
void freeSessionKeyData(SessionKeyData sessionKeyData) {
    if (sessionKeyData.responderCertThumbprint) {
        free(sessionKeyData.responderCertThumbprint);
        sessionKeyData.responderCertThumbprint = NULL;
    }
    if (sessionKeyData.sessionKey) {
        free(sessionKeyData.sessionKey);
        sessionKeyData.sessionKey = NULL;
    }
}
/// Экспорт сессионного ключа
SessionKeyData * cppExportSessionKey(const char * keyId, const char * containerName, const char * containerPin, const char * cert) {
    SessionKeyData * keyData = NULL;
    HCRYPTPROV hProvSender = 0;
    HCRYPTKEY hSenderKey = 0;
    HCRYPTPROV hCryptProv = 0;
    HCRYPTKEY hCertPubKey = 0;
    HCRYPTKEY hAgreeKey = 0;
    PCCERT_CONTEXT pCertContext = 0;
    ProvKeyInfo pkInfo;
    size_t certDataSize;
    DWORD errResult = 0;
    unsigned char * certData = NULL;
    if (!keyId) {
        return NULL;
    }
    // получаем инфо о ключе через ID
    pkInfo = ProvKeyInfoFromB64(keyId);
    //if (CryptAcquireContext(&hProvSender, containerName, PROV_NAME, PROV_TYPE, CRYPT_SILENT)) {
    if (CryptAcquireContext(&hProvSender, containerName, PROV_NAME, PROV_TYPE, CRYPT_SILENT)) {
        bool isPinOk = true;
        if (strlen(containerPin) > 0) {
            // Проверка пароля
            if (!CryptSetProvParam(hProvSender, PP_KEYEXCHANGE_PIN, (BYTE *)containerPin, 0)){
                isPinOk = false;
            }
        }
        if(isPinOk) {
            if (CryptGetUserKey(hProvSender, AT_KEYEXCHANGE, &hSenderKey)) {
                if (CryptAcquireContext(&hCryptProv, NULL, PROV_NAME, PROV_TYPE, CRYPT_VERIFYCONTEXT | CRYPT_SILENT)) {
                    //получаем сертификат
                    certData = base64_decode(cert, strlen(cert), &certDataSize);
                    pCertContext = CertCreateCertificateContext(ENCODING_TYPE, (BYTE *)certData, (DWORD)certDataSize);
                    if (pCertContext) {
                        if (CryptImportPublicKeyInfo(hCryptProv, ENCODING_TYPE, &pCertContext->pCertInfo->SubjectPublicKeyInfo, &hCertPubKey)) {
                            BYTE *pbBlob = NULL;
                            DWORD dwBlobLen = 0;
                            if (CryptExportKey(hCertPubKey, 0, PUBLICKEYBLOB, 0, NULL, &dwBlobLen)) {
                                pbBlob = (BYTE *)malloc(dwBlobLen);
                            }
                            if (pbBlob) {
                                if (CryptExportKey(hCertPubKey, 0, PUBLICKEYBLOB, 0, pbBlob, &dwBlobLen)) {
/*
                                    if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410EL)){
                                        printf("szOID_CP_GOST_R3410EL");
                                    } else
                                    if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_12_256)){
                                        printf("szOID_CP_GOST_R3410_12_256");
                                    } else
                                    if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_12_512)){
                                        printf("szOID_CP_GOST_R3410_12_512");
                                    } else
                                    //if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_DH_EX)){
                                    //} else
                                    if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_DH_EL)){
                                        printf("szOID_CP_DH_EL");
                                    } else
                                    if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_DH_12_256)){
                                        printf("szOID_CP_DH_12_256");
                                    } else
                                    if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_DH_12_512)){
                                        printf("szOID_CP_DH_12_512");
                                    } else
                                    //if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_94_ESDH)){
                                    //} else
                                    if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_01_ESDH)){
                                        printf("szOID_CP_GOST_R3410_01_ESDH");
                                    } else
                                    if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_01_ESDH)){
                                        printf("szOID_CP_GOST_R3410_01_ESDH");
                                    }
                                    
                                    pCertContext->pCertInfo->dwVersion;
 */
                                    if (CryptImportKey(hProvSender, pbBlob, dwBlobLen, hSenderKey, 0, &hAgreeKey)) {
                                      //
                                        //NTE_BAD_DATA
                                        free(pbBlob);
                                        pbBlob = NULL;
                                        dwBlobLen = 0;
                                        if (CryptExportKey(pkInfo.key, hAgreeKey, SIMPLEBLOB, 0, NULL, &dwBlobLen)) {
                                            pbBlob = (BYTE *)malloc(dwBlobLen);
                                            if (pbBlob) {
                                                if (CryptExportKey(pkInfo.key, hAgreeKey, SIMPLEBLOB, 0, pbBlob, &dwBlobLen)){
                                                    //
                                                    unsigned int skTrBlobLen = dwBlobLen + pkInfo.IVLen + sizeof(DWORD) * 2;
                                                    BYTE *skTrBlob = (BYTE *)malloc(skTrBlobLen);
                                                    if (skTrBlob) {
                                                        BYTE *pIndex = skTrBlob;
                                                        memcpy(pIndex, &dwBlobLen, sizeof(DWORD));
                                                        pIndex += sizeof(DWORD);
                                                        memcpy(pIndex, pbBlob, dwBlobLen);
                                                        pIndex += dwBlobLen;
                                                        memcpy(pIndex, &pkInfo.IVLen, sizeof(DWORD));
                                                        pIndex += sizeof(DWORD);
                                                        memcpy(pIndex, pkInfo.IV, pkInfo.IVLen);
                                                        // создаем объект
                                                        keyData = new SessionKeyData();
                                                        // формируем структуру
                                                        keyData->responderCertThumbprint = GetCertThumbprint(pCertContext); // some error here?
                                                        keyData->responderCertThumbprintSize = sizeof(keyData->responderCertThumbprint);
                                                        char * key = base64_encode(skTrBlob, skTrBlobLen, &keyData->sessionKeySize);
                                                        keyData->sessionKey = (char *)malloc(keyData->sessionKeySize + 1);
                                                        memcpy(keyData->sessionKey, key, keyData->sessionKeySize);
                                                        keyData->sessionKey[keyData->sessionKeySize] = 0;
                                                        free(key);
                                                        free(skTrBlob);
                                                        skTrBlob = NULL;
                                                    }
                                                }
                                                free(pbBlob);
                                                pbBlob = NULL;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    errResult = CSP_GetLastError();
    printf("%d",errResult);
    PrintLastError();
    if (certData) {
        free(certData);
    }

    if (hCertPubKey) {
        CryptDestroyKey(hCertPubKey);
    }

    if (pCertContext) {
        CertFreeCertificateContext(pCertContext);
    }
    if (hCryptProv) {
        CryptReleaseContext(hCryptProv, 0);
    }
    if (hSenderKey) {
        CryptDestroyKey(hSenderKey);
    }
    if (hProvSender) {
        CryptReleaseContext(hProvSender, 0);
    }
    return keyData;
    
}
/// импорт сессионного ключа
char * cppImportSessionKey(const char * containerName, const char * containerPin, const char * sessionKey, const char * senderCert)
{
    if (!senderCert || !sessionKey) {
        return NULL;
    }
    HCRYPTPROV hProv = 0;
    HCRYPTKEY hResKey = 0;
    HCRYPTPROV hSenderProv = 0;
    size_t senderCertSize = strlen(senderCert);
    size_t certDataSize = 0;
    unsigned char * certData = NULL;
    PCCERT_CONTEXT pCertContext = NULL;
    HCRYPTKEY hCertPubKey = 0;
    BYTE *pbBlob = NULL;
    DWORD dwBlobLen = 0;
    HCRYPTKEY hAgreeKey = 0;
    ProvKeyInfo pkInfo;
    size_t sessionKeySize = strlen(sessionKey);
    size_t sessionDataSize = 0;
    BYTE *tempData = NULL;
    BYTE *pSessionData = NULL;
    DWORD keyBlobLen = 0;
    BYTE *keyBlob = NULL;
    char * keyId = NULL;


    if (CryptAcquireContext(&hProv, containerName, CP_KC1_GR3410_2012_PROV, PROV_GOST_2012_256, CRYPT_SILENT)) {
        bool isPinOk = true;
        if (strlen(containerPin) > 0) {
            // Проверка пароля
            if (!CryptSetProvParam(hProv, PP_KEYEXCHANGE_PIN, (BYTE *)containerPin, 0)) {
                isPinOk = false;
            }
        }
        if(isPinOk) {
            if (CryptGetUserKey(hProv, AT_KEYEXCHANGE, &hResKey)) {
                if (CryptAcquireContext(&hSenderProv, containerName, CP_KC1_GR3410_2012_PROV, PROV_GOST_2012_256, CRYPT_VERIFYCONTEXT | CRYPT_SILENT)) {
                    certData = base64_decode(senderCert, senderCertSize, &certDataSize);
                    pCertContext = CertCreateCertificateContext(ENCODING_TYPE, (BYTE *)certData, (DWORD)certDataSize);
                    if (pCertContext) {
                        if (CryptImportPublicKeyInfo(hSenderProv, ENCODING_TYPE, &pCertContext->pCertInfo->SubjectPublicKeyInfo, &hCertPubKey)) {
                            if (CryptExportKey(hCertPubKey, 0, PUBLICKEYBLOB, 0, NULL, &dwBlobLen)) {
                                pbBlob = (BYTE *)malloc(dwBlobLen);
                                if (pbBlob) {
                                    if (CryptExportKey(hCertPubKey, 0, PUBLICKEYBLOB, 0, pbBlob, &dwBlobLen)) {
                                        if (CryptImportKey(hProv, pbBlob, dwBlobLen, hResKey, 0, &hAgreeKey)) {
                                            if (CryptAcquireContext(&pkInfo.provider, NULL, /*PROV_NAME*/ CP_KC1_GR3410_2012_PROV, PROV_TYPE, CRYPT_VERIFYCONTEXT | CRYPT_SILENT)) {
                                                pSessionData = (BYTE *) base64_decode(sessionKey, sessionKeySize, &sessionDataSize);
                                                tempData = pSessionData;
                                                memcpy(&keyBlobLen, pSessionData, sizeof(DWORD));
                                                pSessionData += sizeof(DWORD);
                                                keyBlob = pSessionData;
                                                pSessionData += keyBlobLen;
                                                if (CryptImportKey(pkInfo.provider, keyBlob, keyBlobLen, hAgreeKey, CRYPT_EXPORTABLE, &pkInfo.key)) {
                                                    memcpy(&pkInfo.IVLen, pSessionData, sizeof(DWORD));
                                                    pSessionData += sizeof(DWORD);
                                                    if (pkInfo.IVLen <= sizeof(pkInfo.IV)) {
                                                        memcpy(&pkInfo.IV, pSessionData, pkInfo.IVLen);
                                                        if (CryptSetKeyParam(pkInfo.key, KP_IV, pkInfo.IV, 0)) {
                                                            keyId = ProvKeyInfoToB64(pkInfo);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    // освобождаем память и дескрипторы
    if (tempData) {
        free(tempData);
    }
    if (hAgreeKey) {
        CryptDestroyKey(hAgreeKey);
    }
    if (pbBlob) {
        free(pbBlob);
    }
    if (hCertPubKey) {
        CryptDestroyKey(hCertPubKey);
    }
    if (pCertContext) {
        CertFreeCertificateContext(pCertContext);
    }
    if (certData) {
        free(certData);
    }
    if (hSenderProv) {
        CryptReleaseContext(hSenderProv, 0);
    }
    if (hResKey) {
        CryptDestroyKey(hResKey);
    }
    if (hProv) {
        CryptReleaseContext(hProv, 0);
    }    
    return keyId;
}
/// Экспорт сессионного ключа
SessionKeyData * cppExportSessionKeyEphem(const char * keyId, const char * containerName, const char * containerPin, const char * cert) {
    SessionKeyData * keyData = NULL;
    HCRYPTPROV hProvSender = 0;
    HCRYPTKEY hSenderKey = 0;
    HCRYPTPROV hCryptProv = 0;
    HCRYPTKEY hCertPubKey = 0;
    HCRYPTKEY hAgreeKey = 0;
    PCCERT_CONTEXT pCertContext = 0;
    ProvKeyInfo pkInfo;
    size_t certDataSize;
    DWORD errResult = 0;
    unsigned char * certData = NULL;
    if (!keyId) {
        return NULL;
    }
    // получаем инфо о ключе через ID
    pkInfo = ProvKeyInfoFromB64(keyId);
    //
    if (CryptAcquireContext(&hProvSender, containerName, PROV_NAME, PROV_TYPE, CRYPT_VERIFYCONTEXT | CRYPT_SILENT)) {
        bool isPinOk = true;
        if (strlen(containerPin) > 0) {
            // Проверка пароля
            if (!CryptSetProvParam(hProvSender, PP_KEYEXCHANGE_PIN, (BYTE *)containerPin, 0)){
                isPinOk = false;
            }
        }
        if(isPinOk) {
            if (CryptGetUserKey(hProvSender, AT_KEYEXCHANGE, &hSenderKey)) {
                if (CryptAcquireContext(&hCryptProv, NULL, PROV_NAME, PROV_TYPE, CRYPT_VERIFYCONTEXT | CRYPT_SILENT)) {
                    //получаем сертификат
                    certData = base64_decode(cert, strlen(cert), &certDataSize);
                    pCertContext = CertCreateCertificateContext(ENCODING_TYPE, (BYTE *)certData, (DWORD)certDataSize);
                    if (pCertContext) {
                        if (CryptImportPublicKeyInfo(hCryptProv, ENCODING_TYPE, &pCertContext->pCertInfo->SubjectPublicKeyInfo, &hCertPubKey)) {
                            BYTE *pbBlob = NULL;
                            DWORD dwBlobLen = 0;
                            if (CryptExportKey(hCertPubKey, 0, PUBLICKEYBLOB, 0, NULL, &dwBlobLen)) {
                                pbBlob = (BYTE *)malloc(dwBlobLen);
                            }
                            if (pbBlob) {
                                if (CryptExportKey(hCertPubKey, 0, PUBLICKEYBLOB, 0, pbBlob, &dwBlobLen)) {
                                    /*
                                     if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410EL)){
                                     printf("szOID_CP_GOST_R3410EL");
                                     } else
                                     if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_12_256)){
                                     printf("szOID_CP_GOST_R3410_12_256");
                                     } else
                                     if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_12_512)){
                                     printf("szOID_CP_GOST_R3410_12_512");
                                     } else
                                     //if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_DH_EX)){
                                     //} else
                                     if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_DH_EL)){
                                     printf("szOID_CP_DH_EL");
                                     } else
                                     if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_DH_12_256)){
                                     printf("szOID_CP_DH_12_256");
                                     } else
                                     if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_DH_12_512)){
                                     printf("szOID_CP_DH_12_512");
                                     } else
                                     //if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_94_ESDH)){
                                     //} else
                                     if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_01_ESDH)){
                                     printf("szOID_CP_GOST_R3410_01_ESDH");
                                     } else
                                     if (strcmp(pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.pszObjId, szOID_CP_GOST_R3410_01_ESDH)){
                                     printf("szOID_CP_GOST_R3410_01_ESDH");
                                     }
                                     
                                     pCertContext->pCertInfo->dwVersion;
                                     */
                                    if (CryptImportKey(hProvSender, pbBlob, dwBlobLen, hSenderKey, 0, &hAgreeKey)) {
                                        //
                                        //NTE_BAD_DATA
                                        free(pbBlob);
                                        pbBlob = NULL;
                                        dwBlobLen = 0;
                                        if (CryptExportKey(pkInfo.key, hAgreeKey, SIMPLEBLOB, 0, NULL, &dwBlobLen)) {
                                            pbBlob = (BYTE *)malloc(dwBlobLen);
                                            if (pbBlob) {
                                                if (CryptExportKey(pkInfo.key, hAgreeKey, SIMPLEBLOB, 0, pbBlob, &dwBlobLen)){
                                                    //
                                                    unsigned int skTrBlobLen = dwBlobLen + pkInfo.IVLen + sizeof(DWORD) * 2;
                                                    BYTE *skTrBlob = (BYTE *)malloc(skTrBlobLen);
                                                    if (skTrBlob) {
                                                        BYTE *pIndex = skTrBlob;
                                                        memcpy(pIndex, &dwBlobLen, sizeof(DWORD));
                                                        pIndex += sizeof(DWORD);
                                                        memcpy(pIndex, pbBlob, dwBlobLen);
                                                        pIndex += dwBlobLen;
                                                        memcpy(pIndex, &pkInfo.IVLen, sizeof(DWORD));
                                                        pIndex += sizeof(DWORD);
                                                        memcpy(pIndex, pkInfo.IV, pkInfo.IVLen);
                                                        // создаем объект
                                                        keyData = new SessionKeyData();
                                                        // формируем структуру
                                                        keyData->responderCertThumbprint = GetCertThumbprint(pCertContext); // some error here?
                                                        keyData->responderCertThumbprintSize = sizeof(keyData->responderCertThumbprint);
                                                        char * key = base64_encode(skTrBlob, skTrBlobLen, &keyData->sessionKeySize);
                                                        keyData->sessionKey = (char *)malloc(keyData->sessionKeySize + 1);
                                                        memcpy(keyData->sessionKey, key, keyData->sessionKeySize);
                                                        keyData->sessionKey[keyData->sessionKeySize] = 0;
                                                        free(key);
                                                        free(skTrBlob);
                                                        skTrBlob = NULL;
                                                    }
                                                }
                                                free(pbBlob);
                                                pbBlob = NULL;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    errResult = CSP_GetLastError();
    printf("%d",errResult);
    PrintLastError();
    if (certData) {
        free(certData);
    }
    
    if (hCertPubKey) {
        CryptDestroyKey(hCertPubKey);
    }
    
    if (pCertContext) {
        CertFreeCertificateContext(pCertContext);
    }
    if (hCryptProv) {
        CryptReleaseContext(hCryptProv, 0);
    }
    if (hSenderKey) {
        CryptDestroyKey(hSenderKey);
    }
    if (hProvSender) {
        CryptReleaseContext(hProvSender, 0);
    }
    return keyData;
    
}

void freeKeyData(SessionKeyData * keyData) {
    if (keyData) {
        if (keyData->responderCertThumbprint) {
            free(keyData->responderCertThumbprint);
            keyData->responderCertThumbprint = NULL;
            keyData->responderCertThumbprintSize = 0;
        }
        if (keyData->sessionKey) {
            free(keyData->sessionKey);
            keyData->sessionKey = NULL;
            keyData->sessionKeySize = 0;
        }
        delete keyData;
    }
}
///  шифрование
char * cppEncrypt(const char * keyId, const char *b64Data) {
    ProvKeyInfo pkInfo = ProvKeyInfoFromB64(keyId);
    char * result = NULL;
    HCRYPTKEY hDuplicateKey = 0;
    unsigned char * data = NULL;
    size_t dataSize = 0;
    DWORD dwDataLen;
    BYTE *encData = NULL;
    if (CryptDuplicateKey(pkInfo.key, NULL, 0, &hDuplicateKey)) {
        if (CryptSetKeyParam(hDuplicateKey, KP_IV, pkInfo.IV, 0)) {
            data = base64_decode(b64Data, strlen(b64Data), &dataSize);
            dwDataLen = (DWORD)dataSize;
            if (CryptEncrypt(hDuplicateKey, NULL, TRUE, 0, NULL, &dwDataLen, (DWORD)dataSize)) {
                encData = (BYTE *)malloc(dwDataLen);
                if (encData) {
                    memcpy(encData, data, dataSize);
                    if (CryptEncrypt(hDuplicateKey, NULL, TRUE, 0, encData, &dwDataLen, (DWORD)dataSize)) {
                        result = base64_encode_str(encData, dwDataLen);
                    }
                }
            }
        }
    }
    if (hDuplicateKey) {
        CryptDestroyKey(hDuplicateKey);
    }
    if(data) {
        free(data);
    }
    if(encData) {
        free(encData);
    }
    return result;
}
///  расшифровка
char * cppDecrypt(const char *keyId, const char *b64Data) {
    ProvKeyInfo pkInfo = ProvKeyInfoFromB64(keyId);
    char * result = NULL;
    HCRYPTKEY hDuplicateKey = 0;
    unsigned char * data = NULL;
    size_t dataSize = 0;
    DWORD dwDataLen;
    BYTE *decData = NULL;
    if (CryptDuplicateKey(pkInfo.key, NULL, 0, &hDuplicateKey)) {
        if (CryptSetKeyParam(hDuplicateKey, KP_IV, pkInfo.IV, 0)) {
            data = base64_decode(b64Data, strlen(b64Data), &dataSize);
            dwDataLen = (DWORD)dataSize;
            decData = (BYTE *)malloc(dwDataLen);
            if (decData) {
                memcpy(decData, data, dataSize);
                if (CryptDecrypt(hDuplicateKey, NULL, TRUE, 0, decData, &dwDataLen)) {
                    result = base64_encode_str(decData, dwDataLen);
                }
            }
        }
    }
    if (hDuplicateKey) {
        CryptDestroyKey(hDuplicateKey);
    }
    if(data) {
        free(data);
    }
    if(decData) {
        free(decData);
    }
    return result;
}
