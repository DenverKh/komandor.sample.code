//
//  CryptoKeys.hpp
//  Komandor
//
//  Created by Denis Khlopin on 20/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

#ifndef CryptoKeys_hpp
#define CryptoKeys_hpp

#include "define.h"
#ifdef __cplusplus
extern "C" {
#endif
    
    void freeProvKey(ProvKeyInfo  provKey);
    void freeProvKeyId(const char * keyId);
    char * cppCreateProvKey();
    SessionKeyData * cppExportSessionKey(const char * keyId, const char * containerName, const char * containerPin, const char * cert);
    char * cppImportSessionKey(const char * containerName, const char * containerPin, const char * sessionKey, const char * senderCert);
    void freeKeyData(SessionKeyData * keyData);
    void freeSessionKeyData(SessionKeyData sessionKeyData);
    char * cppEncrypt(const char * keyId, const char *b64Data);
    char * cppDecrypt(const char *keyId, const char *b64Data);
    
#ifdef __cplusplus
}
#endif
#endif /* CryptoKeys_hpp */
