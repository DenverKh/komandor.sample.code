//
//  CryptoEphemKeys.hpp
//  Komandor
//
//  Created by Denis Khlopin on 30/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

#ifndef CryptoEphemKeys_hpp
#define CryptoEphemKeys_hpp

#include "define.h"
#ifdef __cplusplus
extern "C" {
#endif
    SessionKeyData * cppExportSessionKeyEphem(const char *keyId, const char * cert);
    char * cppImportSessionKeyEphem(const char * containerName, const char * containerPin, const char * sessionKey);
#ifdef __cplusplus
}
#endif
#endif /* CryptoEphemKeys_hpp */
