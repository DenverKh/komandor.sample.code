//
//  Structures.h
//  Komandor
//
//  Created by Denis Khlopin on 20/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

#ifndef Structures_h
#define Structures_h

typedef struct CPP_STRING {
    char * value;
    size_t size;
} CppString;

// возврата простой подписи и хэша
typedef struct SIMPLE_SIGN {
    char *b64Sign;
    size_t b64SignSize;
    char *b64Hash;
    size_t b64HashSize;
    DWORD errorCode;
} SimpleSign;
// возврат ключа сессии
typedef struct SESSION_KEY {
    char *key;
    size_t keySize;
    DWORD errorCode;
} SessionKey;
//
typedef struct PROV_KEY_INFO {
    HCRYPTPROV provider;
    HCRYPTKEY key;
    DWORD IVLen;
    BYTE IV[256];
} ProvKeyInfo;

typedef struct SESSION_KEY_DATA {
    char * responderCertThumbprint;
    size_t responderCertThumbprintSize;
    char * sessionKey;
    size_t sessionKeySize;
} SessionKeyData;
#endif /* Structures_h */

/*
 struct CertExtensionInfo
 {
 std::string Identifier;
 std::string Name;
 std::string Value;
 };
 
 struct AlgorithmInfo
 {
 std::string Name;
 std::string Identifier;
 std::string Param;
 };
 
 struct PublicKeyInfo
 {
 AlgorithmInfo PublicKeyAlgorithm;
 std::string Value;
 };
 
 struct CertData
 {
 std::vector<std::string> InterndedFor;
 
 std::string IssuedTo;
 std::string IssuedBy;
 std::string ValidFrom;
 std::string ValidTo;
 std::string Version;
 std::string SerialNumber;
 
 AlgorithmInfo SignatureAlgorithm;
 
 std::string Issuer;
 std::string Subject;
 
 PublicKeyInfo PublicKey;
 
 std::vector<CertExtensionInfo> CertExtensions;
 
 std::string B64Certificate;
 
 std::string Thumbprint;
 };
 
 struct KeyInfo
 {
 bool KeyPresent;
 int KeySpec;
 int KeyLength;
 bool HasCertificate;
 CertData CertificateInfo;
 };
 
 struct ContainerInfo
 {
 std::string Name;
 std::string UniqueName;
 std::string FQCN;
 std::vector<AlgorithmInfo> SupportedAlgorithms;
 KeyInfo ExchangeKeyInfo;
 KeyInfo SignatureKeyInfo;
 };
 
 struct ProvKeyInfo
 {
 HCRYPTPROV provider;
 HCRYPTKEY key;
 DWORD IVLen;
 BYTE IV[256];
 };
 

 */
