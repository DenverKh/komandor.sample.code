//
//  CryptoEphemKeys.cpp
//  Komandor
//
//  Created by Denis Khlopin on 30/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

#include "CryptoEphemKeys.hpp"
#include "CryptoUtils.hpp"
#include "Base64.hpp"

SessionKeyData * cppExportSessionKeyEphem(const char *keyId, const char * cert) {
    //
    SessionKeyData * keyData = NULL;
    ProvKeyInfo pkInfo = ProvKeyInfoFromB64(keyId);
    HCRYPTPROV hCryptProv = 0;
    size_t certDataSize = 0;
    unsigned char * certData = NULL;
    HCRYPTKEY hCertPubKey = 0;
    BYTE *pbBlob = NULL;
    DWORD dwBlobLen = 0;
    HCRYPTKEY hEphemKey = 0;
    BYTE *pbEphemKeyBlob = NULL;
    DWORD dwEphemKeyBlobLen = 0;
    HCRYPTKEY hAgreeKey = 0;
    BYTE *pbSessionKeyBlob = NULL;
    DWORD dwSessionKeyBlobLen = 0;
    unsigned int skTrBlobLen = 0;
    BYTE *skTrBlob = NULL;
    PCCERT_CONTEXT pCertContext = NULL;
    //
    if (CryptAcquireContext(&hCryptProv, NULL, PROV_NAME, PROV_TYPE, CRYPT_VERIFYCONTEXT | CRYPT_SILENT)) {
        certData = base64_decode(cert, strlen(cert), &certDataSize);
        pCertContext = CertCreateCertificateContext(ENCODING_TYPE, (BYTE *)certData, (DWORD)certDataSize);
        if (pCertContext) {
            if (CryptImportPublicKeyInfo(hCryptProv, ENCODING_TYPE, &pCertContext->pCertInfo->SubjectPublicKeyInfo, &hCertPubKey)) {
                //1) получаем локальный публичный ключ
                // получаем размер блоб буфера
                if (CryptExportKey(hCertPubKey, 0, PUBLICKEYBLOB, 0, NULL, &dwBlobLen)) {
                    pbBlob = (BYTE *)malloc(dwBlobLen);
                    // получаем блоб
                    if (!CryptExportKey(hCertPubKey, 0, PUBLICKEYBLOB, 0, pbBlob, &dwBlobLen)) {
                        if(pbBlob) {
                            free(pbBlob);
                            pbBlob = NULL;
                        }
                    }
                }
                // если блоб получен успешно
                //2) создаем и получаем эфемерный ключ
                if(pbBlob) { // начинаем работать с эфемерным ключом
                    // генерируем новый эфемерный ключ
                    if (CryptGenKey(hCryptProv, GetEphemKeyAlgorithmByCert(pCertContext), CRYPT_EXPORTABLE, &hEphemKey)) {
                        // получаем длину блоба
                        if (CryptExportKey(hEphemKey, 0, PUBLICKEYBLOB, 0, NULL, &dwEphemKeyBlobLen)) {
                            pbEphemKeyBlob = (BYTE *)malloc(dwEphemKeyBlobLen);
                            if (pbEphemKeyBlob) {
                                // получаем данные блоба эфемерного ключа
                                if (!CryptExportKey(hEphemKey, 0, PUBLICKEYBLOB, 0, pbEphemKeyBlob, &dwEphemKeyBlobLen)) {
                                    // если ошибка - удаляем выделенное пространство
                                    free(pbEphemKeyBlob);
                                    pbEphemKeyBlob = NULL;
                                }
                            }
                        }
                    }
                }
                // если успешно получили получили блоб эфемерного ключа
                //3) получаем сессионный ключ
                if (pbEphemKeyBlob) {
                    // получаем импортированный ключ согласования ()
                    if (CryptImportKey(hCryptProv, pbBlob, dwBlobLen, hEphemKey, 0, &hAgreeKey)) {
                        // начинаем получать блоб сессионного ключа, сначала размер
                        if (CryptExportKey(pkInfo.key, hAgreeKey, SIMPLEBLOB, 0, NULL, &dwSessionKeyBlobLen)) {
                            pbSessionKeyBlob = (BYTE *)malloc(dwSessionKeyBlobLen);
                            if (pbSessionKeyBlob) {
                                if (!CryptExportKey(pkInfo.key, hAgreeKey, SIMPLEBLOB, 0, pbSessionKeyBlob, &dwSessionKeyBlobLen)){
                                    free(pbSessionKeyBlob);
                                    pbSessionKeyBlob = NULL;
                                }
                            }
                        }
                    }
                }
                // если сессионный ключ получили и все остальное тоже ок
                // генерируем транспортный ключ и блоб для него
                if (pbSessionKeyBlob) {
                    skTrBlobLen = dwSessionKeyBlobLen + dwEphemKeyBlobLen + pkInfo.IVLen + sizeof(DWORD) * 3;
                    skTrBlob = (BYTE *)malloc(skTrBlobLen);
                }
                // заполняем транспортный ключ
                if (skTrBlob) {
                    BYTE *pIndex = skTrBlob;
                    //
                    memcpy(pIndex, &dwEphemKeyBlobLen, sizeof(DWORD));
                    pIndex += sizeof(DWORD);
                    memcpy(pIndex, pbEphemKeyBlob, dwEphemKeyBlobLen);
                    pIndex += dwEphemKeyBlobLen;
                    //
                    memcpy(pIndex, &dwSessionKeyBlobLen, sizeof(DWORD));
                    pIndex += sizeof(DWORD);
                    memcpy(pIndex, pbSessionKeyBlob, dwSessionKeyBlobLen);
                    pIndex += dwSessionKeyBlobLen;
                    //
                    memcpy(pIndex, &pkInfo.IVLen, sizeof(DWORD));
                    pIndex += sizeof(DWORD);
                    memcpy(pIndex, pkInfo.IV, pkInfo.IVLen);
                    //
                    keyData = new SessionKeyData();
                    keyData->responderCertThumbprint = GetCertThumbprint(pCertContext);
                    keyData->sessionKey = base64_encode_str(skTrBlob, skTrBlobLen); //base64_encode(skTrBlob, skTrBlobLen);
                }
            }
        }
    }
//HProvGuard provGuard(hCryptProv);
//CertContextGuard certGuard(pCertContext);
//HKeyGuard certPubKeyGuard(hCertPubKey);
//MallocGuard pubKeyGuard(pbBlob);
//HKeyGuard hEphemKeyGuard(hEphemKey);
//MallocGuard pbEphemKeyGuard(pbEphemKeyBlob);
//MallocGuard sessionKeyGuard(pbSessionKeyBlob);
//MallocGuard sessionKeyTransportGuard(skTrBlob);
    if (hCertPubKey) {
        CryptDestroyKey(hCertPubKey);
    }
    if (hEphemKey) {
        CryptDestroyKey(hEphemKey);
    }
    if (pCertContext) {
        CertFreeCertificateContext(pCertContext);
    }
    if (hCryptProv) {
        CryptReleaseContext(hCryptProv, 0);
    }
    if (skTrBlob) {
        free(skTrBlob);
    }
    if (pbSessionKeyBlob) {
        free(pbSessionKeyBlob);
    }
    if (pbEphemKeyBlob) {
        free(pbEphemKeyBlob);
    }
    if (certData) {
        free(certData);
    }
    return keyData;
}
///
char * cppImportSessionKeyEphem(const char * containerName, const char * containerPin, const char * sessionKey) {
    HCRYPTPROV hProv = 0;
    bool isPinOk = false;
    HCRYPTKEY hResKey = 0;
    size_t sessionDataSize = 0;
    unsigned char * sessionData = NULL;
    BYTE *pData = NULL;
    DWORD dwEphemKeyBlobLen= 0;
    BYTE *pbEphemKeyBlob = NULL;
    HCRYPTKEY hAgreeKey = 0;
    ProvKeyInfo pkInfo;
    DWORD keyBlobLen = 0;
    BYTE *keyBlob = NULL;
    char *keyId = NULL;
    //
    if (CryptAcquireContextA(&hProv, containerName, PROV_NAME, PROV_TYPE, CRYPT_SILENT)){
        isPinOk = true;
        if (strlen(containerPin) > 0) {
            if (!CryptSetProvParam(hProv, PP_KEYEXCHANGE_PIN, (BYTE *)containerPin, 0)){
                isPinOk = false;
            }
        }
    }
    //
    if (isPinOk) {
        if (CryptGetUserKey(hProv, AT_KEYEXCHANGE, &hResKey)) {
            sessionData = base64_decode(sessionKey, strlen(sessionKey), &sessionDataSize);
            pData = (BYTE *) sessionData;
        }
    }
    
    if (pData) {
        // получаем блоб эфемерного ключа
        memcpy(&dwEphemKeyBlobLen, pData, sizeof(DWORD));
        pData += sizeof(DWORD);
        pbEphemKeyBlob = pData;
        pData += dwEphemKeyBlobLen;
        // получаем ключ согласования
        if (CryptImportKey(hProv, pbEphemKeyBlob, dwEphemKeyBlobLen, hResKey, 0, &hAgreeKey)) {
            if (CryptAcquireContext(&pkInfo.provider, NULL, PROV_NAME, PROV_TYPE, CRYPT_VERIFYCONTEXT | CRYPT_SILENT)) {
                // получаем блоб ключа
                memcpy(&keyBlobLen, pData, sizeof(DWORD));
                pData += sizeof(DWORD);
                keyBlob = pData;
                pData += keyBlobLen;
                
                if (CryptImportKey(pkInfo.provider, keyBlob, keyBlobLen, hAgreeKey, CRYPT_EXPORTABLE, &pkInfo.key)) {
                    memcpy(&pkInfo.IVLen, pData, sizeof(DWORD));
                    pData += sizeof(DWORD);
                    if (pkInfo.IVLen <= sizeof(pkInfo.IV)) {
                        memcpy(&pkInfo.IV, pData, pkInfo.IVLen);
//
                        if (CryptSetKeyParam(pkInfo.key, KP_IV, pkInfo.IV, 0)) {
                            keyId = ProvKeyInfoToB64(pkInfo);
                        }
                    }
                }
            }
        }
    }
    
    if (hResKey) {
        CryptDestroyKey(hResKey);
    }
    if (hAgreeKey) {
        CryptDestroyKey(hAgreeKey);
    }
    if (hProv) {
        CryptReleaseContext(hProv, 0);
    }
    if (sessionData) {
        free(sessionData);
    }
    if (pbEphemKeyBlob) {
        free(pbEphemKeyBlob);
    }
    if (keyBlob) {
        free(keyBlob);
    }
    return keyId;
}
