//
//  Crypto.hpp
//  CryptoProNew
//
//  Created by Denis Khlopin on 15/01/2019.
//  Copyright © 2019 Denis Khlopin. All rights reserved.
//

#ifndef Crypto_hpp
#define Crypto_hpp
#include "define.h"

#ifdef __cplusplus
extern "C" {
#endif
    // MARK: export functions here
    HCRYPTPROV cppCryptoGetContext(char * containerName);
    void cppCryptoFreeContext(HCRYPTPROV hProv);
    DWORD cppCryptoGetNextContainerSize(HCRYPTPROV hProv, DWORD dwFlags );
    DWORD cppCryptoGetNextContainerName(HCRYPTPROV hProv, DWORD dwFlags, DWORD pdwDataLen, BYTE * containerName);
    DWORD cppCryptoContainerGetSize(HCRYPTPROV hProv, DWORD dwParam, DWORD dwFlags );
    DWORD cppCryptoContainerGetParam(HCRYPTPROV hProv, DWORD dwParam, DWORD dwFlags, DWORD pdwDataLen, BYTE * paramValue);
    HCRYPTKEY cppCryptoGetUserKeyExchange(HCRYPTPROV hProv);
    HCRYPTKEY cppCryptoGetUserKeySignature(HCRYPTPROV hProv);
    DWORD cppCryptoUserKeyGetSize(HCRYPTKEY hKey, DWORD dwParam, DWORD dwFlags );
    DWORD cppCryptoUserKeyGetParam(HCRYPTKEY hKey, DWORD dwParam, DWORD dwFlags, DWORD pdwDataLen, BYTE * paramValue);
    PCCERT_CONTEXT cppCryptoCreateCertificateContextFromKey(HCRYPTKEY hKey);
    void cppCryptoFreeCertificateContext(PCCERT_CONTEXT pCertContext);
    DWORD cppCryptoGetCertificateBlobInfo(HCRYPTKEY hKey, OUT char * pInfo, DWORD dwInfo);
    bool cppCryptoChangePassword(const char * containerName, const char * password, const char * newPassword);
    bool cppCryptoCheckPassword(const char * containerName, const char * password);
    CSP_BOOL cppCryptoHash(HCRYPTPROV  hProv, BYTE *pbData, DWORD dwDataLen, BYTE *pbHashData, DWORD *dwHashDataLen);
    bool cppCryptoCheckPin(const char * containerName);
    bool cppCryptoSetPin(const char * containerName, const char * password);
    bool cppCryptoVerifyCertificate(BYTE *certData, DWORD certSize);
    void cppCryptoFreeSimpleSign(struct SIMPLE_SIGN simpleSign);
    struct SIMPLE_SIGN cppCryptoSimpleSign(const char *containerName, const char *containerPin, const char *hashAlgorithmObjId, const char *b64Data);
    bool cppCryptoVerifySimpleSign(const char *b64Certificate, const char *hashAlgorithmObjId, const char *b64Data, const char *b64Sign);
#ifdef __cplusplus
}
#endif

#endif /* Crypto_hpp */
