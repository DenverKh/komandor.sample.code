//
//  Crypto.cpp
//  CryptoProNew
//
//  Created by Denis Khlopin on 15/01/2019.
//  Copyright © 2019 Denis Khlopin. All rights reserved.
//
extern bool USE_CACHE_DIR;
bool USE_CACHE_DIR = true;

#define LEGACY_FORMAT_MESSAGE_IMPL

#include "Crypto.hpp"
#include "Base64.hpp"
#include "CryptoUtils.hpp"
/// получение дескриптора контейнера
/// если containerName = NULL - получим хэндл для перечисления всех контейнеров
/// если containerName = имяКонтейнера, то получаем хэндл контейнера
HCRYPTPROV cppCryptoGetContext(char * containerName = NULL) {
    HCRYPTPROV  hProv           = 0;
    if ( CryptAcquireContext(&hProv, containerName, CP_KC1_GR3410_2012_PROV, PROV_GOST_2012_256, 1 ) ) {
        return hProv;
    }
    return 0;
}
void cppCryptoFreeContext(HCRYPTPROV hProv) {
    CryptReleaseContext(hProv, 0);
}
/// получаем размер данных для имени контейнера
DWORD cppCryptoGetNextContainerSize(HCRYPTPROV hProv, DWORD dwFlags ) {
    DWORD dwParam = PP_ENUMCONTAINERS;
    //| CRYPT_FQCN
    return cppCryptoContainerGetSize(hProv, dwParam, dwFlags | CRYPT_UNIQUE);
}
/// получаем имя контейнера
DWORD cppCryptoGetNextContainerName(HCRYPTPROV hProv, DWORD dwFlags, DWORD pdwDataLen, BYTE * containerName) {
    DWORD dwParam = PP_ENUMCONTAINERS;
    return cppCryptoContainerGetParam(hProv, dwParam, dwFlags | CRYPT_UNIQUE, pdwDataLen, containerName);
}
/// получение размера параметра
DWORD cppCryptoContainerGetSize(HCRYPTPROV hProv, DWORD dwParam, DWORD dwFlags ) {
    DWORD pdwDataLen;
    if (CryptGetProvParam(hProv, dwParam, NULL, &pdwDataLen, dwFlags)) {
        return pdwDataLen;
    }
    return 0;
}
/// получение текстового параметра для контейнера
/// возвращает 0 если все ок и код ошибки если все плохо)
DWORD cppCryptoContainerGetParam(HCRYPTPROV hProv, DWORD dwParam, DWORD dwFlags, DWORD pdwDataLen, BYTE * paramValue) {
    if (CryptGetProvParam(hProv, dwParam, paramValue, &pdwDataLen, dwFlags)) {
        return 0;
    }
    return CSP_GetLastError();
}
/// получения хэндла для пары публичный/приватный ключ
HCRYPTKEY cppCryptoGetUserKeyExchange(HCRYPTPROV hProv) {
    HCRYPTKEY phUserKey;
    if ( CryptGetUserKey(hProv, AT_KEYEXCHANGE, &phUserKey) ) {
        return phUserKey;
    }
    return 0;    
}
/// получения хэндла для пары публичного ключа(только для подписи документов)
HCRYPTKEY cppCryptoGetUserKeySignature(HCRYPTPROV hProv) {
    HCRYPTKEY phUserKey;
    if ( CryptGetUserKey(hProv, AT_SIGNATURE, &phUserKey) ) {
        return phUserKey;
    }
    return 0;
}
/// получение размера длины параметра ключа
/// возвращает необходимую длину для параметра
DWORD cppCryptoUserKeyGetSize(HCRYPTKEY hKey, DWORD dwParam, DWORD dwFlags ) {
    DWORD pdwDataLen;
    if (CryptGetKeyParam(hKey, dwParam, NULL, &pdwDataLen, dwFlags)) {
        return pdwDataLen;
    }
    return 0;
}
/// получение параметра ключа
/// возвращает 0 если все ок и код ошибки если все плохо)
DWORD cppCryptoUserKeyGetParam(HCRYPTKEY hKey, DWORD dwParam, DWORD dwFlags, DWORD pdwDataLen, BYTE * paramValue) {
    if (CryptGetKeyParam(hKey, dwParam, paramValue, &pdwDataLen, dwFlags)) {
        return 0;
    }
    return CSP_GetLastError();
}
/// получение контекста сертификата
/// !ВАЖНО! после вызова этой функции освободить память вызовом cppCryptoFreeCertificateContext
/// получив PCCERT_CONTEXT - можно получить информацию о сертификате
PCCERT_CONTEXT cppCryptoCreateCertificateContextFromKey(HCRYPTKEY hKey) {
    DWORD pdwDataLen;
    PCCERT_CONTEXT pCertContext;
    if (CryptGetKeyParam(hKey, KP_CERTIFICATE, NULL, &pdwDataLen, 0)) {
        BYTE * pbCertificate = new BYTE [pdwDataLen];
        CryptGetKeyParam(hKey, KP_CERTIFICATE, pbCertificate, &pdwDataLen, 0);
        pCertContext = CertCreateCertificateContext(X509_ASN_ENCODING, pbCertificate, pdwDataLen);        
        return pCertContext;
    }
    return NULL;
}
/// освобождение памяти контекста сертификата
void cppCryptoFreeCertificateContext(PCCERT_CONTEXT pCertContext) {
    CertFreeCertificateContext(pCertContext);
}
/// получение информации о сертификате в расшифрованном виде
/// если pInfo = NULL то вернется размер данных, необходимый для дальнейшей загрузки данных
DWORD cppCryptoGetCertificateBlobInfo(HCRYPTKEY hKey, OUT char * pInfo, DWORD dwInfo) {
    bool isSizeRequest = false;
    if (pInfo == NULL) {
        isSizeRequest = true;
    }
    DWORD pdwDataLen;
    PCCERT_CONTEXT pCertContext;
    if (CryptGetKeyParam(hKey, KP_CERTIFICATE, NULL, &pdwDataLen, 0)) {
        BYTE * pbCertificate = new BYTE [pdwDataLen];
        CryptGetKeyParam(hKey, KP_CERTIFICATE, pbCertificate, &pdwDataLen, 0);
        pCertContext = CertCreateCertificateContext(X509_ASN_ENCODING, pbCertificate, pdwDataLen);
        _CRYPTOAPI_BLOB *blob = NULL;
        switch (dwInfo) {
            case CD_ISSUER: // информация о выдавшем органе
                blob = &pCertContext->pCertInfo->Issuer;
                break;
            case CD_SUBJECT: // информация о хозяине сертификата
                blob = &pCertContext->pCertInfo->Subject;
                break;
            case CD_SIGNATURE_ALGO_PARAMETERS:
                blob = &pCertContext->pCertInfo->SignatureAlgorithm.Parameters;
                break;
            case CD_SUBJECT_PUBLIC_ALGO_PARAMETERS:
                blob = &pCertContext->pCertInfo->SubjectPublicKeyInfo.Algorithm.Parameters;
                break;
            case CD_EXTENTION_VALUE:
                blob = &pCertContext->pCertInfo->rgExtension->Value;
                break;
            case CD_SERIAL_NUMBER:
                blob = &pCertContext->pCertInfo->SerialNumber;
            break;        }
        DWORD cbSize = CertNameToStr(
                                     pCertContext->dwCertEncodingType,
                                     blob,
                                     MY_NAME_LIST,
                                     NULL,
                                     0);
        if (!isSizeRequest) {
            CertNameToStr(
                          pCertContext->dwCertEncodingType,
                          blob,
                          MY_NAME_LIST,
                          pInfo,
                          cbSize);
        }
        // free certificate
        CertFreeCertificateContext(pCertContext);
        return cbSize;
    }
    return 0;
}
/// проверка валидности пин кода
bool cppCryptoCheckPin(const char * containerName) {
    bool result = true;
    HCRYPTPROV  hProv = 0;
    if( !CryptAcquireContext(&hProv, containerName, CP_KC1_GR3410_2012_PROV, PROV_GOST_2012_256, 1 | CRYPT_SILENT) ) {
        return false;
    }
    DWORD dwHashDataLen;
    BYTE *pbHashData;
    if (cppCryptoHash(hProv, (BYTE *)"test", 4, NULL, &dwHashDataLen)) {
        pbHashData = new BYTE[dwHashDataLen];
        if(!cppCryptoHash(hProv, (BYTE *)"test", 4, pbHashData, &dwHashDataLen)){
            DWORD lastError = CSP_GetLastError();
            PrintLastError();
            if (SCARD_W_WRONG_CHV == lastError ){
                result = false;
            }
        }
        // освобождаем хэш
        delete [] pbHashData;
    } else {
        result = false;
    }
    if (!result) {
        // сбрасываем пин, если он не верен
        CryptSetProvParam(hProv, PP_KEYEXCHANGE_PIN, NULL, 0);
    }
    // освобождаем контекст
    cppCryptoFreeContext(hProv);
    return result;
}
/// ввод пин кода
bool cppCryptoSetPin(const char * containerName, const char * password) {
    HCRYPTPROV  hProv = 0;
    if( !CryptAcquireContext(&hProv, containerName, CP_KC1_GR3410_2012_PROV, PROV_GOST_2012_256, 1 | CRYPT_SILENT) ) {
        return false;
    }
    if (!CryptSetProvParam(hProv, PP_KEYEXCHANGE_PIN, (BYTE *)password, 0)) {
        PrintLastError();
        return false;
    }
    cppCryptoFreeContext(hProv);
    return true;
}
/// проверка пароля контейнера
bool cppCryptoChangePassword(const char * containerName, const char * password, const char * newPassword ) {
    HCRYPTPROV  hProv = 0;
    if( !CryptAcquireContext(&hProv, containerName, CP_KC1_GR3410_2012_PROV, PROV_GOST_2012_256, 1 | CRYPT_SILENT) ) {
        return false;
    }
    /// вводим текущий пин
    if (!CryptSetProvParam( hProv, PP_KEYEXCHANGE_PIN, (BYTE *)password, 0)) {
        return false;
    }
    CRYPT_PIN_PARAM param;
    param.type = CRYPT_PIN_PASSWD;
    param.dest.passwd = (char *)newPassword;
    if( CryptSetProvParam(hProv, PP_CHANGE_PIN, (BYTE*)&param, 0)) {
        return false;
    }
    return true;
}


/// проверка пароля контейнера
bool cppCryptoCheckPassword(const char * containerName, const char * password) {
    bool result = true;
    HCRYPTPROV  hProv = 0;
    if( !CryptAcquireContext(&hProv, containerName, CP_KC1_GR3410_2012_PROV, PROV_GOST_2012_256, 1 | CRYPT_SILENT) ) {
        return false;
    }
    /* сохраним для функции по смене пин кода
    CRYPT_PIN_PARAM param;
    param.type = CRYPT_PIN_PASSWD;
    param.dest.passwd = (char *)password;
    //bResult = UniSetProvParam(hProv, PP_CHANGE_PIN, (BYTE*)&param, 0);
     */
    if (!CryptSetProvParam(
                         hProv,
                         PP_KEYEXCHANGE_PIN,
                         (BYTE *)password,
                         0))
    {
        return false;
    }
    DWORD dwHashDataLen;
    BYTE *pbHashData;
    if (cppCryptoHash(hProv, (BYTE *)"test", 4, NULL, &dwHashDataLen)) {
        pbHashData = new BYTE[dwHashDataLen];
        if(!cppCryptoHash(hProv, (BYTE *)"test", 4, pbHashData, &dwHashDataLen)){
            DWORD lastError = CSP_GetLastError();
            PrintLastError();
            if (SCARD_W_WRONG_CHV == lastError ){
                result = false;
            }
        }
        // освобождаем хэш
        delete [] pbHashData;
    } else {
        result = false;
    }
    if (!result) {
        // сбрасываем пин, если он не верен
        CryptSetProvParam(hProv, PP_KEYEXCHANGE_PIN, NULL, 0);
    }
    // освобождаем контекст
    cppCryptoFreeContext(hProv);
    return result;
}

CSP_BOOL cppCryptoHash(HCRYPTPROV  hProv, BYTE *pbData, DWORD dwDataLen, BYTE *pbHashData, DWORD *dwHashDataLen) {
    HCRYPTHASH   hHash           = 0;
    /*BYTE* pbHash          = NULL; // Указатель на буфер для хэша
    DWORD dwHashSize      = 0;    // Размер хэша
    BYTE* pbSignature     = NULL; // Указатель на буфер для подписи
     */
    DWORD dwSignatureSize = 0;    // Размер подписи
    
    // создаем хэш
    if (!CryptCreateHash(hProv, CALG_GR3411, 0, 0, &hHash)) {
        return FALSE;
    }
    if (!CryptSetHashParam(hHash, HP_OID, (BYTE*)OID_HashVerbaO, 0)) {
        return FALSE;
    }
    if (!CryptHashData(hHash, pbData, dwDataLen, 0)) {
        return FALSE;
    }
    if (!CryptGetHashParam(hHash, HP_HASHVAL, NULL, dwHashDataLen, 0)) {
        return FALSE;
    }
    if (pbHashData == NULL) {
        return TRUE;
    }
    if (!CryptGetHashParam(hHash, HP_HASHVAL, pbHashData, dwHashDataLen, 0)){
        return FALSE;
    }
    if (!CryptSignHash(hHash, AT_KEYEXCHANGE, NULL, 0, NULL, &dwSignatureSize)) {
        return FALSE;
    }
    
    return TRUE;
    //
    /*
    std::string text = "test";
    // Получили контейнер, далее получаем хеш и подпись
    if (CryptCreateHash(hProv, CALG_GR3411, 0, 0, &hHash)) {
        if (CryptSetHashParam(hHash, HP_OID, (BYTE*)OID_HashVerbaO, 0)) {
            BYTE *pbData = (BYTE *)text.c_str();
            DWORD dwDataLen = (DWORD)text.length();
            
            // Вычисление значения хэша
            if (CryptHashData(hHash, pbData, dwDataLen, 0)) {
                // Получение размера буфера для хэша
                //if (CryptGetHashParam(hHash, HP_HASHVAL, NULL, &dwHashSize, 0)) {
                if (CryptGetHashParam(hHash, HP_HASHVAL, NULL, &dwHashSize, 0)) {
                    // pbHash = (BYTE*)malloc(dwHashSize * sizeof(BYTE)); // Выделение памяти для буфера
                    pbHash = (BYTE*)malloc(dwHashSize); // Выделение памяти для буфера
                    if(pbHash) {
                        // Получение указателя на буфер с хэшем
                        if (CryptGetHashParam(hHash, HP_HASHVAL, pbHash, &dwHashSize, 0)) {
                            //hashSize = dwHashSize;
                            //hash = pbHash;
                            printf("hashSize: %d\n", dwHashSize);
                            printf("hash: %s\n", pbHash);
                            // Подпись хэшированных данных
                            if (CryptSignHash(hHash, AT_KEYEXCHANGE, NULL, 0, NULL, &dwSignatureSize)) {
                                pbSignature = (BYTE*)malloc(dwSignatureSize * sizeof(BYTE)); // Выделение памяти для буфера
                                if (pbSignature) {
                                    if (CryptSignHash(hHash, AT_KEYEXCHANGE, NULL, 0, pbSignature, &dwSignatureSize)) {
                                        // signSize = dwSignatureSize;
                                        // sign = pbSignature;
                                        // result = true;
                                        // message = "Подпись вычислена";
                                        // error = 0;
                                        printf("Подпись: %s\n", pbSignature);
                                    } else {
                                        printf("Не удалось вычислить подпись\n");
                                    }
                                } else {
                                    printf("Ошибка выделения памяти\n");
                                }
                                
                            } else {
                                printf("Ошибка вычисления подписи\n");
                                printf("error: %d", CSP_GetLastError());
                                PrintLastError();
                            }
                        }
                    }
                }
            }
        }
    }
    return TRUE;
     */
}
///
bool cppCryptoVerifyCertificate(BYTE *certData, DWORD certSize)
{
    //std::string certData = base64_decode(b64Certificate);
    //#define ENCODING_TYPE (X509_ASN_ENCODING | PKCS_7_ASN_ENCODING)
    PCCERT_CONTEXT pCertContext = CertCreateCertificateContext(X509_ASN_ENCODING, (BYTE *)certData, (DWORD)certSize);
    if (!pCertContext)
    {
        PrintLastError();
        return false;
    }

    HCERTCHAINENGINE certChainEngine = NULL;
    //HCERTSTORE hRootStore = NULL;
    CERT_CHAIN_PARA ChainPara;
    ZeroMemory(&ChainPara, sizeof(CERT_CHAIN_PARA));
    ChainPara.cbSize = sizeof(CERT_CHAIN_PARA);
    ChainPara.RequestedUsage.dwType = USAGE_MATCH_TYPE_AND;
    //ChainPara.RequestedUsage.Usage.cUsageIdentifier = 0;
    //ChainPara.RequestedUsage.Usage.rgpszUsageIdentifier = NULL;
    
    PCCERT_CHAIN_CONTEXT pChainContext;
    
    
    DWORD chainFlags = CERT_CHAIN_REVOCATION_CHECK_CHAIN;
    if (!CertGetCertificateChain(certChainEngine, pCertContext, NULL, NULL, &ChainPara, chainFlags, NULL, &pChainContext))
    {
        
        return false;
    }
    PrintLastError();
    printf("%d\n",pChainContext->TrustStatus.dwErrorStatus);
    //CryptRetrieveObjectByUrlA
    //https://software.ucsouz.ru/installer/cert/ucsouz-int-2018.cer
    //CryptRetrieveObjectByUrlA () UrlRetriever failed (CURLcode: 12169 URL: https://software.ucsouz.ru/installer/cert/ucsouz-int-2018.cer).
    
    //CryptRetrieveObjectByUrl
    
    
    printf("The size of the chain context "
           "is %d. \n",pChainContext->cbSize);
    printf("%d simple chains found.\n",pChainContext->cChain);
    printf("\nError status for the chain:\n");
    //ERROR_WINHTTP_CANNOT_CONNECT
    //#define ERROR_WINHTTP_SECURE_INVALID_CERT 12169 !!!!
    switch(pChainContext->TrustStatus.dwErrorStatus)
    {
        case CERT_TRUST_NO_ERROR :
            printf("No error found for this certificate or chain.\n");
            break;
        case CERT_TRUST_IS_NOT_TIME_VALID:
            printf("This certificate or one of the certificates in the "
                   "certificate chain is not time-valid.\n");
            break;
        case CERT_TRUST_IS_REVOKED:
            printf("Trust for this certificate or one of the certificates "
                   "in the certificate chain has been revoked.\n");
            break;
        case CERT_TRUST_IS_NOT_SIGNATURE_VALID:
            printf("The certificate or one of the certificates in the "
                   "certificate chain does not have a valid signature.\n");
            break;
        case CERT_TRUST_IS_NOT_VALID_FOR_USAGE:
            printf("The certificate or certificate chain is not valid "
                   "in its proposed usage.\n");
            break;
        case CERT_TRUST_IS_UNTRUSTED_ROOT:
            printf("The certificate or certificate chain is based "
                   "on an untrusted root.\n");
            break;
        case CERT_TRUST_REVOCATION_STATUS_UNKNOWN:
            printf("The revocation status of the certificate or one of the"
                   "certificates in the certificate chain is unknown.\n");
            break;
        case CERT_TRUST_IS_CYCLIC :
            printf("One of the certificates in the chain was issued by a "
                   "certification authority that the original certificate "
                   "had certified.\n");
            break;
        case CERT_TRUST_IS_PARTIAL_CHAIN:
            printf("The certificate chain is not complete.\n");
            break;
        case CERT_TRUST_CTL_IS_NOT_TIME_VALID:
            printf("A CTL used to create this chain was not time-valid.\n");
            break;
        case CERT_TRUST_CTL_IS_NOT_SIGNATURE_VALID:
            printf("A CTL used to create this chain did not have a valid "
                   "signature.\n");
            break;
        case CERT_TRUST_CTL_IS_NOT_VALID_FOR_USAGE:
            printf("A CTL used to create this chain is not valid for this "
                   "usage.\n");
    } // End switch
    
    printf("\nInfo status for the chain:\n");
    switch(pChainContext->TrustStatus.dwInfoStatus)
    {
        case 0:
            printf("No information status reported.\n");
            break;
        case CERT_TRUST_HAS_EXACT_MATCH_ISSUER :
            printf("An exact match issuer certificate has been found for "
                   "this certificate.\n");
            break;
        case CERT_TRUST_HAS_KEY_MATCH_ISSUER:
            printf("A key match issuer certificate has been found for this "
                   "certificate.\n");
            break;
        case CERT_TRUST_HAS_NAME_MATCH_ISSUER:
            printf("A name match issuer certificate has been found for this "
                   "certificate.\n");
            break;
        case CERT_TRUST_IS_SELF_SIGNED:
            printf("This certificate is self-signed.\n");
            break;
        case CERT_TRUST_IS_COMPLEX_CHAIN:
            printf("The certificate chain created is a complex chain.\n");
            break;
    } // end switch
    return pChainContext->TrustStatus.dwErrorStatus == CERT_TRUST_NO_ERROR;
}
///
void cppCryptoFreeSimpleSign(SIMPLE_SIGN simpleSign) {
    if (simpleSign.b64Hash) {
        delete simpleSign.b64Hash;
        simpleSign.b64Hash = NULL;
    }
    if (simpleSign.b64Sign) {
        delete simpleSign.b64Sign;
        simpleSign.b64Sign = NULL;
    }
    simpleSign.b64HashSize = 0;
    simpleSign.b64SignSize = 0;
    simpleSign.errorCode = 0;
}
/// делает простую подпись данных в base64 кодировке,
/// возвращает подписанную строку также в base64 кодировке
SIMPLE_SIGN cppCryptoSimpleSign(const char *containerName, const char *containerPin, const char *hashAlgorithmObjId, const char *b64Data) {
    size_t b64DataSize = strlen(b64Data);
    SIMPLE_SIGN result;
    HCRYPTPROV hProv = 0;
    HCRYPTHASH hHash = 0;
    size_t dataSize;
    BYTE *data = NULL;
    DWORD dwHashSize;
    BYTE *pbHash = NULL;
    DWORD dwSignSize;
    BYTE *pbSign = NULL;
    CSP_SetLastError(0);
    result.b64Hash = NULL;
    result.b64Sign = NULL;
    result.errorCode = 0;
    
    if (CryptAcquireContext(&hProv, containerName, CP_KC1_GR3410_2012_PROV, PROV_GOST_2012_256, 1 | CRYPT_SILENT)) {
        if (containerPin != NULL) {
            if (!CryptSetProvParam(hProv, PP_KEYEXCHANGE_PIN, (BYTE *)containerPin, 0)) {
                result.errorCode = CSP_GetLastError();
                CryptReleaseContext(hProv, 0);
                return result;
            }
        }
        ALG_ID algid = GetHashAlgorithm(hashAlgorithmObjId);
        // создаем хэш
        if (CryptCreateHash(hProv, algid, 0, 0, &hHash)) {
            if (algid == CALG_GR3411) {
                if (!CryptSetHashParam(hHash, HP_OID, (BYTE *)OID_HashVerbaO, 0)) {
                    result.errorCode = CSP_GetLastError();
                    CryptReleaseContext(hProv, 0);
                    return result;
                }
            }
            data = base64_decode(b64Data, b64DataSize, &dataSize);
            if (CryptHashData(hHash, (BYTE *)data, (DWORD)dataSize, 0)) {
                if (CryptGetHashParam(hHash, HP_HASHVAL, NULL, &dwHashSize, 0)){
                    pbHash = new BYTE [dwHashSize];
                    if (CryptGetHashParam(hHash, HP_HASHVAL, pbHash, &dwHashSize, 0)) {
                        result.b64Hash = base64_encode(pbHash, dwHashSize, &result.b64HashSize);
                        if (pbHash != NULL) {
                            delete [] pbHash;
                            pbHash = NULL;
                        }
                        if (CryptSignHash(hHash, AT_KEYEXCHANGE, NULL, 0, NULL, &dwSignSize)){
                            pbSign = new BYTE [dwSignSize];
                            if (CryptSignHash(hHash, AT_KEYEXCHANGE, NULL, 0, pbSign, &dwSignSize)){
                                result.b64Sign = base64_encode(pbSign, dwSignSize, &result.b64SignSize);
                            }
                            if (pbSign != NULL) {
                                delete [] pbSign;
                                pbSign = NULL;
                            }
                        }//CryptSignHash
                    } //CryptGetHashParam size
                }// CryptGetHashParam
            }// CryptHashData
        }// CryptCreateHash
    }//CryptAcquireContext
    result.errorCode = CSP_GetLastError();
    if (data != NULL) {
        delete [] data;
        data = NULL;
    }
    if(hHash)
        CryptDestroyHash(hHash);
    if(hProv)
    CryptReleaseContext(hProv, 0);
    PrintLastError();
    return result;
}
// TODO: есть потенциальные утечки памяти, надо перестроить код
bool cppCryptoVerifySimpleSign(const char *b64Certificate, const char *hashAlgorithmObjId, const char *b64Data, const char *b64Sign)
{
    size_t certDataSize64 = strlen(b64Certificate);
    size_t signSize64 = strlen(b64Sign);
    size_t dataSize64 = strlen(b64Data);
    size_t certDataSize;
    size_t dataSize;
    bool result = false;
    BYTE *sign = NULL;
    BYTE *data = NULL;
    HCRYPTPROV hProv = 0;
    HCRYPTKEY hPubKey = 0;
    HCRYPTHASH hHash = 0;
    size_t signSize;
    PCCERT_CONTEXT pCertContext;
    char * certData = (char *)base64_decode(b64Certificate, certDataSize64, &certDataSize);
    ALG_ID algid = GetHashAlgorithm(hashAlgorithmObjId);
    data = base64_decode(b64Data, dataSize64, &dataSize);
    sign = base64_decode(b64Sign, signSize64, &signSize);

    pCertContext = CertCreateCertificateContext(ENCODING_TYPE, (BYTE *)certData, (DWORD)certDataSize);
    PrintLastError();
    
    if (pCertContext) {
        if (CryptAcquireContext(&hProv, NULL, CP_KC1_GR3410_2012_PROV, PROV_GOST_2012_256, CRYPT_VERIFYCONTEXT | CRYPT_SILENT))
        {
            if (CryptImportPublicKeyInfo(hProv, ENCODING_TYPE, &(pCertContext->pCertInfo->SubjectPublicKeyInfo), &hPubKey))
            {
                if (CryptCreateHash(hProv, algid, 0, 0, &hHash))
                {
                    if (algid == CALG_GR3411) {
                        CryptSetHashParam(hHash, HP_OID, (BYTE *)OID_HashVerbaO, 0);
                    }
                    if (CryptHashData(hHash, data, (DWORD)dataSize, 0))
                    {
                        if (CryptVerifySignature(hHash, (BYTE *)sign, (DWORD)signSize, hPubKey, NULL, 0))
                        {
                            result = true;
                        }
                    }
                }
            }
        }
    }
    PrintLastError();
    /// free all
    if (certData)
        free(certData);
    if (data)
        free(data);
    if (sign)
        free(sign);
    if(hProv)
        CryptReleaseContext(hProv, 0);
    if(hPubKey)
        CryptDestroyKey(hPubKey);
    if(hHash)
        CryptDestroyHash(hHash);
    
    return result;
}
