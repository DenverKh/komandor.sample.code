//
//  define.h
//  Komandor
//
//  Created by Denis Khlopin on 20/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

#ifndef define_h
#define define_h

#include <CPROCSP/CPROCSP.h>
#include <CPROCSP/CSP_WinDef.h>
#include <CPROCSP/CSP_WinCrypt.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "Structures.h"

#define PROV_TYPE PROV_GOST_2012_256
#define PROV_NAME CP_KC1_GR3410_2012_PROV
#define PROV_NAME_A CP_GR3410_2012_PROV_A
/*
#define PROV_NAME CP_GR3410_2012_PROV
#define PROV_NAME_A CP_GR3410_2012_PROV_A
*/
#define MY_ENCODING_TYPE (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#define MY_NAME_LIST (CERT_OID_NAME_STR|CERT_NAME_STR_CRLF_FLAG|CERT_NAME_STR_NO_QUOTING_FLAG)

#define CD_ISSUER 1
#define CD_SUBJECT 2
#define CD_ISSUER_UNIQUE_ID 4
#define CD_SUBJECT_UNIQUE_ID 5
#define CD_SIGNATURE_ALGO_PARAMETERS 6
#define CD_SUBJECT_PUBLIC_KEY 7
#define CD_SUBJECT_PUBLIC_ALGO_PARAMETERS 8
#define CD_EXTENTION_VALUE 9
#define CD_SERIAL_NUMBER 10

#define ENCODING_TYPE (X509_ASN_ENCODING | PKCS_7_ASN_ENCODING)


#endif /* define_h */
