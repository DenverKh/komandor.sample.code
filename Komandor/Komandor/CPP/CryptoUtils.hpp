//
//  CryptoUtils.h
//  Komandor
//
//  Created by Denis Khlopin on 20/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

#ifndef CryptoUtils_h
#define CryptoUtils_h

#include "define.h"
#ifdef __cplusplus
extern "C" {
#endif
    ALG_ID GetEphemKeyAlgorithmByCert(const PCCERT_CONTEXT pCertContext);
    ALG_ID GetHashAlgorithm(const char * hashAlgorithmObjId);
    double timeToDouble(FILETIME ft);
    void PrintLastError();
    ProvKeyInfo ProvKeyInfoFromB64(const char *keyId);
    char * ProvKeyInfoToB64(const ProvKeyInfo pkInfo);
    char * InvByteToHexStr(void *pvData, DWORD cbData);
    char * ByteToHexStr(void *pvData, DWORD cbData);
    char * GetCertThumbprint(PCCERT_CONTEXT pCertContext);

#ifdef __cplusplus
}
#endif

#endif /* CryptoUtils_h */
