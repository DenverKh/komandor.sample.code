//
//  Date+Extension.swift
//  VideoRecorder
//
//  Created by Denis Khlopin on 05.10.17.
//  Copyright © 2017 Denis Khlopin. All rights reserved.
//

import Foundation
extension Date {
    func valueOf(_ component: Calendar.Component) -> String {
        let val = Calendar.current.component(component, from: self)
        return String(val)
    }
    func valueOf(_ component: Calendar.Component, withMinLength len: Int) -> String {
        let val = Calendar.current.component(component, from: self)
        var result = String(val)
        while result.count < len {
            result = "0" + result
        }
        return result
    }

    /* examples
     gmtDate!.valueOf(.minute)
     gmtDate!.valueOf(.second)
     gmtDate!.valueOf(.hour)
     gmtDate!.valueOf(.day)
     gmtDate!.valueOf(.month)
     gmtDate!.valueOf(.year)
     */
    func value(withFormat: String) -> String {
        let formatter = DateFormatter()
        if withFormat.isEmpty {
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        } else {
            formatter.dateFormat = withFormat
        }
        return formatter.string(from: self)
    }
    func getGMTDate() -> Date {
        let gmtFormatter = DateFormatter()
        gmtFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        gmtFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let localFormatter = DateFormatter()
        localFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let gmtDate =  localFormatter.date(from: gmtFormatter.string(from: self) )
        return gmtDate!
    }
}
