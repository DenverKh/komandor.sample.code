//
//  NSRegularExpression+Extension.swift
//  Komandor
//
//  Created by Denis Khlopin on 21/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
extension NSRegularExpression {
    convenience init(_ pattern: String) {
        do {
            try self.init(pattern: pattern)
        } catch {
            preconditionFailure("Illegal regular expression: \(pattern).")
        }
    }
    func matches(_ string: String) -> Bool {
        let range = NSRange(location: 0, length: string.utf16.count)
        return firstMatch(in: string, options: [], range: range) != nil
    }
}
