//
//  Bundle+Extension.swift
//  Komandor
//
//  Created by Kostya Golenkov on 13/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
extension Bundle {
    /**
     Название приложения
     */
    var appName: String {
        let name = infoDictionary?["CFBundleDisplayName"] as? String
        return name ?? ""
    }
    /**
     Версия приложения
     */
    var releaseVersionNumber: String {
        let version = infoDictionary?["CFBundleShortVersionString"] as? String
        return version ?? ""
    }
    /**
     Версия сборки приложения
     */
    var buildVersionNumber: String {
        let build = infoDictionary?["CFBundleVersion"] as? String
        return build ?? ""
    }
}
