//
//  String+Extention.swift
//  Komandor
//
//  Created by Denis Khlopin on 21/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit

extension String {
    // open url from string
    func openURLString() {
        guard let url = URL(string: self) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    // обработка регулярные выражения
    // пример : "8917899999999" ~= "79([0-9]{2})([0-9]{3})([0-9]{2})([0-9]{2})$" -> false
    // пример : "7917899999999" ~= "79([0-9]{2})([0-9]{3})([0-9]{2})([0-9]{2})$" -> true
    /// проверка на регулярное выражение
    static func ~= (lhs: String, rhs: String) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: rhs) else { return false }
        let range = NSRange(location: 0, length: lhs.utf16.count)
        return regex.firstMatch(in: lhs, options: [], range: range) != nil
    }
    /// проверка на регулярное выражение вариант 2
    func  isMatches(regexp regexString: String) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: regexString) else { return false }
        let range = NSRange(location: 0, length: self.utf16.count)
        return regex.firstMatch(in: self, options: [], range: range) != nil

    }
    /// конвертация строки в формате BASE64 в Data
    func convertToImageData() -> Data? {
        //"data:image/jpeg;base64,AAAAAA=="
        //"data:image/png;base64,AAAAAA=="
        let jpegPrefix = "data:image/jpeg;base64,"
        let pngPrefix = "data:image/png;base64,"
        var base64String: String
        if self.hasPrefix(jpegPrefix) {
            base64String = String(self.suffix(self.count - jpegPrefix.count))
        } else if self.hasPrefix(pngPrefix) {
            base64String = String(self.suffix(self.count - pngPrefix.count))
        } else {
            return nil
        }
        if let data = Data(base64Encoded: base64String) {
            return data
        }
        return nil
    }
}
