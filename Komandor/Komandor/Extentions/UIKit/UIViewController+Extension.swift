//
//  UIViewController+Extension.swift
//  Komandor
//
//  Created by Kostya Golenkov on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

extension UIViewController {
    /**
     Returns a newly initialized view controller with the nib file. The nib file has same name as the view controller.
     */
    static func initFormNib<T: UIViewController>() -> T {
        return T(nibName: String(describing: self), bundle: nil)
    }
}
