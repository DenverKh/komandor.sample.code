//
//  UIImage+Extension.swift
//  Komandor
//
//  Created by Denis Khlopin on 22/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

extension UIImage {
    func resize(_ targetSize: CGSize) -> UIImage {
        let size = self.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    // image to base64 data format
    func convertToBase64Jpeg() -> String? {
        //"data:image/png;base64,AAAAAA=="
        if let jpegData = self.jpegData(compressionQuality: 1.0) {
            let jpegBase64 = jpegData.base64EncodedString()
            let result = "data:image/jpeg;base64," + jpegBase64
            return result
        }
        return nil
    }
    // image to base64 data format
    func convertToBase64Png() -> String? {
        //"data:image/png;base64,AAAAAA=="
        if let pngData = self.pngData() {
            let pngBase64 = pngData.base64EncodedString()
            let result = "data:image/png;base64," + pngBase64
            return result
        }
        return nil
    }
}
