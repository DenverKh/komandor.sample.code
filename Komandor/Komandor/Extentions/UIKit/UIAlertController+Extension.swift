//
//  UIAlertController+Extension.swift
//  Komandor
//
//  Created by Kostya Golenkov on 13/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

extension UIAlertController {
    /**
     Создает алерт диалог с кнопкной "Ок"
     */
    class func simpleAlert(title: String?, message: String?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: nil))
        return alert
    }
    /** Создает алерт диалог с названием "Ошибка" и кнопкной "Ок"*/
    class func simpleErrorAlert(message: String) -> UIAlertController {
        let alert = UIAlertController.simpleAlert(title: "Ошибка", message: message)
        return alert
    }
}
