//
//  UITextField+Extension.swift
//  Komandor
//
//  Created by Denis Khlopin on 09/01/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//
import UIKit

extension UITextField {
    func addDoneToKeyboard(target: Any?,
                           doneAction: Selector? = nil, cancelAction: Selector? = nil,
                           doneLabel: String? = nil, cancelLabel: String? = nil) -> UIToolbar {
        let width = UIScreen.main.bounds.width
        let doneToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: width, height: 40))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarItem = UIBarButtonItem(title: doneLabel ?? "Done", style: .done, target: target, action: doneAction)
        var items = [UIBarButtonItem]()
        if cancelAction != nil {
            let cancelBarItem = UIBarButtonItem(title: cancelLabel ?? "Cancel", style: .plain, target: target, action: cancelAction)
            items.append(cancelBarItem)
        }
        items.append(flexSpace)
        items.append(doneBarItem)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.inputAccessoryView = doneToolbar
        return doneToolbar
    }
}
