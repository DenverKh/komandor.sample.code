//
//  UIImageView+Extension.swift
//  Komandor
//
//  Created by Denis Khlopin on 22/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

extension UIImageView {
    func setPhotoImage(data: Data?) {
        self.layer.cornerRadius = self.frame.height < self.frame.width ? self.frame.height/2: self.frame.width/2
        self.clipsToBounds = true
        if let data = data {
            self.image = UIImage(data: data)
        } else {
            let image = #imageLiteral(resourceName: "user-icon")
            self.image = image
        }
    }
}
