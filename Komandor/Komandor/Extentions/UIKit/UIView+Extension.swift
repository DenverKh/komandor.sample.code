//
//  UIView+Extension.swift
//  Komandor
//
//  Created by Denis Khlopin on 28/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit

extension UIView {
    func wrapInSuperview(constant: CGFloat = 0) {
        if let superview = self.superview {
            translatesAutoresizingMaskIntoConstraints = false
            self.leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: constant).isActive = true
            self.trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: constant).isActive = true
            self.topAnchor.constraint(equalTo: superview.topAnchor, constant: constant).isActive = true
            self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: constant).isActive = true
        }
    }
    // TODO: удалить за ненадобностью
    func getActiveTextField() -> UITextField? {
        let totalTextFields = getTextFieldsInView(view: self)
        for textField in totalTextFields where textField.isFirstResponder {
                return textField
        }
        return nil
    }
    func getTextFieldsInView(view: UIView) -> [UITextField] {
        var totalTextFields = [UITextField]()
        for subview in view.subviews as [UIView] {
            if let textField = subview as? UITextField {
                totalTextFields += [textField]
            } else {
                totalTextFields += getTextFieldsInView(view: subview)
            }
        }
        return totalTextFields
    }
    // получение VisualEffectsSubview
    func searchVisualEffectsSubview() -> UIVisualEffectView? {
        if let visualEffectView = self as? UIVisualEffectView {
            return visualEffectView
        } else {
            for subview in subviews {
                if let found = subview.searchVisualEffectsSubview() {
                    return found
                }
            }
        }
        return nil
    }
}
