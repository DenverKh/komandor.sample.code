//
//  UINavigationController+Extension.swift
//  Komandor
//
//  Created by Denis Khlopin on 25/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit

extension UINavigationController {
    func setBackButtonTitle(_ title: String) {
        self.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem()
        self.navigationBar.topItem?.backBarButtonItem?.title = title
    }
}
