//
//  SplashScreenRouter.swift
//  Komandor
//
//  Created by Kostya Golenkov on 12/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

class SplashScreenRouter: Router {
    enum Routes {
        case login
        case general
    }
    var baseViewController: UIViewController?
    init(navigationController: UINavigationController?) {
        self.baseViewController = navigationController
    }

    func start(context: Any?) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            let controller: SplashScreenViewController = SplashScreenViewController.initFormNib()
            let viewModel = SplashScreenViewModel(router: self)
            controller.viewModel = viewModel
            self.baseViewController = controller
            delegate.switchRootViewController(toVC: controller)
        }
    }

    func route(context: Any?) {
        guard let route = context as? Routes else { return }
        // check route here
        switch route {
        case .login:
            RouteManager.manager.startLoginFlow()
        case .general:
            RouteManager.manager.startGeneralFlow()
        }
    }
}

extension SplashScreenRouter {
    func route(goTo: Routes) {
        route(context: goTo)
    }
}
