//
//  SplashScreenViewController.swift
//  Komandor
//
//  Created by Kostya Golenkov on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController, View {
    var viewModel: SplashScreenViewModel!
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.checkAuth()
    }
}
