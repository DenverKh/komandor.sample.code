//
//  SplashViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation

class SplashScreenViewModel: ViewModel {
    var router: Router
    var sessionService: Any?
    var authService: APIAuthService?
    //
    init(router: Router) {
        self.router = router
    }
    //
    func checkAuth() {
        guard let router = self.router as? SplashScreenRouter else {
            fatalError("Router not found!")
        }
        // проверяем, пинкод введен в сертификат ранее
        let pinCertAuthed = CertificatesManager.manager.checkCurrentCertificateAuthed()
        // если пин подходит
        if pinCertAuthed, let certificate = CertificatesManager.manager.currentCertificate {
            // создаем пользователя
            _ = UserManager.manager.authCurrentUser(certificate: certificate)
            // TODO: в случае отсутствия интернета, нужно загружаться в оффлайн режиме
            // добавить эту проверку здесь, либо вынести в API AuthService            
            authService = APIAuthService(certificate: certificate)
            authService?.auth { (error, token, phoneNeeded) in
                DispatchQueue.main.async {
                    if let error = error {
                        print(error.localizedDescription)
                        CertificatesManager.manager.currentCertificate = nil
                        router.route(goTo: .login)
                    } else {
                        // если нет ошибок и телефон регистрировать не нужно, делаем автологин
                        if let phoneNeeded = phoneNeeded, !phoneNeeded, let token = token {
                            // save token here
                            /// сохраняем токен
                            _ = SecureStorage.setString(service: "token", key: certificate.name, value: token )
                            // route to general
                            router.route(goTo: .general)
                        } else {
                            // route to login
                            CertificatesManager.manager.currentCertificate = nil
                            router.route(goTo: .login)
                        }
                    }
                }
            }
        } else {
            CertificatesManager.manager.currentCertificate = nil
            // route to login
            router.route(goTo: .login)
        }
    }
}
