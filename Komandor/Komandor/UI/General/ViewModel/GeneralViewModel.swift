//
//  GeneralViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class GeneralViewModel: ViewModel {
    var router: Router
    init(router: Router) {
        self.router = router
    }
}
