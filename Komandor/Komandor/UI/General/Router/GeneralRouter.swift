//
//  GeneralRouter.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

class GeneralRouter: Router {
    enum Routes {
        case empty
    }
    enum Tabs: Int {
        case contacts = 0
        case chat = 1
        case settings = 2
    }
    var baseViewController: UIViewController?
    // ссылки на локальные роутеры
    var chatRouter: ChatRouter?
    var contactsRouter: ContactsRouter?
    var settingsRouter: SettingsRouter?
    var generalViewController: GeneralViewController? {
        return baseViewController as? GeneralViewController
    }
    init(navigationController: UINavigationController?) {
        self.baseViewController = navigationController
    }

    func start(context: Any?) {
        // present first controller here
        showGeneral()
    }

    func route(context: Any?) {
        guard let route = context as? Routes else { return }
        // check route here
        switch route {
        case .empty:
            break
        }
    }
    func showGeneral() {
        createChildren()
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            let controller = GeneralViewController()
            let viewModel = GeneralViewModel(router: self)
            controller.viewModel = viewModel
            // добавляем контроллеры
            if let contactsController = contactsRouter?.baseViewController,
                let chatController = chatRouter?.baseViewController,
                let settingsController = settingsRouter?.baseViewController {
                controller.viewControllers = [contactsController, chatController, settingsController]
            }
            self.baseViewController = controller
            delegate.switchRootViewController(toVC: controller)
        }
    }
    /// создаем МВВМ структуру для дочерних контроллеров
    func createChildren() {
        // создаем мввм для окна Контакты
        contactsRouter = ContactsRouter(navigationController: nil, parent: self)
        contactsRouter?.start()
        // создаем мввм для окна Чаты
        chatRouter = ChatRouter(navigationController: nil, parent: self)
        chatRouter?.start()
        // создаем мввм для окна Настройки
        settingsRouter = SettingsRouter(navigationController: nil, parent: self)
        settingsRouter?.start()
    }
    func moveToChat(chatId: String) {
        if let chatRouter = self.chatRouter {
            //set chat id
            chatRouter.route(goTo: .moveToChat(chatId: chatId))
            switchTo(tab: .chat)
        }
    }
    func switchTo(tab: Tabs) {
        if let tabController = generalViewController {
            tabController.selectedIndex = tab.rawValue
        }
    }
}

extension GeneralRouter {
    func route(goTo: Routes) {
        route(context: goTo)
    }
}
