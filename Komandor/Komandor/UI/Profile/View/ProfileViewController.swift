//
//  ProfileViewController.swift
//  Komandor
//
//  Created by Denis Khlopin on 25/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

//TODO - нужно скорее всего сделать динамический набор полей в зависимости от типа сертификата
// физик
// ип
// ооо
// можно набор полей с названиями зашить в какой нибудь json

class ProfileViewController: UITableViewController, View {
    var viewModel: ProfileViewModel!
    let disposeBag = DisposeBag()
    //
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelOnlineStatus: UILabel!
    @IBOutlet weak var labelPhoneNumber: UILabel!
    @IBOutlet weak var labelSNILS: UILabel!
    @IBOutlet weak var labelCompany: UILabel!
    @IBOutlet weak var labelOGRN: UILabel!
    @IBOutlet weak var labelINN: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var buttonLogout: UIRoundButton!
    @IBOutlet weak var imagePhoto: UIImageView!
    @IBOutlet weak var buttonSelectPhoto: UIButton!
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProfileUI()
    }
    fileprivate func setupProfileUI() {
        guard let viewModel = self.viewModel else {
            fatalError("viewModel не определена!")
        }
        // обновляем лабелы и т.п. по событию
        viewModel.profileRefresh.subscribe(onNext: { [weak self] (_) in
            DispatchQueue.main.async {
                guard let viewModel = self?.viewModel else {
                    return
                }
                self?.labelName.text = viewModel.name
                self?.labelOnlineStatus.text = viewModel.onlineStatus
                self?.labelPhoneNumber.text = viewModel.phoneNumber
                self?.labelSNILS.text = viewModel.snils
                self?.labelCompany.text = viewModel.company
                self?.labelOGRN.text = viewModel.ogrn
                self?.labelINN.text = viewModel.inn
                self?.labelEmail.text = viewModel.email
                self?.imagePhoto.setPhotoImage(data: viewModel.imagePhotoData)
            }
        }).disposed(by: disposeBag)
        // buttons
        buttonLogout.rx.tap.bind { [weak self] in
            self?.logout()
        }.disposed(by: disposeBag)
    }
    fileprivate func logout() {
        guard let viewModel = self.viewModel else {
            fatalError("viewModel не определена!")
        }
        let alertController = UIAlertController(title: "Выйти", message: "Вы действительно хотите выйти из приложения?", preferredStyle: .alert)
        alertController.addAction(
            UIAlertAction(title: "Выйти", style: .default, handler: { (_) in
                viewModel.pressLogout()
            })
        )
        alertController.addAction(
            UIAlertAction(title: "Отмена", style: .cancel, handler: { (_) in
                // пусто, просто отмена
            })
        )
        self.present(alertController, animated: true)
    }
}
