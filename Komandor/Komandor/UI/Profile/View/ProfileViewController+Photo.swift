//
//  ProfileViewController+Photo.swift
//  Komandor
//
//  Created by Denis Khlopin on 22/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    /// редактирование фото профиля
    @IBAction func actionSelectPhoto(_ sender: Any) {
        // вывод меню загрузки фото
        let menuTitle = "Изменение фотографии профиля"
        let menuController = UIAlertController(title: menuTitle, message: nil, preferredStyle: .actionSheet)
        menuController.addAction(UIAlertAction(title: "Сделать новое фото", style: .default, handler: { (_) in
            self.pickImageFromCamera()
        }))
        menuController.addAction(UIAlertAction(title: "Выбрать из галереи", style: .default, handler: { (_) in            //
            self.pickImageFromLibrary()
        }))
        menuController.addAction(UIAlertAction(title: "Удалить текущее фото", style: .destructive, handler: { (_) in
            self.deletePhotoAlert()
        }))
        menuController.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: { (_) in
            //
        }))
        self.present(menuController, animated: true) {
            //
            print("present completion")
        }
    }
    /// выбор фото с камеры
    func pickImageFromCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.camera
        imagePicker.cameraDevice = UIImagePickerController.CameraDevice.front
        imagePicker.allowsEditing = true
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.present(imagePicker, animated: true)
        } else {
            self.present(imagePicker, animated: true)
        }
    }
    /// выбор фото с галереи
    func pickImageFromLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.present(imagePicker, animated: true)
        } else {
            self.present(imagePicker, animated: true)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let viewModel = self.viewModel else {
            fatalError("wrong viewModel")
        }
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            // получаем прямоугольную фотографию
            // обрезаем до размера 200x200
            let croppedImage = image.resize(CGSize(width: 200, height: 200))
            let jpegBase64String = croppedImage.convertToBase64Jpeg()
            if let base64 = jpegBase64String {
                viewModel.uploadPhoto(base64: base64)
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func deletePhotoAlert() {
        guard let viewModel = self.viewModel else {
            fatalError("wrong viewModel")
        }
        let title = "Удалить фото?"
        let message = "Вы действительно хотите удалить текущую фотографию?"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { (_) in
            viewModel.uploadPhoto(base64: "")
        }))
        alertController.addAction(UIAlertAction(title: "Отмена", style: .default, handler: { (_) in
        }))
        self.present(alertController, animated: true)
    }
}
