//
//  ProfileViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 25/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class ProfileViewModel: ViewModel {
    var router: Router
    var user: User!
    var certificate: Certificate? {
        return user.certificate
    }
    var userInfo: UserInfo? {
        return user.userInfo
    }
    var name: String {
        return user.userInfo?.name ?? ""
    }
    var onlineStatus: String {
        // TODO: определять онлайн статус        
        return user.status.toString()
    }
    var phoneNumber: String {
        return MaskUtils.phone(user.userInfo?.phone ?? "").mask
    }
    var snils: String {
        return MaskUtils.snils(user.userInfo?.snils ?? "")
    }
    var company: String {
        return user.userInfo?.company ?? ""
    }
    var ogrn: String {
        return user.userInfo?.ogrn ?? ""
    }
    var inn: String {
        return user.userInfo?.inn ?? ""
    }
    var email: String {
        return user.userInfo?.email ?? ""
    }
    var imagePhotoData: Data? {
        return user.photoImageData
    }
    //
    // сервис для загрузки фото профиля
    var apiPhotoService: APIPhotoService?
    var uploadingPhotoObserver = BehaviorSubject<Bool>(value: false)
    // сервис для обновления данных пользователя
    var userProfileLoaderService: UserProfileLoadService?
    var profileRefresh = BehaviorSubject<Bool>(value: true)
    let disposeBag = DisposeBag()
    //
    init(router: Router) {
        self.router = router
        guard let localUser = UserManager.manager.currentUser else {
            fatalError("user not found")
        }
        self.user = localUser
    }
    //
    func pressLogout() {
        guard let router = self.router as? ProfileRouter else {
            fatalError("Ошибка! роутер не присвоен!")
        }
        UserManager.manager.logoutCurrentUser()
        router.route(goTo: .auth)
    }
    func getToken() -> String {
        guard let certificate = self.certificate else {
            fatalError("wrong Certificate")
        }
        guard let token = CertificateToken(certificate: certificate).getToken() else {
            fatalError("can't get Certificate Token")
        }
        return token
    }
    // запрос на сервер на изменения фото
    func uploadPhoto(base64: String) {
        let token = getToken()
        let photoRequest = APIPhotoRequest(token: token, data: base64)
        apiPhotoService = APIPhotoService(request: photoRequest, apiClient: HttpAPIClient.defaultClient)
        uploadingPhotoObserver.onNext(true)
        apiPhotoService?.run(completion: { [weak self] (_, error) in
            if error != nil {
                // stop with error
                self?.uploadingPhotoObserver.onNext(false)
            } else {
                // normal finish
                self?.uploadingPhotoObserver.onNext(false)
                self?.refreshUserProfile()
            }
        })
    }
    func refreshUserProfile() {
        if userProfileLoaderService == nil {
            userProfileLoaderService = UserProfileLoadService(user: user)
            userProfileLoaderService?.userInfoObserver.subscribe(onNext: { [weak self] (userInfo) in
                if userInfo != nil {
                    self?.profileRefresh.onNext(true)
                }
            }).disposed(by: disposeBag)
        }
        userProfileLoaderService?.load()
    }
}
