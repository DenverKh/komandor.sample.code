//
//  ProfileRouter.swift
//  Komandor
//
//  Created by Denis Khlopin on 25/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

class ProfileRouter: Router {
    enum Routes {
        case profile
        case auth
    }
    var baseViewController: UIViewController?
    init(navigationController: UINavigationController?) {
        self.baseViewController = navigationController
    }

    func start(context: Any?) {
        showProfile()
    }

    func route(context: Any?) {
        guard let route = context as? Routes else { return }
        // check route here
        switch route {
        case .profile:
            break
        case .auth:
            showAuth()
        }
    }
    func showProfile() {
        if let controller: ProfileViewController = Storyboards.getController(from: .profile) {
            let viewModel = ProfileViewModel(router: self)
            controller.viewModel = viewModel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func showAuth() {
        RouteManager.manager.startLoginFlow()
    }
}

extension ProfileRouter {
    func route(goTo: Routes) {
        route(context: goTo)
    }
}
