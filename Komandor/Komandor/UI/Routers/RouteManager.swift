//
//  RouteManager.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation
class RouteManager {
    static let manager = RouteManager()
    //variables
    //routers
    fileprivate let authRouter = AuthRouter(navigationController: nil)
    fileprivate let splashScreenRouter = SplashScreenRouter(navigationController: nil)
    fileprivate let profileRouter = ProfileRouter(navigationController: nil)
    fileprivate let generalRouter = GeneralRouter(navigationController: nil)
    //
    fileprivate init() {
        initialize()
    }
    // initialize here
    fileprivate func initialize() {
    }
    func start() {
        splashScreenRouter.start(context: nil)
    }
    func startLoginFlow() {
        authRouter.start(context: nil)
    }
    func startGeneralFlow() {
        generalRouter.start(context: nil)
    }
}
