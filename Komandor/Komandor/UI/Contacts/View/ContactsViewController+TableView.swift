//
//  ContactsViewController+TableView.swift
//  Komandor
//
//  Created by Denis Khlopin on 18/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

extension ContactsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel else {
            fatalError("отсутствует viewModel")
        }
        let contact = getContact(indexPath: indexPath)
        viewModel.selectContact(contact: contact)
    }
}
extension ContactsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            fatalError("отсутствует viewModel")
        }
        return viewModel.contactsCount
    }
    //
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as? ContactCell {
            //убираем выделение ячейки
            cell.selectionStyle = .none
            let contact = getContact(indexPath: indexPath)
            let cellViewModel = ContactCellViewModel(contact: contact)
            cell.viewModel = cellViewModel
            cell.parentController = self
            return cell
        }
        return UITableViewCell()
    }
    //
    func getContact(indexPath: IndexPath) -> ContactCellProtocol {
        guard let viewModel = viewModel else {
            fatalError("отсутствует viewModel")
        }
        return viewModel.contacts[indexPath.row]
    }
}
