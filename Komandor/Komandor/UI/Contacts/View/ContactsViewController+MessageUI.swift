//
//  ContactsViewController+MessageUI.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import MessageUI

extension ContactsViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    //
    func sendMessageTo(contact: Contact, message: String) {
        if MFMessageComposeViewController.canSendText() {
            let controller = MFMessageComposeViewController()
            controller.body = message
            controller.recipients = contact.contactPhones
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
}
