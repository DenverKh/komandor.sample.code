//
//  ContactsViewController.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ContactsViewController: UIViewController, View {
    var viewModel: ContactsViewModel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupContactsUI()
        configureRefreshControl()
    }
    fileprivate func setupContactsUI() {
        guard let viewModel = self.viewModel else {
            fatalError("недоступна viewModel")
        }
        // убираем нижнюю часть таблицы (пустые полоски)
        tableView.tableFooterView = UIView()
        // добавляем обработку нажатия, для сброса клавиатуры
        let tapGestureForHideKeyboard = UITapGestureRecognizer(target: self, action: #selector(tapOnView(_:)))
        tapGestureForHideKeyboard.numberOfTapsRequired = 1
        tapGestureForHideKeyboard.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGestureForHideKeyboard)
        // подписываемся на обновление данных контактов
        viewModel.reloadingObserver.subscribe(onNext: { [weak self] (_) in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }).disposed(by: disposeBag)
        // обработка ошибок
        viewModel.contactsErrorObserver?.subscribe(onNext: { (error) in
            if let error = error as? ContactsError {
                switch error {
                case .denied:
                    // покажем алерт с переходом в сеттингс
                    self.showDeniedAlert()
                case .restricted:
                    // покажем алерт о невозможности доступа к контактам
                    self.showRestrictedAlert()
                case .isRefreshing:
                    break
                }
            }
        }).disposed(by: disposeBag)
        // обаботка фильтра поиска
        searchBar.rx.text.orEmpty
            .throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { (filter) in
                viewModel.searchFilter = filter
            }).disposed(by: disposeBag)
    }
    @objc func tapOnView(_ sender: Any?) {
        view.endEditing(true)
    }
    func showDeniedAlert() {
        let title = "Доступ к контактом ограничен"
        let message = "Хотите открыть настройки для получения доступа к контактам?"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let settingsTitle = "Открыть Настройки"
        alertController.addAction(
            UIAlertAction(title: settingsTitle, style: .default, handler: { (_) in
                self.showSettings()
            })
        )
        alertController.addAction(
            UIAlertAction(title: "Закрыть", style: .cancel, handler: { (_) in})
        )
        self.present(alertController, animated: true)
    }
    func showRestrictedAlert() {
        let title = "Доступ к контактом запрещен!"
        let message = "Доступ к контактом запрещен! Попросите администратора открыть вам доступ"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(
            UIAlertAction(title: "Закрыть", style: .cancel, handler: { (_) in})
        )
        self.present(alertController, animated: true)
    }
    func showSettings() {
        // переход в настройки
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (_) in })
        }
    }
}
