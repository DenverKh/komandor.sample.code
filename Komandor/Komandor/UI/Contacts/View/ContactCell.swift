//
//  ContactCell.swift
//  Komandor
//
//  Created by Denis Khlopin on 18/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
    var viewModel: ContactCellViewModel? {
        didSet {
            updateUI()
        }
    }
    weak var parentController: ContactsViewController?
    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelInfo: UILabel!
    @IBOutlet weak var imageViewOnlineStatus: UIImageView!
    @IBOutlet weak var buttonInvite: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateUI() {
        guard let viewModel = self.viewModel else {
            fatalError("viewModel отсутствует")
        }
        let contact = viewModel.contact
        self.buttonInvite.isHidden = (contact.cellType != .phone)
        self.imageViewLogo.setPhotoImage(data: contact.titlePhoto)
        self.imageViewLogo.clipsToBounds = true
        //self.imageViewOnlineStatus.isHidden = !contact.isOnline
        // временно убираем шарик ОНЛАЙН статуса
        self.imageViewOnlineStatus.isHidden = true
        self.labelName.text = contact.titleName
        self.labelInfo.text = contact.titleInfo
    }
    @IBAction func actionInvite(_ sender: Any) {
        guard let viewModel = self.viewModel else {
            fatalError("viewModel отсутствует")
        }
        if let controller = parentController, let contact = viewModel.contact as? Contact {
            // TODO: сделать текстовые константы через emum
            controller.sendMessageTo(contact: contact, message: "Привет, я использую Komandor для переписки. Присоединяйся! Скачать его можно здесь: ")
        }
    }
}
