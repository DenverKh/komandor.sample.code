//
//  ContactsViewController+UIRefreshControl.swift
//  Komandor
//
//  Created by Denis Khlopin on 21/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

extension ContactsViewController {
    func configureRefreshControl () {
        guard let viewModel = self.viewModel else {
            fatalError("отсутствует viewModel")
        }
        // Add the refresh control to your UIScrollView object.
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        // обработка рефреш контрола
        viewModel.contactsRefreshObserver?.subscribe(onNext: { [weak self] (isRefreshing) in
            DispatchQueue.main.async {
                if let refreshControl = self?.tableView.refreshControl {
                    if !isRefreshing {
                        refreshControl.endRefreshing()
                    }
                }
            }
        }).disposed(by: disposeBag)

    }
    @objc func handleRefreshControl() {
        guard let viewModel = self.viewModel else {
            fatalError("отсутствует viewModel")
        }
        //
        viewModel.refreshContacts()
        // Dismiss the refresh control.
        DispatchQueue.main.async {
            //self.tableView.refreshControl?.endRefreshing()
        }
    }
}
