//
//  ContactProfileViewController.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit
import RxSwift


class ContactProfileViewController: UITableViewController, View {
    var viewModel: ContactProfileViewModel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelOnlineStatus: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelCompany: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var buttonCreateChat: UIRoundButton!
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupContactProfileUI()
    }
    func setupContactProfileUI() {
        //
        updateProfileUI()
    }
    func updateProfileUI() {
        guard let viewModel = viewModel else {
            fatalError("недоступна viewModel")
        }
        guard let chatCreateService = viewModel.chatCreatorService else {
            fatalError("недоступна chatCreateService")
        }
        labelName.text = viewModel.name
        labelOnlineStatus.text = viewModel.online
        labelPhone.text = viewModel.phone
        labelCompany.text = viewModel.company
        labelTitle.text = viewModel.title
        imageViewPhoto.setPhotoImage(data: viewModel.imageData)
        // rx observers
        chatCreateService.errorObserver.subscribe(onNext: { (error) in
            if let error = error {
                print(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
        chatCreateService.dataObserver.subscribe(onNext: { (chatId) in
            DispatchQueue.main.async {
                if let chatId = chatId {
                    // выбран чат, необходимо перенестись туда, паралельно делаем возврат к списку контактов
                    self.navigationController?.popViewController(animated: false)
                    viewModel.moveToChat(chat: chatId)
                }
            }
        }).disposed(by: disposeBag)
        chatCreateService.refreshObserver.subscribe(onNext: { [weak self] (refreshing) in
            DispatchQueue.main.async {
                self?.buttonCreateChat.isEnabled = !refreshing
            }
        }).disposed(by: disposeBag)
    }
    @IBAction func actionCreateChat(_ sender: Any) {
        guard let viewModel = viewModel else {
            fatalError("недоступна viewModel")
        }
        viewModel.createChat()
    }
}
