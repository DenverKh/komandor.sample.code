//
//  ContactCellProtocol.swift
//  Komandor
//
//  Created by Denis Khlopin on 18/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
enum ContactCellType {
    case phone
    case server
    case unknown
}
/// протокол отображения контакта в ячейке
protocol ContactCellProtocol {
    var titleName: String { get }
    var titleTitle: String { get }
    var titleCompany: String { get }
    var titleInfo: String { get }
    var cellType: ContactCellType { get }
    var isOnline: Bool { get }
    var titlePhoto: Data? { get }
}
