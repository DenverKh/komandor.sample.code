//
//  ContactCellViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class ContactCellViewModel {
    var contact: ContactCellProtocol
    init(contact: ContactCellProtocol) {
        self.contact = contact
    }
}
