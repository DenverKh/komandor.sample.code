//
//  ContactProfileViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import RxSwift

class ContactProfileViewModel: ViewModel {
    var router: Router
    var contact: Contact! {
        didSet {
            setupContact()
        }
    }
    var chatCreatorService: ChatCreatorService?
    var name: String {
        return contact.titleName
    }
    var online: String {
        return "В сети"
    }
    var imageData: Data? {
        return contact.titlePhoto
    }
    var phone: String {
        if let phone = contact.phone {
            return MaskUtils.phone(phone).mask
        }
        return ""
    }
    var company: String {
        return contact.titleCompany
    }
    var title: String {
        return contact.titleTitle
    }
    init(router: Router) {
        self.router = router
    }
    func setupContact() {
        chatCreatorService = ChatCreatorService(contact: contact)
    }
    func createChat() {
        if let chatCreatorService = chatCreatorService {
            chatCreatorService.run()
        }
    }
    func moveToChat(chat: String) {
        guard let router = self.router as? ContactsRouter else {
         fatalError("wrong router")
        }
        router.route(goTo: .chat(chatId: chat))
    }
}
