//
//  ContactsViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class ContactsViewModel: ViewModel {
    var router: Router
    // мусорка
    fileprivate var disposeBag = DisposeBag()
    var user: User? {
        return UserManager.manager.currentUser
    }
    /// все доступные контакты
    fileprivate var totalContacts = [Contact]()
    /// список контактов
    var contacts = [ContactCellProtocol]()
    /// строка поиска
    var searchFilter: String = "" {
        didSet {
            updateContacts()
        }
    }
    var contactsCount: Int {
        return contacts.count
    }
    /// индикатор обновления данных
    var reloadingObserver = PublishSubject<Bool>()
    var contactsErrorObserver: BehaviorSubject<Error?>? {
        return ContactsManager.manager.contactsErrorObserver
    }
    var contactsRefreshObserver: BehaviorSubject<Bool>? {
        return ContactsManager.manager.contactsRefreshObserver
    }
    ///
    init(router: Router) {
        self.router = router
        guard let user = self.user, let certificate = user.certificate else {
            fatalError("нет авторизованного пользователя или нет сертификата пользователя!")
        }
        // загружаем контакты для нашего сертификата
        let manager = ContactsManager.manager
        manager.loadContacts(for: certificate)
        manager.contactsObserver?.subscribe(onNext: { [weak self] (contacts) in
            // обработка обновления контактов
            if let contacts = contacts {
                self?.totalContacts = contacts
                self?.updateContacts()
            }
        }).disposed(by: disposeBag)
    }
    func updateContacts() {
        contacts = totalContacts
        .filter({ (contact) -> Bool in
            return self.search(contact: contact, filter: searchFilter)
        })
        reloadingObserver.onNext(true)
    }
    // проверка на совпадение
    fileprivate func search(contact: ContactCellProtocol, filter: String) -> Bool {
        if filter.isEmpty {
            return true
        } else {
            if contact.titleName.uppercased().contains(filter.uppercased())
                || contact.titleInfo.uppercased().contains(filter.uppercased()) {
                return true
            }
            if let detailedContact = contact as? Contact {
                if let phone = detailedContact.phone, phone.uppercased().contains(filter.uppercased()) {
                    return true
                }
                for phone in detailedContact.contactPhones {
                    if phone.uppercased().contains(filter.uppercased()) {
                        return true
                    }
                }
            }
        }
        return false
    }
    func selectContact(contact: ContactCellProtocol) {
        guard let router = router as? ContactsRouter else {
            fatalError("wrong router type")
        }
        router.route(goTo: .profile(contact: contact))
    }
    func refreshContacts() {
        ContactsManager.manager.refreshContacts()
    }
}
