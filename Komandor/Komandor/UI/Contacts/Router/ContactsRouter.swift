//
//  ContactsRouter.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

class ContactsRouter: Router, HasParentRouter {
    enum Routes {
        case profile(contact: ContactCellProtocol)
        case invite(contact: ContactCellProtocol)
        case chat(chatId: String)
    }
    var baseViewController: UIViewController?
    var parent: GeneralRouter!
    init(navigationController: UINavigationController?, parent: GeneralRouter) {
        self.baseViewController = navigationController
        self.parent = parent
    }
    func start(context: Any?) {
        // просто создаем нав контроллер и в него добавляем нужный контроллер
        // настраиваем иконку и т.п.
        if let controller: ContactsViewController = Storyboards.getController(from: .contacts) {
            let viewModel = ContactsViewModel(router: self)
            controller.viewModel = viewModel
            let navigationController = UINavigationController(rootViewController: controller)
            let iconImage = #imageLiteral(resourceName: "contacts-icon")
            navigationController.tabBarItem = UITabBarItem(title: "Контакты", image: iconImage, tag: 0)
            self.baseViewController = navigationController
        }
    }

    func route(context: Any?) {
        guard let route = context as? Routes else { return }
        // check route here
        switch route {
        case .profile(let contact):
            showProfile(contact: contact)
        case .invite(let contact):
            showInviteMessage(contact: contact)
        case .chat(let chatId):
            showChat(chatId: chatId)
        }
    }
    func showProfile(contact: ContactCellProtocol) {
        guard let contact = contact as? Contact else {
            return
        }
        switch contact.cellType {
        case .phone:
            //
            break
        case .server:
            // show server profile
            if  let navigationController = self.navigationController,
                let controller: ContactProfileViewController = Storyboards.getController(from: .contacts) {
                let viewModel = ContactProfileViewModel(router: self)
                viewModel.contact = contact
                controller.viewModel = viewModel
                navigationController.pushViewController(controller, animated: true)
            }
        case .unknown:
            break
        }
    }
    ///
    func showChat(chatId: String) {
        // TODO: создание и переход на чат с этим контактом
        if let generalRouter = parent {
            generalRouter.moveToChat(chatId: chatId)
        }
    }
    ///
    func showInviteMessage(contact: ContactCellProtocol) {
        // empty
    }
}

extension ContactsRouter {
    func route(goTo: Routes) {
        route(context: goTo)
    }
}
