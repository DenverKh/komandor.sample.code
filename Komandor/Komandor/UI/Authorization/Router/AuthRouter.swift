//
//  AuthRouter.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit

class AuthRouter: Router {
    enum Routes {
        case start
        case certificateList
        case noCertificate
        case login(certificate: Certificate)
        case phone(certificate: Certificate)
        case phoneCode(certificate: Certificate, pnone: String)
        case main
        case profile
    }
    var baseViewController: UIViewController?
    init(navigationController: UINavigationController?) {
        self.baseViewController = navigationController
    }
    func start(context: Any?) {
        // OLD LOGIC
        // FIXME: Заменить на логику поиска доступных сертификатов
        //let isSertificatesExist = CertificatesManager.manager.isAvailableCertificates()
        //На старте мы смотрим, есть ли у нас сертификаты
        //Если сертификаты есть, то стартовое выводим окно со списком сертификатов
        //Если нет, то выводим окно запроса сертификата
        //Создаем навигейшн для перехода меду окнами
        // present first controller here
        /*
        var firstController: UIViewController?
        if isSertificatesExist {
            let viewModel = AuthCertificatesListViewModel(router: self)
            if let controller: AuthCertificatesListViewController = Storyboards.getController(from: .auth) {
                controller.viewModel = viewModel
                firstController = controller
            }
        } else {
            let viewModel = AuthNoCertificateViewModel(router: self)
            if let controller: AuthNoCertificateViewController = Storyboards.getController(from: .auth) {
                controller.viewModel = viewModel
                firstController = controller
            }
        }*/
        //var firstController: UIViewController?
        let viewModel = AuthCertificatesViewModel(router: self)
        if let delegate = UIApplication.shared.delegate as? AppDelegate,
            let controller: AuthCertificatesViewController = Storyboards.getController(from: .auth) {
            controller.viewModel = viewModel
            let navigationController = UINavigationController(rootViewController: controller)
            self.baseViewController = navigationController
            delegate.switchRootViewController(toVC: navigationController)
        }
    }

    fileprivate func showLogin(_ certificate: Certificate) {
        if let navigationController = self.navigationController,
            let controller: AuthLoginViewController = Storyboards.getController(from: .auth) {
            let viewModel = AuthLoginViewModel(router: self, certificate: certificate)
            controller.viewModel = viewModel
            navigationController.setBackButtonTitle("")
            navigationController.pushViewController(controller, animated: true)
        }
    }
    fileprivate func showPhoneRegister(_ certificate: Certificate) {
        if let navigationController = self.navigationController,
            let controller: AuthPhoneViewController = Storyboards.getController(from: .auth) {
            let viewModel = AuthPhoneViewModel(router: self, certificate: certificate)
            controller.viewModel = viewModel
            navigationController.setBackButtonTitle("")
            navigationController.pushViewController(controller, animated: true)
        }
    }
    fileprivate func showPhoneConfirmationCode(_ certificate: Certificate, phone: String) {
        if let navigationController = self.navigationController,
            let controller: AuthPhoneCodeViewController = Storyboards.getController(from: .auth) {
            let viewModel = AuthPhoneCodeViewModel(router: self, certificate: certificate)
            viewModel.phone = phone
            controller.viewModel = viewModel
            navigationController.setBackButtonTitle("")
            navigationController.pushViewController(controller, animated: true)
        }
    }
    func route(context: Any?) {
        guard let route = context as? Routes else { return }
        // check route here
        switch route {
        case .certificateList:
            break
        case .noCertificate:
            break
        case .start:
            break
        case .login(let certificate):
            showLogin(certificate)
        case .phone(let certificate):
            showPhoneRegister(certificate)
        case .phoneCode(let certificate, let phone):
            showPhoneConfirmationCode(certificate, phone: phone)
        case .main:
            break
        case .profile:
            break
        }
    }
}

extension AuthRouter {
    func route(goTo: Routes) {
        route(context: goTo)
    }
}
