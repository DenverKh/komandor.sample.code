//
//  AuthCertificatesViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 09/01/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class AuthCertificatesViewModel: ViewModel {
    var router: Router
    var certificates = [Certificate]()
    var disposeBag = DisposeBag()
    init(router: Router) {
        self.router = router
        CertificatesManager.manager.sertificatesObserver.subscribe(onNext: { [weak self] (certificates) in
            self?.certificates = certificates
            self?.isCertificatesAvailable.onNext(certificates.count > 0)
        }).disposed(by: disposeBag)
    }
    var isCertificatesAvailable = BehaviorSubject<Bool>(value: false)
    func getCertificate() {
        // MARK: Переход на сайт получения сертификата
        let certificateUrl = "https://ucsouz.ru/"
        certificateUrl.openURLString()
    }
    func loadCertificate() {
        // TODO: Загрузка сертификата из iCloud или локальных файлов        
    }
    func selectCertificate(_ certificate: Certificate) {
        if let router = self.router as? AuthRouter {
            router.route(goTo: .login(certificate: certificate))
        }
    }
}
