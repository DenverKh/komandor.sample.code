//
//  AuthPhoneViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class AuthPhoneViewModel: ViewModel {
    var router: Router
    private var _phone: String = ""
    var phone: String {
        get {
            return _phone
        }
        set {
            _phone = newValue
            checkPhoneValid()
        }
    }
    let certificate: Certificate!
    var phoneValidObserver = PublishSubject<Bool>()
    var authService: APIAuthService?
    var sendCodeTask: IntervalTask?
    var errorObserver = PublishSubject<Error>()
    init(router: Router, certificate: Certificate) {
        self.router = router
        self.certificate = certificate
    }
    func getCode() {
        // инициализация и выполнение запроса на отправку СМС с кодом, если такой телефон зареген, то выводим сообщение
        initSendCodeTask()
        sendCode()
    }
    func initSendCodeTask() {
        sendCodeTask = IntervalTaskManager.manager.getTask(withName: phone, interval: 60, runCode: { [weak self] (task) -> Bool in
            guard   let certificate = self?.certificate,
                let token = SecureStorage.string(service: "token", key: certificate.name),
                let phone = self?.phone,
                let router = self?.router as? AuthRouter
                else {
                    fatalError("невозможно получить токен из хранилища")
            }
            // удаляем предыдущий идентификатор
            _ = SecureStorage.delete(service: "phoneIdentifier", key: certificate.name)
            //
            //print("send sms to client \(self?.code ?? "")")
            self?.authService = APIAuthService(certificate: certificate)
            self?.authService?.registerPhone(phone: phone, token: token, completion: { (error, phoneIdentifier) in
                DispatchQueue.main.async {
                    if let error = error {
                        print(error.localizedDescription)
                        if let apiError = error as? APIError {
                            switch apiError {
                            case .phoneAlreadyRegistered:
                                //print(apiError.localizedDescription)
                                self?.errorObserver.onNext(apiError)
                            default:
                                break
                            }
                        }
                        task.cancel()
                    } else {
                        _ = SecureStorage.setValue(service: "phoneIdentifier", key: certificate.name, value: phoneIdentifier)
                        router.route(goTo: .phoneCode(certificate: certificate, pnone: phone))
                    }
                }
            })
            return true
        })
    }
    func sendCode() {
        if let task = sendCodeTask {
            _ = task.execute()
        }
    }
    func checkPhoneValid() {
        phoneValidObserver.onNext(phone.count == 11)
        print("check validation: is ")
    }
    /*
    // update phone
    func phoneMaskUpdate(inPhone: String) -> (mask: String, numbersOnly: String) {
        var result = inPhone.filter {
            "01234567890".contains($0)
        }
        if result.count > 11 {
            result = String(result.prefix(11))
        }
        // check first symbol
        if result.count > 0 {
            if result.prefix(1) != "7" {
                if result.prefix(1) == "8" {
                    result.replaceSubrange(result.startIndex..<result.index(result.startIndex, offsetBy: 1), with: "7")
                } else {
                    result = "7" + result
                }
            }
            result = "+" + result
            if result.count > 2 {
                result.insert(" ", at: result.index(result.startIndex, offsetBy: 2))
            }
            if result.count > 6 {
                result.insert(" ", at: result.index(result.startIndex, offsetBy: 6))
            }
            if result.count > 10 {
                result.insert("-", at: result.index(result.startIndex, offsetBy: 10))
            }
            if result.count > 13 {
                result.insert("-", at: result.index(result.startIndex, offsetBy: 13))
            }
        }
        return (mask: result, numbersOnly: result.filter { "01234567890".contains($0) })
    }*/
}
