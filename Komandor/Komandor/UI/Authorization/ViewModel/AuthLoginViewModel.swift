//
//  AuthLoginViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class AuthLoginViewModel: ViewModel {
    var router: Router
    weak var certificate: Certificate?
    var password: String = ""
    var loginErrorObserver = PublishSubject<Error>()
    var authService: APIAuthService?
    var certificatePasswordService: CertificatePasswordService?
    init(router: Router) {
        self.router = router
        certificate = nil
    }
    init(router: Router, certificate: Certificate) {
        self.router = router
        self.certificate = certificate
    }
    func login() {
        // Авторизация пользователя по паролю
        if let router = self.router as? AuthRouter {
            checkLogin(onSuccess: { [weak self] in
                CertificatesManager.manager.currentCertificate = self?.certificate
                // создаем пользователя
               _ = UserManager.manager.authCurrentUser(certificate: CertificatesManager.manager.currentCertificate!)
                RouteManager.manager.startGeneralFlow()
            }, onError: { [weak self] (error) in
                if let error = error {
                    self?.loginErrorObserver.onNext(error)
                    print(error.localizedDescription)
                }
            }, onNoPhoneAdded: { [weak self] in
                CertificatesManager.manager.currentCertificate = self?.certificate
                router.route(goTo: .phone(certificate: (self?.certificate)!))
            })
        }
    }
    ///проверка авторизации
    func checkLogin(onSuccess: @escaping () -> Void, onError: @escaping (Error?) -> Void, onNoPhoneAdded: @escaping () -> Void) {
        guard let certificate = self.certificate else {
            onError(AuthError.wrongCertificate(description: "Отсутствует сертификат"))
            return
        }
        // проверяем ПИН код от контейнера
        certificatePasswordService = CertificatePasswordService(certificate: certificate)
        if certificatePasswordService!.verifyPassword(password: password) {
            // авторизацию сертификата прошли, теерь отправляем запрос на сервер
            authService = APIAuthService(certificate: certificate)
            authService?.auth { (error, token, needToRegisterPhone) in
                DispatchQueue.main.async {
                    if let error = error {
                        // обработка ошибок
                        print(error.localizedDescription)
                    } else {
                        guard let token = token, let needToRegisterPhone = needToRegisterPhone else {
                            onError(CustomError.customError(error: "Не получен параметер token или needToRegisterPhone "))
                            return
                        }
                        /// сохраняем токен
                        _ = SecureStorage.setString(service: "token", key: certificate.name, value: token)
                        if needToRegisterPhone {
                            onNoPhoneAdded()
                        } else {
                            onSuccess()
                        }
                    }
                }
            }
        } else {
            onError(AuthError.wrongPassword)
        }
    }
}
