//
//  AuthPhoneCodeViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class AuthPhoneCodeViewModel: ViewModel {
    var router: Router
    fileprivate var _code: String = ""
    var code: String {
        get {
            return _code
        }
        set {
            _code = newValue
            validateCode()
        }
    }
    var isValideCodeObserver = BehaviorSubject<Bool>(value: false)
    var confirmationCodeObserver = PublishSubject<Error?>()
    var phone: String = "" {
        didSet {
            initSendCodeTask()
        }
    }
    let certificate: Certificate!
    var sendCodeTask: IntervalTask!
    var authService: APIAuthService?
    init(router: Router, certificate: Certificate) {
        self.router = router
        self.certificate = certificate
    }
    func validateCode() {
        let identifier: Int? = SecureStorage.value(service: "phoneIdentifier", key: certificate.name)
        let result = code.count == 6 && identifier != nil
        isValideCodeObserver.onNext(result)
    }
    func initSendCodeTask() {
        sendCodeTask = IntervalTaskManager.manager.getTask(withName: phone, interval: 60, runCode: { [weak self] (task) -> Bool in
            guard   let certificate = self?.certificate,
                    let token = SecureStorage.string(service: "token", key: certificate.name),
                    let phone = self?.phone
            else {
                    fatalError("невозможно получить токен из хранилища")
            }
            // отправка СМС на телефон
            // удаляем предыдущий идентификатор
            _ = SecureStorage.delete(service: "phoneIdentifier", key: certificate.name)
            //
            //print("send sms to client \(self?.code ?? "")")
            self?.authService = APIAuthService(certificate: certificate)
            self?.authService?.registerPhone(phone: phone, token: token, completion: { (error, phoneIdentifier) in
                if let error = error {
                    print(error.localizedDescription)
                    task.cancel()
                } else {
                    DispatchQueue.main.async {
                        _ = SecureStorage.setValue(service: "phoneIdentifier", key: certificate.name, value: phoneIdentifier)
                        //router.route(goTo: .phoneCode(certificate: certificate, pnone: (self?.phone)!))
                        self?.validateCode()
                    }
                }
            })
            return true
        })
    }
    func confirmCode() {
        guard   let certificate = self.certificate,
                let token = SecureStorage.string(service: "token", key: certificate.name),
                let phoneIdentifier: Int = SecureStorage.value(service: "phoneIdentifier", key: certificate.name)
            else {
                fatalError("невозможно получить токен из хранилища")
        }
        // проверяем код, отправленный через СМС
        authService = APIAuthService(certificate: certificate)
        authService?.confirmPhone(code: code, phoneIdentifier: phoneIdentifier, token: token, completion: { [weak self] (error) in
            DispatchQueue.main.async {
                if let error = error {
                    print(error.localizedDescription)
                    if let apiError = error as? APIError {
                        switch apiError {
                        case .incorrectCode:
                            // обработка ошибки!
                            self?.confirmationCodeObserver.onNext(ConfirmationCodeError.wrongCode)
                        case .phoneAlreadyRegistered:
                            self?.confirmationCodeObserver.onNext(error)
                        default:
                            break
                        }
                    }
                } else {
                    /// route to profile here
                    print("route to profile page!!!")
                    self?.confirmationCodeObserver.onNext(nil)
                    if let router = self?.router as? AuthRouter {
                        router.route(goTo: .profile)
                    }
                }
            }
        })
    }
    func sendCode() {
        if let task = sendCodeTask {
            _ = task.execute()
        }
    }
    // update code
    func codeMaskUpdate(inCode: String) -> (mask: String, numbersOnly: String) {
        var result = inCode.filter {
            "01234567890".contains($0)
        }
        if result.count > 6 {
            result = String(result.prefix(6))
        }
        // check first symbol
        if result.count > 3 {
            result.insert(" ", at: result.index(result.startIndex, offsetBy: 3))
        }
        return (mask: result, numbersOnly: result.filter { "01234567890".contains($0) })
    }
}
