//
//  AuthPhoneCodeViewController.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AuthPhoneCodeViewController: UIViewController, View {
    var viewModel: AuthPhoneCodeViewModel!
    var disposeBug = DisposeBag()
    private var errorHeightConstant: CGFloat = 0.0
    //
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var confirmButton: UIRoundButton!
    @IBOutlet weak var sendAgainButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var errorHeight: NSLayoutConstraint!
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAuthPhoneCodeUI()
    }
    fileprivate func setupAuthPhoneCodeUI() {
        codeTextField.text = ""
        errorHeightConstant = errorHeight.constant
        showError(nil)
        let tapGestureForHideKeyboard = UITapGestureRecognizer(target: self, action: #selector(tapOnView(_:)))
        tapGestureForHideKeyboard.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGestureForHideKeyboard)
        viewModel.isValideCodeObserver.subscribe(onNext: { [weak self] (isValid) in
            self?.confirmButton.isEnabled = isValid
            self?.confirmButton.alpha = isValid ? 1.0 : 0.5
        }).disposed(by: disposeBug)
        //
        // confirmButton push button
        confirmButton.rx.tap.bind { [weak self] in
            self?.showError(nil, animated: true)
            self?.viewModel.confirmCode()
        }.disposed(by: disposeBug)
        // sendAgainButton push button
        sendAgainButton.rx.tap.bind { [weak self] in
            self?.viewModel.sendCode()
        }.disposed(by: disposeBug)
        // send button enabled handle
        if let task = viewModel.sendCodeTask {
            // subscribe to event available is changed
            task.availableObserver.subscribe(onNext: { [weak self] (isAvailable) in
                self?.sendAgainButton.isEnabled = isAvailable
                self?.sendAgainButton.alpha = isAvailable ? 1.0 : 0.5
            }).disposed(by: disposeBug)
            // subscribe to event how much time is elapsed
            task .timerObserver.subscribe(onNext: { [weak self] (leftTime) in
                let time = Int(leftTime)
                let title = time <= 0 ?
                    "Выслать код еще раз" :
                    "Выслать код еще раз через \(time) сек"
                self?.sendAgainButton.setTitle(title, for: .normal)
            }).disposed(by: disposeBug)
        }
        //error handler
        viewModel.confirmationCodeObserver.subscribe(onNext: { [weak self] (error) in
            if let error = error {
                self?.showError(error, animated: true)
            } else {
                // it's ok!
            }
        }).disposed(by: disposeBug)
    }
    // error functions
    func showError(_ error: Error?, animated: Bool = false) {
        if let error = error {
            errorLabel.text = error.localizedDescription
            errorHeight.constant = errorHeightConstant
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    self?.view.layoutIfNeeded()
                    }, completion: { [weak self] (_) in
                        self?.errorHeight.constant = self?.errorHeightConstant ?? 0.0
                })
            }
        } else if errorHeight.constant != 0.0 {
            errorLabel.text = ""
            errorHeight.constant = 0.0
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    self?.view.layoutIfNeeded()
                    }, completion: { [weak self] (_) in
                        self?.errorHeight.constant = 0.0
                })
            }
        }
    }
    @objc func tapOnView(_ sender: Any?) {
        view.endEditing(true)
    }
}

extension AuthPhoneCodeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == codeTextField {
            self.showError(nil, animated: true)
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let result = viewModel.codeMaskUpdate(inCode: newString)
            textField.text = result.mask
            viewModel.code = result.numbersOnly
            return false
        } else {
            return true
        }
    }
}
