//
//  CertificatePersonalView.swift
//  Komandor
//
//  Created by Denis Khlopin on 25/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit

class CertificateCompanyView: CertificateView {
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyInfoLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame, certificateType: .company)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.certificateType = .company
    }
    override func updateUI() {
        guard let certificate = self.certificate else {
            return
        }
        companyNameLabel.text = certificate.companyName
        companyInfoLabel.text = "\(certificate.fullName)\n"
            + "\(certificate.post)\n"
            + "ИНН \(certificate.inn) ОГРН \(certificate.ogrn)\n"
            + "СНИЛС \(certificate.snils)"
        dateLabel.text = certificate.finishDateStr
    }
}
