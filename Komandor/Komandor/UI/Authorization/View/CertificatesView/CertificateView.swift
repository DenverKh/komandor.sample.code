//
//  CertificateView.swift
//  Komandor
//
//  Created by Denis Khlopin on 28/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit

class CertificateView: UIView {
    @IBOutlet var view: UIView!
    var certificateType: CertificateType!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.certificateType = CertificateType.unknown
        setup()
    }
    init(frame: CGRect, certificateType: CertificateType) {
        super.init(frame: frame)
        self.certificateType = certificateType
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.certificateType = CertificateType.unknown
        setup()
    }
    var certificate: Certificate? {
        didSet {
            updateUI()
        }
    }
    func updateUI() {
    }
    func setup() {
        Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,
                                 UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
        view.wrapInSuperview()
        clipsToBounds = true
    }
}
