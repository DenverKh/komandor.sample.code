//
//  CertificatePersonalView.swift
//  Komandor
//
//  Created by Denis Khlopin on 25/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit

class CertificatePersonalView: CertificateView {
    @IBOutlet weak var personNameLabel: UILabel!
    @IBOutlet weak var innSnilsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame, certificateType: .personal)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.certificateType = .personal
    }
    override func updateUI() {
        guard let certificate = self.certificate else {
            return
        }
        personNameLabel.text = certificate.fullName
        innSnilsLabel.text = "ИНН \(certificate.inn) СНИЛС \(certificate.snils)"
        dateLabel.text = certificate.finishDateStr
    }
}
