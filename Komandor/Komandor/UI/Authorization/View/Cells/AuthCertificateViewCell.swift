//
//  AuthCertificateViewCell.swift
//  Komandor
//
//  Created by Denis Khlopin on 28/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit

class AuthCertificateViewCell: UITableViewCell {
    var certificate: Certificate? {
        didSet {
            setupSertificate()
        }
    }
    var certificateView: CertificateView?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func setupSertificate() {
        if let certificate = certificate {
            if self.certificateView == nil {
                switch certificate.type {
                case .unknown:
                    break
                case .personal:
                    self.certificateView = CertificatePersonalView(frame: contentView.frame)
                case .individual:
                    self.certificateView = CertificateIndividualView(frame: contentView.frame)
                case .company:
                    self.certificateView = CertificateCompanyView(frame: contentView.frame)
                }
            }
            if let certificateView = self.certificateView {
                certificateView.certificate = certificate
                contentView.addSubview(certificateView)
                certificateView.wrapInSuperview()
            }
        }
    }
}
