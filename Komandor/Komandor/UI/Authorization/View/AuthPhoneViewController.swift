//
//  AuthPhoneViewController.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AuthPhoneViewController: UIViewController, View {
    var viewModel: AuthPhoneViewModel!
    let disposeBag = DisposeBag()
    var isGetCodeButtonValid = false
    //
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var getCodeButton: UIButton!
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAuthPhoneUI()
    }
    @IBAction func getCodeAction(_ sender: Any) {
        if isGetCodeButtonValid {
            viewModel.getCode()
        }
    }
    fileprivate func setupAuthPhoneUI() {
        //phone textfield binding
        phoneTextField.text = ""
        viewModel.phoneValidObserver.subscribe(onNext: { [weak self] (isValid) in
            self?.isGetCodeButtonValid = isValid
            self?.getCodeButton.isEnabled = isValid
            self?.getCodeButton.alpha = isValid ? 1.0 : 0.5
        }).disposed(by: disposeBag)
        // вывод ошибок
        viewModel.errorObserver.asObserver().subscribe(onNext: { (error) in
            let alert = UIAlertController(title: "Ошибка", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: { (_) in
            }))
            self.present(alert, animated: true)
        }).disposed(by: disposeBag)
        phoneTextField.becomeFirstResponder()
    }
}

extension AuthPhoneViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField {
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let result = MaskUtils.phone(newString)
            textField.text = result.mask
            viewModel.phone = result.numbersOnly
            return false
        } else {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.phoneTextField {
            self.getCodeAction(textField)
            textField.resignFirstResponder()
            return false
        }
        return true
    }
}
