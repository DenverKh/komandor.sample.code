//
//  AuthCertificatesViewController.swift
//  Komandor
//
//  Created by Denis Khlopin on 09/01/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AuthCertificatesViewController: UIViewController, View {
    var viewModel: AuthCertificatesViewModel!
    fileprivate let disposeBag = DisposeBag()
    //
    @IBOutlet weak var buttonGetCertificate: UIButton!
    @IBOutlet weak var buttonLoadCertificate: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var certificatesView: UIView!
    @IBOutlet weak var noCertificatesView: UIView!
    // временная переменная для хранения цвета кнопок навбара
    var navBarItemsColor: UIColor?
    //
    var fileSharing: FileSharing?
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        // инициализация класса для шаринга сертификатов(установка из iTunes и тп)
        fileSharing = FileSharing(parentController: self)
        setupAuthCertificatesUI()
    }
    fileprivate func setupAuthCertificatesUI() {
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        viewModel.isCertificatesAvailable.subscribe(onNext: { [weak self] (isAvailable) in
            self?.certificatesView.isHidden = !isAvailable
            self?.noCertificatesView.isHidden = isAvailable
        }).disposed(by: disposeBag)
        //rx
        buttonGetCertificate.rx.tap.bind { [weak self] in
            self?.viewModel.getCertificate()
        }.disposed(by: disposeBag)
        buttonLoadCertificate.rx.tap.bind { [weak self] in
            self?.loadCertificate()
            }.disposed(by: disposeBag)
        CertificatesManager.manager.sertificatesObserver.subscribe(onNext: { [weak self] (_) in
            self?.tableView.reloadData()
        }).disposed(by: disposeBag)
    }
    /// обработка кнопки Загрузить сертификат
    func loadCertificate() {
        guard let fileSharing = self.fileSharing else {
            return
        }
        if !fileSharing.installFromITunes(completion: {
           CertificatesManager.manager.reLoad()
        }) {
            viewModel.loadCertificate()
            let documentPickerController = UIDocumentPickerViewController(documentTypes: ["public.archive"], in: .import)
            navBarItemsColor = UINavigationBar.appearance().tintColor
            // меняем цвет кнопок нав бара на дефолтный на время показа пикер контроллера
            setNavBarTint(color: self.view.tintColor)
            documentPickerController.delegate = self
            present(documentPickerController, animated: false)
        }
    }
    /// функция замены цвета кнопок нав бара
    func setNavBarTint(color: UIColor?) {
        if let color = color {
            navBarItemsColor = UINavigationBar.appearance().tintColor
            UINavigationBar.appearance().tintColor = color
        } else {
            if let defaultColor = navBarItemsColor {
                UINavigationBar.appearance().tintColor = defaultColor
            }
            navBarItemsColor = nil
        }
    }
}
extension AuthCertificatesViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if let zipUrl = urls.first, let fileSharing = self.fileSharing {
            fileSharing.installFromZipUrl(zipUrl: zipUrl) {
                //self.tableView.reloadData()
                CertificatesManager.manager.reLoad()
            }
        }
        setNavBarTint(color: nil)
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        setNavBarTint(color: nil)
    }
}

extension AuthCertificatesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let certificate = getCertificate(indexPath: indexPath) else {
            return
        }
        viewModel.selectCertificate(certificate)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // MARK: удаление сертификата
            if let certificate = getCertificate(indexPath: indexPath) {
                let alertController = UIAlertController(
                                            title: "Удалить сертификат?",
                                            message: "Вы действительно желаете удалить выбранный сертификат?",
                                            preferredStyle: .alert)
                alertController.addTextField { (textField) in
                    textField.placeholder = "Введите пароль для сертификата"
                    textField.isSecureTextEntry = true
                }
                alertController.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { (_) in
                    if let textField = alertController.textFields?[0], let password = textField.text {
                        let passwordService = CertificatePasswordService(certificate: certificate)
                        if passwordService.verifyPassword(password: password) {
                            CertificatesManager.manager.delete(certificate: certificate)
                        } else {
                            // неверный пароль
                            let alertWrongPasswordController = UIAlertController(
                                title: "Неверный пароль!",
                                message: "Пароль введен не верно!",
                                preferredStyle: .alert)
                            alertWrongPasswordController.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: { (_) in
                                _ = passwordService.clearPin()
                            }))
                            self.present(alertWrongPasswordController, animated: true, completion: nil)
                        }
                    }
                }))
                alertController.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: { (_) in
                }))
                self.present(alertController, animated: true) {}
            }
        }
    }
}

extension AuthCertificatesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.certificates.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let certificate = getCertificate(indexPath: indexPath) else {
            return UITableViewCell()
        }
        switch certificate.type {
        case .unknown:
            break
        case .personal, .company, .individual:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "certificate") as? AuthCertificateViewCell {
                cell.selectionStyle = .none
                cell.certificate = certificate
                return cell
            }
        }
        return UITableViewCell()
    }
    func getCertificate(indexPath: IndexPath) -> Certificate? {
        let index = indexPath.row
        if index >= 0 && index < viewModel.certificates.count {
            return viewModel.certificates[index]
        }
        return nil
    }
}
