//
//  AuthLoginViewController.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AuthLoginViewController: UIViewController, View {
    var viewModel: AuthLoginViewModel!
    var certView: CertificateView?
    fileprivate var errorHeightConstant: CGFloat = 0.0
    var disposeBag = DisposeBag()
    //
    @IBOutlet weak var certificateView: UIView!
    @IBOutlet weak var certificateViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var errorHeight: NSLayoutConstraint!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var scrollView: AutoScrollableView!
    @IBOutlet weak var loginButton: UIButton!
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAuthLoginUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // show keyboard directly
        passwordTextfield.becomeFirstResponder()
    }
    // initial setup for UI and rx binding here
    fileprivate func setupAuthLoginUI() {
        //save designed constant and hide error label
        errorHeightConstant = errorHeight.constant
        showError(nil)
        if let certificate = viewModel.certificate {
            switch certificate.type {
            case .unknown:
                break
            case .personal:
                certView = CertificatePersonalView(frame: .zero)
            case .individual:
                certView = CertificateIndividualView(frame: .zero)
            case .company:
                certView = CertificateCompanyView(frame: .zero)
            }
            if let certView = self.certView {
                certificateView.removeConstraint(certificateViewHeightConstraint)
                certView.certificate = certificate
                certificateView.addSubview(certView)
                certView.wrapInSuperview()
            }
        }
        //passwordTextfield.keyboardAppearance = .dark
        let tapGestureForHideKeyboard = UITapGestureRecognizer(target: self, action: #selector(tapOnView(_:)))
        tapGestureForHideKeyboard.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGestureForHideKeyboard)
        scrollView.alwaysVisibleView = loginButton
        //rx binding
        passwordTextfield.rx.text.orEmpty
        .subscribe(onNext: { [weak self] (text) in
            self?.viewModel.password = text
            self?.showError(nil, animated: true)
        }).disposed(by: disposeBag)
        // on press DONE button event handler
        passwordTextfield.rx.controlEvent([.editingDidEndOnExit]).subscribe(onNext: { [weak self] () in
            self?.passwordTextfield.resignFirstResponder()
            self?.showError(nil, animated: false)
            self?.viewModel.login()
        }).disposed(by: disposeBag)
        // error handling
        viewModel.loginErrorObserver.subscribe(onNext: { [weak self] (error) in
            self?.showError(error, animated: true)
        }).disposed(by: disposeBag)
    }
    // action login button
    @IBAction func login(_ sender: Any?) {
        showError(nil, animated: false)
        viewModel.login()
    }
    // error functions
    func showError(_ error: Error?, animated: Bool = false) {
        if let error = error {
            errorLabel.text = error.localizedDescription
            errorHeight.constant = errorHeightConstant
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    self?.view.layoutIfNeeded()
                }, completion: { [weak self] (_) in
                    self?.errorHeight.constant = self?.errorHeightConstant ?? 0.0
                })
            }
        } else if errorHeight.constant != 0.0 {
            errorLabel.text = ""
            errorHeight.constant = 0.0
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    self?.view.layoutIfNeeded()
                }, completion: { [weak self] (_) in
                    self?.errorHeight.constant = 0.0
                })
            }
        }
    }
    @objc func tapOnView(_ sender: Any?) {
        view.endEditing(true)
    }
}
