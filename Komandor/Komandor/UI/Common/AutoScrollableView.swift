//
//  AutoScrollableView.swift
//  auroom_alpha
//
//  Created by Jeanna Golenkova on 18.04.18.
//  Copyright © 2018 Reason8, Inc. All rights reserved.
//

import UIKit

class AutoScrollableView: UIScrollView, UITextFieldDelegate {
    var alwaysVisibleView: UIView?
    fileprivate var activeTextField: UITextField?
    fileprivate var fixedContentOffset: CGPoint?
    fileprivate let kHideKeyboardAnimationDuratin = 0.25
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureAutoScrollableView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureAutoScrollableView()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        configureAutoScrollableView()
    }
    fileprivate func configureAutoScrollableView() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AutoScrollableView.keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AutoScrollableView.keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        let tapGeasture = UITapGestureRecognizer.init(target: self, action: #selector(AutoScrollableView.dismissKeyboard))
        self.addGestureRecognizer(tapGeasture)
        fixedContentOffset = contentOffset
    }
    // MARK: Handle Keyboard Notifications
    @objc func keyboardWillShow(_ notification: Notification) {
        fixedContentOffset = self.contentOffset
        var activeView = alwaysVisibleView
        if activeView == nil {
            activeView = activeTextField
        }
        guard activeView != nil else {
            return
        }
        let kbSize = ((notification as NSNotification).userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        guard kbSize != nil else {
            return
        }
        let contentOffset = self.contentOffset
        let contentInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: kbSize!.height, right: 0)
        var aRect = self.bounds
        aRect.size.height -= kbSize!.height
        let  activeViewFrame = activeView!.frame
        let bottomPointOfActiveView = CGPoint(x: activeViewFrame.origin.x - contentOffset.x,
                                              y: activeViewFrame.maxY - contentOffset.y)
        if aRect.contains(bottomPointOfActiveView) {
            self.scrollRectToVisible(activeViewFrame, animated: true)
        } else {
            self.contentInset = contentInsets
            self.scrollIndicatorInsets = contentInsets
            self.scrollRectToVisible(activeViewFrame, animated: true)
        }
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        UIView.animate(withDuration: kHideKeyboardAnimationDuratin, animations: {
            if !self.isScrollEnabled {
                self.contentOffset = self.fixedContentOffset!
            }
            let contentInsets = UIEdgeInsets.zero
            self.contentInset = contentInsets
            self.scrollIndicatorInsets = contentInsets
        })
    }
    // MARK: UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    // MARK: Target Actions
    @objc func dismissKeyboard() {
        self.endEditing(true)
        activeTextField?.resignFirstResponder()
    }
    // MARK: deinit
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
