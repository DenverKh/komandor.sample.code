//
//  UIRoundButton.swift
//  Komandor
//
//  Created by Denis Khlopin on 24/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit
/// класс кнопки с закругленными углами
@IBDesignable class UIRoundButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 22.5 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    override func prepareForInterfaceBuilder() {
        initialize()
    }
    final func initialize() {
        refreshCorners(value: cornerRadius)
    }
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
}
