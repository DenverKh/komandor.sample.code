//
//  SettingsRouter.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

class SettingsRouter: Router, HasParentRouter {
    enum Routes {
        case profile
    }
    var baseViewController: UIViewController?
    //
    var parent: GeneralRouter!
    init(navigationController: UINavigationController?, parent: GeneralRouter) {
        self.baseViewController = navigationController
        self.parent = parent
    }
    //
    func start(context: Any?) {
        if let controller: SettingsViewController = Storyboards.getController(from: .settings) {
            let viewModel = SettingsViewModel(router: self)
            controller.viewModel = viewModel
            let navigationController = UINavigationController(rootViewController: controller)
            let iconImage = #imageLiteral(resourceName: "settings-icon")
            navigationController.tabBarItem = UITabBarItem(title: "Настройки", image: iconImage, tag: 2)
            self.baseViewController = navigationController
        }
    }
    func route(context: Any?) {
        guard let route = context as? Routes else { return }
        switch route {
        case .profile:
            openProfile()
        }
    }
    //
    fileprivate func openProfile() {
        let profileRouter = ProfileRouter(navigationController: self.navigationController)
        profileRouter.start(context: nil)
    }
}
//
extension SettingsRouter {
    func route(goTo: Routes) {
        route(context: goTo)
    }
}
