//
//  SettingsViewController.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MessageUI

class SettingsViewController: UITableViewController, View {
    var viewModel: SettingsViewModel!
    let disposeBag = DisposeBag()
    //
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var themeTitleLabel: UILabel!
    @IBOutlet weak var lightThemeSwitch: UISwitch!
    @IBOutlet weak var feedbackLabel: UILabel!
    @IBOutlet weak var aboutAppLabel: UILabel!
    @IBOutlet weak var userProfileCell: UITableViewCell!
    @IBOutlet weak var feedbackCell: UITableViewCell!
    @IBOutlet weak var aboutAppCell: UITableViewCell!
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSettingsUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        guard let viewModel = self.viewModel else {
            fatalError("viewModel не определена!")
        }
        viewModel.refreshUser()
    }
    //
    fileprivate func setupSettingsUI() {
        guard let viewModel = self.viewModel else {
            fatalError("viewModel не определена!")
        }
        viewModel.userInfoDataObserver?.subscribe(onNext: { [weak self]  (_) in
            guard let controller = self else {
                return
            }
            DispatchQueue.main.async {
                controller.userNameLabel.text = viewModel.name
                controller.userAvatarImageView.setPhotoImage(data: viewModel.imagePhotoData)
            }
        }).disposed(by: disposeBag)
    }
}
//
extension SettingsViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
        if selectedCell == userProfileCell {
            self.viewModel.openUserProfile()
        } else if selectedCell == feedbackCell {
            showMailEditor()
        } else if selectedCell == aboutAppCell {
            showAboutAppInfo()
        }
    }
    //
    fileprivate func showAboutAppInfo() {
        let alert = UIAlertController.simpleAlert(title: "\(Bundle.main.appName)",
            message: "Версия:\(Bundle.main.releaseVersionNumber)\nCборка:\(Bundle.main.buildVersionNumber)")
        self.present(alert, animated: true, completion: nil)
    }
}
//
extension SettingsViewController: MFMailComposeViewControllerDelegate {
    fileprivate func showMailEditor() {
        if !MFMailComposeViewController.canSendMail() {
            let alertError = UIAlertController.simpleErrorAlert(message: "Отправка электронного собщения не возможна")
            self.present(alertError, animated: true, completion: nil)
            return
        }
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients(["support@komandor.app"])
        composeVC.setSubject("Обратная связь")
        var message = "Опишите подробно вашу проблему или пожелаение\n\n\n"
        message += "Платформа: iOS\nВерсия:\(Bundle.main.releaseVersionNumber)\nCборка:\(Bundle.main.buildVersionNumber)"
        composeVC.setMessageBody(message, isHTML: false)
        self.present(composeVC, animated: true, completion: nil)
    }
    //
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
