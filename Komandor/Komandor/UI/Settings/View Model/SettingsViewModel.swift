//
//  SettingsViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class SettingsViewModel: ViewModel {
    var router: Router
    var loadProfileService: UserProfileLoadService?
    var userInfoDataObserver: BehaviorSubject<UserInfo?>? {
        return loadProfileService?.userInfoObserver
    }
    var user: User!
    var userInfo: UserInfo? {
        return user.userInfo
    }
    var name: String {
        return user.userInfo?.name ?? ""
    }
    var imagePhotoData: Data? {
        return user.photoImageData
    }
    //
    init(router: Router) {
        self.router = router
        guard let localUser = UserManager.manager.currentUser else {
            fatalError("user not found")
        }
        self.user = localUser
        refreshUser()
    }
    func openUserProfile() {
        if let router = self.router as? SettingsRouter {
            router.route(goTo: .profile )
        }
    }
    func refreshUser() {
        if loadProfileService == nil {
            loadProfileService = UserProfileLoadService(user: self.user)
        }
        loadProfileService?.load()
    }
}
