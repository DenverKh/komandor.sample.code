//
//  ChatCellViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/04/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class ChatCellViewModel {
    var chat: ChatCellProtocol
    init(chat: ChatCellProtocol) {
        self.chat = chat
    }
}
