//
//  ChatViewModel.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import RxSwift

class ChatViewModel: ViewModel {
    var router: Router
    var user: User!
    var certificate: Certificate!
    var chats = [Chat]()
    var refresh: BehaviorSubject<Bool>? {
        return ChatManager.manager.chatLoaderService?.refreshObserver
    }
    fileprivate let disposeBag = DisposeBag()
    init(router: Router) {
        self.router = router
        guard let localUser = UserManager.manager.currentUser else {
            fatalError("не найден пользователь!")
        }
        self.user = localUser
        guard let localCertificate = localUser.certificate else {
            fatalError("не найден сертификат пользователя!")
        }
        self.certificate = localCertificate
        let manager = ChatManager.manager
        manager.load(for: certificate)
        if let chatLoaderService = manager.chatLoaderService {
            chatLoaderService.dataObserver.subscribe(onNext: { (chats) in
                if let chats = chats {
                    self.chats = chats
                }
            }).disposed(by: disposeBag)
        }
    }
    func refreshChats() {
        ChatManager.manager.refresh()
    }
}
