//
//  ChatRouter.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

class ChatRouter: Router, HasParentRouter {
    enum Routes {
        case empty
        case moveToChat(chatId: String)
    }
    var baseViewController: UIViewController?
    var parent: GeneralRouter!
    init(navigationController: UINavigationController?, parent: GeneralRouter) {
        self.baseViewController = navigationController
        self.parent = parent
    }

    func start(context: Any?) {
        // просто создаем нав контроллер и в него добавляем нужный контроллер
        // настраиваем иконку и т.п.
        if let controller: ChatViewController = Storyboards.getController(from: .chat) {
            let viewModel = ChatViewModel(router: self)
            controller.viewModel = viewModel
            let navigationController = UINavigationController(rootViewController: controller)
            let iconImage = #imageLiteral(resourceName: "chat-icon")
            navigationController.tabBarItem = UITabBarItem(title: "Чаты", image: iconImage, tag: 1)
            self.baseViewController = navigationController
        }
    }

    func route(context: Any?) {
        guard let route = context as? Routes else { return }
        // check route here
        switch route {
        case .empty:
            break
        case .moveToChat(let chatId):
            print("from chat router = ", chatId)
        }
    }
}

extension ChatRouter {
    func route(goTo: Routes) {
        route(context: goTo)
    }
}
