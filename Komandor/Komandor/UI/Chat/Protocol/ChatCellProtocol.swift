//
//  ChatCellProtocol.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/04/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

/// протокол отображения чата в ячейке
protocol ChatCellProtocol {
    var titleName: String { get }
    var titleTitle: String { get }
    var titleInfo: String { get }
    var isOnline: Bool { get }
    var titlePhoto: Data? { get }
}
