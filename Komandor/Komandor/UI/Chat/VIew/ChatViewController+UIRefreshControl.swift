//
//  ChatViewController+UIRefreshControl.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/04/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//
import UIKit

extension ChatViewController {
    func configureRefreshControl () {
        guard let viewModel = self.viewModel else {
            fatalError("отсутствует viewModel")
        }
        // Add the refresh control to your UIScrollView object.
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        // обработка рефреш контрола
        viewModel.refresh?.subscribe(onNext: { [weak self] (isRefreshing) in
            DispatchQueue.main.async {
                if let refreshControl = self?.tableView.refreshControl {
                    if !isRefreshing {
                        refreshControl.endRefreshing()
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    @objc func handleRefreshControl() {
        guard let viewModel = self.viewModel else {
            fatalError("отсутствует viewModel")
        }
        //
        viewModel.refreshChats()
        // Dismiss the refresh control.
        DispatchQueue.main.async {
            //self.tableView.refreshControl?.endRefreshing()
        }
    }
}
