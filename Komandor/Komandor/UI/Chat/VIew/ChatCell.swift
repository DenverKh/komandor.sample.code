//
//  ChatCell.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/04/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {
    var viewModel: ChatCellViewModel? {
        didSet {
            updateUI()
        }
    }
    weak var parentController: ChatViewController?
    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelInfo: UILabel!
    func updateUI() {
        guard let viewModel = self.viewModel else {
            fatalError("viewModel not found!!!")
        }
        let chat = viewModel.chat
        imageViewLogo.setPhotoImage(data: chat.titlePhoto)
        labelName.text = chat.titleTitle
        labelInfo.text = chat.titleInfo
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
