//
//  ChatViewController.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChatViewController: UIViewController, View {
    var viewModel: ChatViewModel!
    @IBOutlet weak var tableView: UITableView!
    var disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupChatViewControllerUI()
        configureRefreshControl()
    }
    ///
    func setupChatViewControllerUI() {
        guard let viewModel = self.viewModel else {
            fatalError("viewModel not found!!!")
        }
        tableView.tableFooterView = UIView()
        if let refreshObserver = viewModel.refresh {
            refreshObserver.subscribe(onNext: { [weak self] (_) in
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }).disposed(by: disposeBag)
        }
    }
}
