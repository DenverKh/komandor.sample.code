//
//  ChatViewController+UITableView.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/04/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import UIKit

extension ChatViewController: UITableViewDelegate {
}

extension ChatViewController: UITableViewDataSource {
    ///
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as? ChatCell {
            //убираем выделение ячейки
            cell.selectionStyle = .none
            let chat = getChat(indexPath: indexPath)
            let cellViewModel = ChatCellViewModel(chat: chat)
            cell.viewModel = cellViewModel
            cell.parentController = self
            return cell
        }
        return UITableViewCell()
    }
    ///
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = self.viewModel else {
            fatalError("отсутствует viewModel!")
        }
        //print(viewModel.chats.count)
        return viewModel.chats.count
    }
    func getChat(indexPath: IndexPath) -> ChatCellProtocol {
        guard let viewModel = viewModel else {
            fatalError("отсутствует viewModel")
        }
        return viewModel.chats[indexPath.row]
    }
}
