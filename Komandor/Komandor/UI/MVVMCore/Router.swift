//
//  Router.swift
//
//  Created by Denis Khlopin on 22/10/2018.
//  Copyright © 2018 Denis Khlopin. All rights reserved.
//

import UIKit
/// протокол класс для контроля перемещений по экранам
/**
 необходимо внутри класса реализовать енум для выбора и переходов пользователя
 например
 
 enum routes{
 
 case back
 
 case showInfo(Model)
 
 case delete(Model)
 
 case etc
 
 }
 
 и вызывать из modelView следующую конструкцию
 
 router.route(.back)
 router.route(.delete(model))
 
 и в функции route обрабатывать эти переходы
 **/
protocol Router: class {
    ///Базовый контроллер, если он пустой, то должен создаваться навигейшн контроллер с самого дна
    var baseViewController: UIViewController? { get set }
    /// запуск первого контроллера для роутера
    /// - Parameters:
    ///     - context: какая либо модель, известная роутеру
    func start(context: Any?)
    ///Эту функцию нужно вызывать из viewModel для перехода на любой другой экран или открытия модальных окон
    /// так же можно использовать эту функцию для сохранения каких либо глобальных данных роутера
    /// - Parameters:
    ///     - context: в качестве параметра передается енум
    func route(context: Any?)
}

extension Router {
    func start() {
        start(context: nil)
    }
    var navigationController: UINavigationController? {
        if let navigationController = baseViewController as? UINavigationController {
            return navigationController
        }
        return nil
    }
}

protocol HasParentRouter {
    associatedtype RouterType: Router
    var parent: RouterType! { get }
}
