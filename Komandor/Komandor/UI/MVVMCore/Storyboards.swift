//
//  Storyboards.swift
//

import UIKit

class Storyboards {
    ///список доступных сторибордов
    enum StoryboardName: String {
        case auth = "Auth"
        case profile = "Profile"
        case contacts = "Contacts"
        case chat = "Chat"
        case settings = "Settings"
    }
    /// получить сториборд по типу ViewControllera с именем идентификатора,
    /// равным строковому представению имени класса
    ///
    /// например для класса TestViewController - идентификатор контроллера в сториборде должен быть "TestViewController"
    /**
     let controller:TestViewController = Storyboards.getController(from:.test) */
    /// - Parameters:
    ///     - from: имя доступного сториборда
    static func getController<T>(from name: StoryboardName) -> T? {
        let storyboard = UIStoryboard(name: name.rawValue, bundle: nil)
        let controllerName = String(describing: T.self)
        if let controller = storyboard.instantiateViewController(withIdentifier: controllerName) as? T {
            return controller
        }
        return nil
    }
    /// получить сториборд по типу и идентификатору,
    /// например для класса TestViewController - в сториборде может находиться контроллер с идентификатором "Test"
    /**
     let controller:TestViewController = Storyboards.getController(from:.test,withIdentifier:"Test") */
    /// - Parameters:
    ///     - from: имя доступного сториборда
    ///     - withIdentifier: идентификатор контроллера внутри сториборда
    static func getController<T>(from name: StoryboardName, withIdentifier identifier: String) -> T? {
        let storyboard = UIStoryboard(name: name.rawValue, bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: identifier) as? T {
            return controller
        }
        return nil
    }
    /// получить стартовый сториборд по типу
    /// если тип не соответствует, то вернется nil
    /// - Parameters:
    ///     - from: имя сториборда
    static func getInitialController<T>(from name: StoryboardName) -> T? {
        let storyboard = UIStoryboard(name: name.rawValue, bundle: nil)
        if let controller = storyboard.instantiateInitialViewController() as? T {
            return controller
        }
        return nil
    }
    static func getController<T: UIViewController>() -> T? {
        let controllerName = String(describing: T.self)
        let controller: T = T.init(nibName: controllerName, bundle: nil)
        return controller
    }
}
