//
//  View.swift
//
//  Created by Denis Khlopin on 22/10/2018.
//  Copyright © 2018 Denis Khlopin. All rights reserved.
//

import UIKit

protocol View: class {
    associatedtype ViewModelType
    var viewModel: ViewModelType! { get set }
}

extension View where Self: UIViewController {
    static func getController() -> Self? {
        print(String(describing: Self.Type.self))
        return UIViewController(nibName: String(describing: Self.Type.self), bundle: nil) as? Self
    }
}
