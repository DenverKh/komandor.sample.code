//
//  ViewModel.swift
//
//  Created by Denis Khlopin on 22/10/2018.
//  Copyright © 2018 Denis Khlopin. All rights reserved.
//

import Foundation

protocol ViewModel {
    var router: Router { get }
}
