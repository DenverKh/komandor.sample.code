//
//  AppDelegate.swift
//  Komandor
//
//  Created by Kostya Golenkov on 17/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // create empty default window
        window = UIWindow(frame: UIScreen.main.bounds)
        // Fabric init
        Fabric.with([Crashlytics .self])
        // start routing here
        RouteManager.manager.start()
        //setup theme colors
        // TODO: (перенести в темы) установка цвета по умолчанию для разных UI элементов
        setupNavigationBar()
        setupTabBar()
        return true
    }
    // setup navigation bar
    fileprivate func setupNavigationBar() {
        // MARK: Установка цвета нав бара
        UINavigationBar.appearance().barTintColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().barStyle = .black
    }
    fileprivate func setupTabBar() {
        UITabBar.appearance().tintColor = .white
        UITabBar.appearance().barTintColor = .black
        UITabBar.appearance().alpha = 1
    }
    //switch root view controller
    func switchRootViewController(toVC: UIViewController) {
        guard let window = self.window else {
            fatalError("Window not found")
        }
        let closure = { (viewController: UIViewController) -> Void in
            window.rootViewController = viewController
            window.makeKeyAndVisible()
        }
        guard let fromVC = window.rootViewController else {
            closure(toVC)
            return
        }
        guard let snapshotView = fromVC.view.snapshotView(afterScreenUpdates: true) else {
            closure(toVC)
            return
        }
        //check destination view controller type
        switch toVC {
        case is UINavigationController:
            let topVC = (toVC as? UINavigationController)?.topViewController
            topVC!.view.addSubview(snapshotView)
            snapshotView.wrapInSuperview()
        default:
            toVC.view.addSubview(snapshotView)
        }
        closure(toVC)
        //switch with anymation
        UIView.animate(withDuration: 0.75, animations: {
            snapshotView.layer.opacity = 0
        }, completion: { (_) in
            snapshotView.removeFromSuperview()
        })
    }
}
