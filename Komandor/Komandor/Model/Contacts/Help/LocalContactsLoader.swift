//
//  ContactLoaderService.swift
//  Komandor
//
//  Created by Denis Khlopin on 07/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import Contacts
/// загрузка локальных контактов телефона
class LocalContacts {
    fileprivate func withAccess(completion: @escaping (_ granted: Bool) -> Void) {
        let status = CNContactStore.authorizationStatus(for: .contacts)
        switch status {
        case .authorized:
            completion(true)
        default:
            let store = CNContactStore()
            //Запросить разрешение у пользователя на доступ к контактам
            store.requestAccess(for: .contacts) { (granted, error) in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    if granted {
                        completion(true)
                    } else {
                        completion(false)
                    }
                }
            }
        }
    }
    /// получение контактов с телефона
    func getPhoneContacts(completion: @escaping ([Contact]) -> Void) {
        withAccess { [weak self] (granted) in
            if granted {
                // получаем контакты
                if let contacts = self?.getContacts()
                    // отсееваем номера без телефонов
                    .filter({ (contact) -> Bool in
                        return contact.contactPhones.count != 0
                    })
                    .sorted(by: {$0.contactName! > $1.contactName!}) {
                    for contact in contacts {
                        print(contact.contactName ?? "unknown name")
                        print(contact.contactPhones)
                    }
                    completion(contacts)
                    return
                }
            }
            completion([])
        }
    }
    /// получение контактов
    fileprivate func getContacts() -> [Contact] {
        let store = CNContactStore()
        guard let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as? [CNKeyDescriptor]
            else {
                fatalError()
        }
        //
        var allContainers: [CNContainer] = []
        do {
            allContainers = try store.containers(matching: nil)
            var results: [CNContact] = []
            for container in allContainers {
                let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                let containerResults = try store.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch )
                results.append(contentsOf: containerResults)
            }
            return results.map({ (cnContact) -> Contact in
                return self.toContact(from: cnContact)
            })
        } catch {
            print(error.localizedDescription)
        }
        return []
    }
    /// конвертируем CNContact -> Contact
    fileprivate func toContact(from cnContact: CNContact) -> Contact {
        var contact = Contact()
        contact.contactType = .phone
        contact.contactName = cnContact.givenName + " " + cnContact.familyName
        for phone in cnContact.phoneNumbers {
            let mobilePhone = MaskUtils.phone(phone.value.stringValue).numbersOnly
            // добавляем только сотовые российские номера
            if mobilePhone.prefix(2) == "79" {
                contact.contactPhones.append(mobilePhone)
            }
        }
        return contact
    }
}
