//
//  Contact.swift
//  Komandor
//
//  Created by Denis Khlopin on 06/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

struct Contact: PhoneContactProtocol, ServerContactProtocol, ChatProtocol {
    //
    var contactType = ContactType.unknown
    //
    var contactStatus = ContactStatus.offline
    /// данные из записной книжки
    // телефоны
    var contactPhones: [String] = []
    // имя
    var contactName: String?
    //
    var imageData: Data?
    //
    /// данные с сервера
    // id пользователя
    var uid: Int?
    // id сертификата
    var pid: Int?
    // имя пользователя
    var userName: String?
    // имя сертификата
    var name: String?
    // заголовок(компания или ИП)
    var title: String?
    // уникальный телефон
    var phone: String?
    // фото в формате base64 (jpeg)
    var photo: String?
    // тип
    var type: Int?
    // список сертификатов
    var certificates: [APICertificate]?
    // идентификатор чата
    var chatId: String?
    // тип чата 0 - личный, 1 - групповой 2 - канал
    var chatType: Int?
    // имя чата
    var chatName: String?
    // ключ
    var key: APIKey?
}
