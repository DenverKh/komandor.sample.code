//
//  ContactType.swift
//  Komandor
//
//  Created by Denis Khlopin on 07/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

enum ContactType {
    /// не известный тип
    case unknown
    /// контакт телефона
    case phone
    /// контакт с сервера komandor
    case comandor
    /// контакт есть на телефоне и на сервере
    case both
    /// это чат!
    case chat
}
