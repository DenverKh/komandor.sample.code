//
//  ContactStatus.swift
//  Komandor
//
//  Created by Denis Khlopin on 11/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

enum ContactStatus {
    case offline
    case online
}
