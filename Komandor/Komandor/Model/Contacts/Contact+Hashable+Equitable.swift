//
//  Contact+Hashable+Equitable.swift
//  Komandor
//
//  Created by Denis Khlopin on 21/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
/// расширение контактов для сравнения и поиска дупликатов!!!
extension Contact: Equatable, Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine("\(contactType)\(contactName ?? userName ?? "empty")")
        for mobile in contactPhones {
            hasher.combine("\(mobile)")
        }
        if let phone = self.phone {
            hasher.combine("\(phone)")
        }
    }
    static func == (lhs: Contact, rhs: Contact) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}
