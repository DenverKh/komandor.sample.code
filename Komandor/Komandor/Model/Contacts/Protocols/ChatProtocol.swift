//
//  ChatProtocol.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

protocol ChatProtocol {
    // идентификатор чата
    var chatId: String? { get set }
    // тип чата 0 - личный, 1 - групповой 2 - канал
    var chatType: Int? { get set }
    // имя чата
    var chatName: String? { get set }
    // ключ
    var key: APIKey? { get set }
}
