//
//  PhoneContactProtocol.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

protocol PhoneContactProtocol {
    // телефоны
    var contactPhones: [String] { get set }
    // имя
    var contactName: String? { get set }
}
