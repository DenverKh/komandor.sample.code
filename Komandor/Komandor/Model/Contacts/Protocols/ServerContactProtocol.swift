//
//  ServerContactProtocol.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

protocol ServerContactProtocol {
    // id пользователя
    var uid: Int? { get set }
    // id сертификата
    var pid: Int? { get set }
    // имя пользователя
    var userName: String? { get set }
    // имя сертификата
    var name: String? { get set }
    // заголовок(компания или ИП)
    var title: String? { get set }
    // уникальный телефон
    var phone: String? { get set }
    // фото в формате base64 (jpeg)
    var photo: String? { get set }
    // тип
    var type: Int? { get set }
    // список сертификатов
    var certificates: [APICertificate]? { get set }
}
