//
//  Contact+ContactCellProtocol.swift
//  Komandor
//
//  Created by Denis Khlopin on 18/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
// подготовка информации для таблицы
extension Contact: ContactCellProtocol {
    // статус
    var isOnline: Bool {
        if cellType == .server {
            // TODO: проверка на онлайн
            return true
        }
        return false
    }
    // тип ячейки
    var cellType: ContactCellType {
        switch self.contactType {
        case .unknown:
            return .unknown
        case .phone:
            return .phone
        case .comandor:
            return .server
        case .both:
            return .server
        case .chat:
            return .server
        }
    }
    // отображение имени контакта
    var titleName: String {
        if let name = self.userName {
            return name
        }
        if let name = self.contactName {
            return name
        }
        return "неизвестный"
    }
    // отображение информации и телефона контакта
    var titleInfo: String {
        var result = ""
        if !self.titleCompany.isEmpty, let type = self.type {
            switch type {
            case 1:
                result = ""
            case 2:
                result = "Индивидуальный предприниматель\n"
            case 3:
                result = self.titleCompany + "\n"
            default:
                result = ""
            }
        }
        if let phone = self.phone {
            return result + MaskUtils.phone(phone).mask
        }
        if let phone = self.contactPhones.first {
            return MaskUtils.phone(phone).mask
        }
        return ""
    }
    var titlePhoto: Data? {
        if let base64 = self.photo, let data = base64.convertToImageData() {
            return data
        }
        if let data = self.imageData {
            return data
        }
        return nil
    }
    var titleTitle: String {
        if let title = self.title {
            return title
        }
        return ""
    }
    var titleCompany: String {
        if let name = self.name {
            return name
        }
        return ""
    }
}
