//
//  ContactLoaderService.swift
//  Komandor
//
//  Created by Denis Khlopin on 07/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import Contacts

class LocalContactsLoader {
    fileprivate func withAccess(completion: @escaping (_ granted: Bool) -> Void) {
        let status = CNContactStore.authorizationStatus(for: .contacts)
        switch status {
        case .authorized:
            completion(true)
        default:
            let store = CNContactStore()
            //Запросить разрешение у пользователя на доступ к контактам
            store.requestAccess(for: .contacts) { (granted, error) in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    if granted {
                        completion(true)
                    } else {
                        completion(false)
                    }
                }
            }
        }
    }
    /// получение контактов с телефона
    func getPhoneContacts(completion: @escaping ([Contact]) -> Void) {
        withAccess { (granted) in
            if granted {
                // получаем контакты
                let contacts = self.getContacts(filter: "")
                for contact in contacts {
                    print(contact.contactName ?? "unknown name")
                    print(contact.contactPhones)
                }
            } else {
                completion([])
            }
        }
    }
    ///
    fileprivate func getContacts(filter: String) -> [Contact] {
        let store = CNContactStore()
        guard let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as? [CNKeyDescriptor]
            else {
                fatalError()
        }
        //
        var allContainers: [CNContainer] = []
        do {
            allContainers = try store.containers(matching: nil)
            var results: [CNContact] = []
            for container in allContainers {
                let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                let containerResults = try store.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch )
                results.append(contentsOf: containerResults)
                //print(results.last?.phoneNumbers.first?.value.stringValue ?? "")
            }
            //print(results)
            return results.map({ (cnContact) -> Contact in
                return self.toContact(from: cnContact)
            }).filter({ (contact) -> Bool in
                return contact.contactPhones.count != 0
            })
        } catch {
            print(error.localizedDescription)
        }
        return []
    }
    fileprivate func toContact(from cnContact: CNContact) -> Contact {
        var contact = Contact()
        contact.contactType = .phone
        contact.contactName = cnContact.givenName + " " + cnContact.familyName
        contact.name = cnContact.givenName
        //if
        //contact.phone = cnContact.phoneNumbers.first!.value.stringValue
        for phone in cnContact.phoneNumbers {
            contact.contactPhones.append(MaskUtils.phone(phone.value.stringValue).numbersOnly)
        }
        return contact
    }
}
