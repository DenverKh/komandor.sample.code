//
//  User.swift
//  Komandor
//
//  Created by Denis Khlopin on 26/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
/// текущий пользователь с привязанным сертификатом
class User {
    var certificate: Certificate?
    var userInfo: UserInfo?
    var status = UserStatus.online
}

extension User {
    var photoImageData: Data? {
        if let base64String = self.userInfo?.photo {
            return base64String.convertToImageData()
        }
        return nil
    }
}
