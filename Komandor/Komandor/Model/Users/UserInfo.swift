//
//  UserInfo.swift
//  Komandor
//
//  Created by Denis Khlopin on 26/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

struct UserInfo {
    var pid: Int?
    var cid: Int?
    var type: Int?
    var name: String?
    var company: String?
    var title: String?
    var inn: String?
    var ogrn: String?
    var snils: String?
    var phone: String?
    var photo: String?
    var email: String?
    var billing: UserBillingInfo?
    //var certificates: [UserCertificateInfo]?
    var certificates: [APICertificate]?
}
struct UserBillingInfo {
    var balance: Double?
}

struct UserCertificateInfo {
    var identifier: Int?
    var cert: String?
}
