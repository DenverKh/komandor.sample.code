//
//  UserStatus.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

enum UserStatus {
    case online
    case offline
    func toString() -> String {
        switch self {
        case .online:
            return "В сети"
        case .offline:
            return "Не в сети"
        }
    }
}
