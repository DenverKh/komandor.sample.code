//
//  APIContacts.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
///JSON /api/contacts/
/// запрос на данные профиля, передается токен
struct APIContactsRequest: Codable {
    var token: String
    var phones: [String]?
}

/// возвращает структуру профиля
struct APIContactsResponse: Codable {
    var uid: Int
    var pid: Int
    var user: String?
    var name: String?
    var title: String?
    var phone: String?
    var photo: String?
    var type: Int
    var certificates: [APICertificate]?
    var chatId: String?
    var chatName: String?
    var chatType: Int?
    var key: APIKey?
}

struct APIKey: Codable {
    var identifier: Int
    var key: String?
    var isImport: Bool?
    var cert: String?
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case key
        case cert
        case isImport = "import"
    }
}
/*
 {
 "success": true,
 "data": [{
    "uid": 29,
    "pid": 33,
    "user": "ФИО",
    "name": "Название организации|Индивидуальный предприниматель|ФИО",
    "title": "Должность",
    "phone": "79991234567",
    "photo": "base64:AAAAA=",
    "type": 1,
    "certificates": [{
        "id": 22,
        "cert": "base64"
        }],
    "chatId": "91ec6ed7-aced-4251-bb01-55debd90ddb6",
    "chatType": 0,
    "chatName": "Название группы или канала",
    "key": {
        "id": 123,
        "key": "base64",
        "import": false,
        "cert": "base64"
    }
 }]
 }
 */
