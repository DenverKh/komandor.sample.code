//
//  APICertificates.swift
//  Komandor
//
//  Created by Denis Khlopin on 28/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

///JSON /api/certificates/
/// запрос на данные профиля, передается токен
struct APICertificatesRequest: Codable {
    var token: String
    var pid: Int
}

// возвращает массив APICertificates
typealias APICertificatesResponse = [APICertificate]
