//
//  APICreateChat.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

///JSON /api/createChat/
/// запрос на данные профиля, передается токен
struct APICreateChatRequest: Codable {
    var token: String
    var type: Int
    var name: String
    var keys: [APIChatKey]
}

struct APIChatKey: Codable {
    var cid: Int
    var pid: Int
    var key: String
}
/// возвращает структуру профиля
/// возвращает APIContactResponse
typealias APICreateChatResponse = APIContactsResponse

/*
 {
 "token": "99b082c6-979b-4646-9e85-60f30b1c37a7",
 "type": 0,
 "name": "Название группы или канала",
 "keys": [{
 "cid": 123,
 "pid": 456,
 "key": "base64"
 }]
 }
 */
