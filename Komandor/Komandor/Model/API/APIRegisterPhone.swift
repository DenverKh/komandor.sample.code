//
//  APIRegisterPhone.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
/// /api/registryPhone/
/// запрос на регистрацию телефона
struct APIRegisterPhoneRequest: Codable {
    var token: String
    var phone: String
}
/// в ответ возвращается id запроса, который используется для проверки кода
struct APIRegisterPhoneResponse: Codable {
    var identifier: Int
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
    }
}
