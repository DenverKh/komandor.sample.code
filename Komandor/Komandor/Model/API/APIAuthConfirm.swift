//
//  APIAuthConfirm.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

/// /api/confirmAuth/
/// запрос на авторизацию, передаем токен и подписанную строку nonce, полученную ранее
struct APIAuthConfirmRequest: Codable {
    var token: String
    var sign: String
}
/// в ответ получаем ОК и переменную, нужно ли регистрировать телефон или нет
struct APIAuthConfirmResponse: Codable {
    var needRegisterPhone: Bool
}
