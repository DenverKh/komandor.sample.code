//
//  APIPhoto.swift
//  Komandor
//
//  Created by Denis Khlopin on 22/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

///JSON /api/profile/
/// запрос на данные профиля, передается токен
struct APIPhotoRequest: Codable {
    var token: String
    var data: String
}

/// возвращает структуру профиля
struct APIPhotoResponse: Codable {
}
