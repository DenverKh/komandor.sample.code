//
//  APICertSign.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
/// path = api/auth/
/// запрос на подключение
/// содержит сертификат и подпись сертификата в base64String
struct APICertSignRequest: Codable {
    var cert: String
    var sign: String
}
/// ответ: токен и строка nonce, которую нужно повторно подписать
struct APICertSignResponse: Codable {
    var nonce: String
    var token: String
}
