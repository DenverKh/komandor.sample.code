//
//  APIConfirmPhone.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

/// /api/confirmPhone/
/// запрос на проверку смс кода, отправленного на телефон
struct APIConfirmPhoneReguest: Codable {
    var token: String
    var identifier: Int
    var code: String
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case token
        case code
    }
}
/// ответ пустой
struct APIConfirmPhoneResponse: Codable {
}
