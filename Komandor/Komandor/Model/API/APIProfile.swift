//
//  APIProfile.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

///JSON /api/profile/
/// запрос на данные профиля, передается токен
struct APIProfileRequest: Codable {
    var token: String
}

struct APIBilling: Codable {
    var balance: Double
}

struct APICertificate: Codable {
    var identifier: Int
    var cert: String?
    var sync: Bool?
    var pid: Int?
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case cert
    }
}
/// возвращает структуру профиля
struct APIProfileResponse: Codable {
    var pid: Int
    var cid: Int
    var type: Int
    var name: String?
    var company: String?
    var title: String?
    var inn: String?
    var ogrn: String?
    var snils: String?
    var phone: String?
    var photo: String?
    var email: String?
    var billing: APIBilling
    var certificates: [APICertificate]
}
