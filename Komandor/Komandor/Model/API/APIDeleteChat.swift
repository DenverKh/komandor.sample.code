//
//  APIDeleteChat.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/04/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
///JSON /api/deleteChatToId/
/// запрос на данные профиля, передается токен
struct APIDeleteChatRequest: Codable {
    var token: String
    var chat: String
}

struct APIDeleteChatResponse: Codable {
    var deleteChatResult: Int?
    var deleteChatKeysResult: Int?
    var deleteMessages: Int?
}
