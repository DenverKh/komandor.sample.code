//
//  APIResponse.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
/// общая структура ответа
struct APIResponse<ResultType>: Codable where ResultType: Codable {
    var success: Bool?
    var error: String?
    var data: ResultType?
}
