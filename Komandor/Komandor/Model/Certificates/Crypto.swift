//
//  Crypto.swift
//  CryptoProNew
//
//  Created by Denis Khlopin on 15/01/2019.
//  Copyright © 2019 Denis Khlopin. All rights reserved.
//

import Foundation
class Crypto: CryptoParams {
    var containerContextHandle: UInt = 0
    // constants and enums
    private let noMoreItems: UInt32 = 259
    var cryptoContainers = [Certificate]()
    init() {
        self.updateContext()
    }
    deinit {
        if containerContext != 0 {
            cppCryptoFreeContext(containerContextHandle)
        }
    }
    func updateContext() {
        // получаем контекст корневого сертификата!
        // если не установлен ни один сертификат, то контекст будет равен 0
        if containerContext != 0 {
            cppCryptoFreeContext(containerContextHandle)
        }
        containerContextHandle = cppCryptoGetContext(nil)
        // загружаем список контейнеров в массив
        loadContainers()
    }
    // загрузка контейнеров
    func loadContainers() {
        // очищаем список контейнеров
        cryptoContainers.removeAll()
        // если контейнеров нет, выходим
        if containerContext <= 0 {
            print(CryptoError.noContainersInstalled)
            return
        }
        // перебираем список контейнеров, получаем их имена
        var containersNames = [String]()
        var containerEnum = ContainerEnum.first
        while true {
            let enumSize = cppCryptoGetNextContainerSize(containerContext, containerEnum.rawValue)
            let enumBuffer: UnsafeMutablePointer<BYTE> = UnsafeMutablePointer<BYTE>.allocate(capacity: Int(enumSize))
            let result = cppCryptoGetNextContainerName(containerContext, containerEnum.rawValue, enumSize, enumBuffer)
            if result == noMoreItems {
                enumBuffer.deallocate()
                break
            }
            containersNames.append(String(cString: enumBuffer))
            enumBuffer.deallocate()
            containerEnum = ContainerEnum.next
        }
        print(containersNames)
        // создаем контейнеры по  полученным именам
        for name in containersNames {
            let certificate = Certificate(name: name)
            let loadDataService = CertificateLoadContainerService(certificate: certificate)
            loadDataService.load()
            cryptoContainers.append(certificate)
        }
    }
}
