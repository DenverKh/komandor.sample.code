//
//  CertificateType.swift
//  Komandor
//
//  Created by Denis Khlopin on 24/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation

enum CertificateType {
    case unknown
    case personal
    case individual
    case company
}
