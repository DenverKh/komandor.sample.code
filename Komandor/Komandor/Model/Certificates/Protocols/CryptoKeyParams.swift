//
//  CryptoKeyParams.swift
//  CryptoProNew
//
//  Created by Denis Khlopin on 16/01/2019.
//  Copyright © 2019 Denis Khlopin. All rights reserved.
//

import Foundation
/// протокол параметров публичных/приватных ключей контейнера
/*
protocol CryptoKeyParams {
    var keyHandle: UInt { get }
}

extension CryptoKeyParams {
    var key: UInt {
        return keyHandle
    }
    // получение параметра для контейнера
    func getKeyParam(param: KeyParam) -> KeyParamResult {
        var paramInfo = KeyParamResult(paramStr: "", paramInt: UInt32(0), errorCode: UInt32(0), size: UInt32(0))
        let size = cppCryptoUserKeyGetSize(key, param.rawValue, 0)
        paramInfo.size = size
        if size == 0 {
            /// если празмер равен 0, возвращаем пустые значения
            return paramInfo
        }
        /// выделяем память для буфера размером size
        let buffer: UnsafeMutablePointer<BYTE> = UnsafeMutablePointer<BYTE>.allocate(capacity: Int(size))
        let result = cppCryptoUserKeyGetParam(key, param.rawValue, 0, size, buffer)
        // копируем из буфера в строку
        paramInfo.paramStr = String(cString: buffer)
        paramInfo.errorCode = result
        // приводим буфер к типу DWORD
        let uint32Pointer = UnsafeMutableRawPointer(buffer).bindMemory(to: UInt32.self, capacity: 1)
        paramInfo.paramInt = uint32Pointer.pointee
        /// освобождаем память буфера
        buffer.deallocate()
        return paramInfo
    }
    func getBlobParam(param: BlobParam) -> String {
        var result = ""
        let size = cppCryptoGetCertificateBlobInfo(key, nil, param.rawValue)
        if size > 0 {
            let buffer: UnsafeMutablePointer<Int8> = UnsafeMutablePointer<Int8>.allocate(capacity: Int(size))
            cppCryptoGetCertificateBlobInfo(key, buffer, param.rawValue)
            result = String(cString: buffer)
            buffer.deallocate()
        }
        return result
    }
}
*/
/*struct KeyParamResult {
    var paramStr: String
    var paramInt: UInt32
    var errorCode: UInt32
    var size: UInt32
}*/
