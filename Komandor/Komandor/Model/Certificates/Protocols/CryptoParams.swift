//
//  CryptoParams.swift
//  CryptoProNew
//
//  Created by Denis Khlopin on 15/01/2019.
//  Copyright © 2019 Denis Khlopin. All rights reserved.
//

import Foundation
/// протокол параметров контейнера
protocol CryptoParams {
    var containerContextHandle: UInt { get }
}

extension CryptoParams {
    var containerContext: UInt {
        return containerContextHandle
    }
    // получение параметра для контейнера
    func getContainerParam(param: ContainerParam, containerEnum: ContainerEnum = .none ) -> (param: String, errorCode: UInt32) {
        var paramInfo = (param: "", errorCode: UInt32(0))
        let size = cppCryptoContainerGetSize(containerContext, param.rawValue, containerEnum.rawValue)
        if size == 0 {
            return paramInfo
        }
        let buffer: UnsafeMutablePointer<BYTE> = UnsafeMutablePointer<BYTE>.allocate(capacity: Int(size))
        let result = cppCryptoContainerGetParam(containerContext, param.rawValue, containerEnum.rawValue, size, buffer)
        paramInfo.param = String(cString: buffer)
        paramInfo.errorCode = result
        buffer.deallocate()
        return paramInfo
    }
}
