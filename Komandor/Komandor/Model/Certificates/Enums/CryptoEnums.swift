//
//  CryptoEnums.swift
//  CryptoProNew
//
//  Created by Denis Khlopin on 15/01/2019.
//  Copyright © 2019 Denis Khlopin. All rights reserved.
//

import Foundation

enum ContainerParam: UInt32, CaseIterable {
    case ppEnumAlgs = 1
    case ppEnumContainers = 2
    case ppImpType = 3
    case ppName = 4
    case ppVersion = 5
    case ppContainer = 6
    case ppChangePassword = 7
    case ppKeySetSecDescr = 8
    case ppCertChain = 9
    case ppKeyTypeSubtype = 10
    case ppProvType = 16
    case ppKeyStorage = 17
    case ppAppliCert = 18
    case ppSymKeySize = 19
    case ppSessionKeySize = 20
    case ppUIPromt = 21
    case ppEnumAlgsEx = 22
    case ppEnumMandRoots = 25
    case ppEnumElectRoots = 26
    case ppKeySetType = 27
    case ppAdminPin = 31
    case ppKeyExchangePin = 32
    case ppSignaturePin = 33
    case ppSigKeySizeInc = 34
    case ppKeyxKeySize = 35
    case ppUniqueContainer = 36
    case ppSGCInfo = 37
    case ppUseHardwareRng = 38
    case ppKeySpec = 39
    case ppEnumExSigningProt = 40
    case ppCryptCountKeyUse = 41
    case ppUserCertStore = 42
    case ppSmartCardReader = 43
    case ppSmartCardGUID = 45
    case ppRootCertStore = 46
    case ppPinInfo = 120
    case ppPasswdTerm = 123
}
enum ContainerEnum: UInt32 {
    case none = 0
    case first = 1
    case next = 2
}
enum KeyParam: UInt32, CaseIterable {
    case kpNone = 0
    case kpIV = 1               /* Initialization vector*/
    case kpSalt = 2             /* Salt value*/
    case kpPadding = 3          /* Padding values*/
    case kpMode = 4             /* Mode of the cipher*/
    case kpModeBits = 5         /* Number of bits to feedback*/
    case kpPermissions = 6      /* Key permissions DWORD*/
    case kpAlgId = 7            /* Key algorithm*/
    case kpBlockLen = 8         /* Block size of the cipher*/
    case kpKeyLen = 9           /* Length of key in bits*/
    case kpSaltEx = 10          /* Length of salt in bytes*/
    case kpP = 11               /* DSS/Diffie-Hellman P value*/
    case kpG = 12               /* DSS/Diffie-Hellman G value*/
    case kpQ = 13               /* DSS Q value*/
    case kpX = 14               /* Diffie-Hellman X value*/
    case kpY = 15               /* Y value*/
    case kpRA = 16              /* Fortezza RA value*/
    case kpRB = 17              /* Fortezza RB value*/
    case kpInfo = 18            /* for putting information into an RSA envelope*/
    case kpEffectiveKeyLen = 19 /* setting and getting RC2 effective key length*/
    case kpSchannelAlg = 20    /* for setting the Secure Channel algorithms*/
    case kpClientRandom = 21   /* for setting the Secure Channel client random data*/
    case kpServerRandom = 22   /* for setting the Secure Channel server random data*/
    case kpRP = 23
    case kpPreCompMD5 = 24
    case kpPreCompSHA = 25
    case kpCertificate = 26     /* for setting Secure Channel certificate data (PCT1)*/
    case kpClearKey = 27       /* for setting Secure Channel clear key data (PCT1)*/
    case kpPubExLen = 28
    case kpPubExVal = 29
    case kpKeyVal = 30
    case kpAdminPin = 31
    case kpKeyExchangePin = 32
    case kpSignaturePin = 33
    case kpPreHash = 34
    case kpOAEPParams = 36      /* for setting OAEP params on RSA keys*/
    case kpCMSKeyInfo = 37
    case kpCMSDHKeyInfo = 38
    case kpPubParams = 39      /* for setting public parameters*/
    case kpVerifyParams = 40   /* for verifying DSA and DH parameters*/
    case kpHighestVersion = 41 /* for TLS protocol version setting*/
}

enum BlobParam: UInt32, CaseIterable {
    case cdIssuer = 1
    case cdSubject = 2
    case cdIssuerUniqueId = 4
    case cdSubjectUniqueId = 5
    case cdSignatureAlgoParameters = 6
    case cdSubjectPublicKey = 7
    case cdSubjectPublicAlgoParameters = 8
    case cdExtentionValue = 9
    case cdSerialNumber = 10
}
