//
//  OIDDictionary.swift
//  CryptoProNew
//
//  Created by Denis Khlopin on 16/01/2019.
//  Copyright © 2019 Denis Khlopin. All rights reserved.
//

import Foundation

enum OIDDictionary: String, Hashable {
    case unknown = ""
    case unstructured = "1.2.840.113549.1.9.2"
    case email = "1.2.840.113549.1.9.1"
    case inn = "1.2.643.3.131.1.1"
    case ogrn = "1.2.643.100.1"
    case snils = "1.2.643.100.3"
    case ogrnip = "1.2.643.100.5"
    case rnsFss = "1.2.643.3.141.1.1"
    case kpFss = "1.2.643.3.141.1.2"
    case streetAddress = "2.5.4.9"
    case localityName = "2.5.4.7"
    case stateOrProvinceName = "2.5.4.8"
    case countryName = "2.5.4.6"
    case givenName = "2.5.4.42"
    case sureName = "2.5.4.4"
    case commonName = "2.5.4.3"
    case organizationName = "2.5.4.10"
    case post = "2.5.4.12"  // должность

    static func parse(from string: String) -> (oid: OIDDictionary, value: String) {
        var result = (oid: OIDDictionary.unknown, value: "")
        if let rawValue = string.split(separator: "=").first,
            let oid = OIDDictionary.init(rawValue: String(rawValue)) {
            result.value = String(string.suffix(string.count - rawValue.count - 1))
            result.oid = oid
        }
        return result
    }
    /*
     OID.2.5.4.12
 1.2.840.113549.1.9.2=INN=027507180116
 1.2.840.113549.1.9.1=khlopin_den@mail.ru
 1.2.643.3.131.1.1=027507180116
 1.2.643.100.3=10686361464
 2.5.4.9=Запорожский переулок д. 7, кв. 12
 2.5.4.7=г. Уфа
 2.5.4.8=2 Республика Башкортостан
 2.5.4.6=RU
 2.5.4.42=Денис Александрович
 2.5.4.4=Хлопин
 2.5.4.3=Хлопин Денис Александрович
 */
    /*
    1.2.840.113549.1.9.1=info@ucsouz.ru
    1.2.643.100.1=1147746629755
    1.2.643.3.131.1.1=007710963520
    2.5.4.6=RU
    2.5.4.8=77 г. Москва
    2.5.4.7=Москва
    2.5.4.9=ул. Садовая-Триумфальная д.18 пом.1 комн.1
    2.5.4.10=ООО УЦ СОЮЗ
    2.5.4.3=ООО УЦ СОЮЗ
 */
}
