//
//  CryptoContainer.swift
//  CryptoProNew
//
//  Created by Denis Khlopin on 15/01/2019.
//  Copyright © 2019 Denis Khlopin. All rights reserved.
//

import Foundation

class Certificate: CryptoParams {
    var containerContextHandle: UInt = 0
    var keyHandle: UInt = 0
    var isExchangeKey = true
    var containerContext: UInt {
        return containerContextHandle
    }
    var name: String!
    var uniqueName: String?
    /// параметры издателя сертификата
    var issuerValues = [OIDDictionary: String]()
    /// параметры владельца сертификата
    var subjectValues = [OIDDictionary: String]()
    // параметры сертификата
    /// версия сертификата
    var version: UInt32 = 0
    /// серийный номер
    var serialNumber: Data?
    /// oid алгоритма шифрования сертификата
    var oidAlgoSignature: String?
    /// дата начала действия сертификата
    var startDate: Date?
    /// дата окончания действия сертификата
    var endDate: Date?
    /// закодированный сертификат
    var certEncoded: UnsafeMutablePointer<BYTE>?
    var certEncodedSize: UInt32?
    var certEncodedData: Data?
    // расчетные значения
    // ФИО
    var firstName: String = ""
    var secondName: String = ""
    var middleName: String = ""
    var urlContainer: URL?
    /// тип сертификата (вычисляем по нескольким факторам)
    var type = CertificateType.unknown

    init(name: String, isExchangeKey: Bool = true) {
        self.name = name
        self.isExchangeKey = isExchangeKey
        /// загрузка данных сертификата из контейнера
        //load()
    }
    deinit {
        if containerContext != 0 {
            cppCryptoFreeContext(containerContextHandle)
        }
        if certEncoded != nil {
           certEncoded?.deallocate()
        }
    }
}
