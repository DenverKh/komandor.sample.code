//
//  Certificate.swift
//  Komandor
//
//  Created by Denis Khlopin on 21/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation
extension Certificate {
    /// ИНН
    var inn: String {
        return subjectValues[.inn] ?? ""
    }
    /// наименование организации
    var companyName: String {
        return subjectValues[.organizationName] ?? ""
    }
    /// СНИЛС
    var snils: String {
        return subjectValues[.snils] ?? ""
    }
    /// ОГРН
    var ogrn: String {
        return subjectValues[.ogrn] ?? ""
    }
    /// должность управляющего(директор/ген директор и т.п.)
    var post: String {
        return subjectValues[.post] ?? ""
    }
    var streetAddress: String {
        return subjectValues[.streetAddress] ?? ""
    }
    var email: String {
        return subjectValues[.email] ?? ""
    }
    var ogrnip: String {
        return subjectValues[.ogrnip] ?? ""
    }
    var city: String {
        return subjectValues[.localityName] ?? ""
    }
    var province: String {
        return subjectValues[.stateOrProvinceName] ?? ""
    }
    func check() -> Bool {
        if let endDate = self.endDate {
            if endDate > Date() {
                return true
            }
        }
        return false
    }
    var fullName: String {
        return "\(self.secondName) \(self.firstName) \(self.middleName)"
    }
    var startDateStr: String {
        return startDate?.value(withFormat: "dd.MM.yyyy HH:mm") ?? ""
    }
    var finishDateStr: String {
        return endDate?.value(withFormat: "dd.MM.yyyy HH:mm") ?? ""
    }
    func getEncodedCertificateBase64() -> String {
        guard let data = self.certEncodedData else {
            return ""
        }
        return data.base64EncodedString()
    }
}
