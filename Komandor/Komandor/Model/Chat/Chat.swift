//
//  Chat.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

struct Chat: ChatProtocol, ServerContactProtocol {
    var uid: Int?
    //
    var pid: Int?
    //
    var userName: String?
    //
    var name: String?
    //
    var title: String?
    //
    var phone: String?
    //
    var photo: String?
    //
    var type: Int?
    //
    var certificates: [APICertificate]?
    //
    var key: APIKey?
    //
    var chatId: String?
    //
    var chatType: Int?
    //
    var chatName: String?
}
