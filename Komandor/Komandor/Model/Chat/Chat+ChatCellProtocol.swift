//
//  Chat+ChatCellProtocol.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/04/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

extension Chat: ChatCellProtocol {
    var titleName: String {
        return self.userName ?? "- user name -"
    }
    var titleTitle: String {
        return self.userName ?? "- chat name -"
    }
    var titleInfo: String {
        return "последние слова!"
    }
    var isOnline: Bool {
        return true
    }
    var titlePhoto: Data? {
        if let base64 = self.photo, let data = base64.convertToImageData() {
            return data
        }
        return nil
    }
}
