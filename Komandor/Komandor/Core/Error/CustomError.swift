//
//  CustomError.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

enum CustomError: Error {
    case customError(error: String)
}
extension CustomError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .customError(error: let error):
            return NSLocalizedString(error, comment: "")
        }
    }
}
