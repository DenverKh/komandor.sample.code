//
//  CryptoError.swift
//  Komandor
//
//  Created by Denis Khlopin on 18/01/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

enum CryptoError: Error {
    case noContainersInstalled
}

extension CryptoError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .noContainersInstalled:
            return NSLocalizedString("Ни одного контейнера не установлено!", comment: "Установите хотя бы один контейнер!")
        }
    }
}
