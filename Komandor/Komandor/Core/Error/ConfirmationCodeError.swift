//
//  ConfirmationCodeError.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation

enum ConfirmationCodeError: Error {
    case wrongCode
}

extension ConfirmationCodeError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .wrongCode:
            return NSLocalizedString("Неверный код!", comment: "Введен неверный код")
        }
    }
}
