//
//  APIError.swift
//  Komandor
//
//  Created by Denis Khlopin on 12/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

enum APIError: String, Error {
    /// - Общая (неизвестная) ошибка
    case commonError = "CommonError"
    /// - Ошибка при получении данных
    case dataError = "DataError"
    /// - Метод не найден
    case methodNotFound = "MethodNotFound"
    /// - Ошибка аутентификации
    case authenticationFailed = "AuthenticationFailed"
    /// - Требуется аутентификация
    case needAuthentication = "NeedAuthentication"
    /// - Срок действия сессии истек
    case sessionExpired = "SessionExpired"
    /// - Срок действия сертификата истек
    case certificateExpired = "CertificateExpired"
    ///- Некорректный код подтверждения
    case incorrectCode = "IncorrectCode"
    /// - Требуется указать мобильный телефон
    case needRegisterPhone = "NeedRegisterPhone"
    /// - Телефон уже зарегистрирован
    case phoneAlreadyRegistered = "PhoneAlreadyRegistered"
    /// - Некорректный пароль
    case wrongPassword = "WrongPassword"
    /// - Некорректный контейнер
    case wrongContainer = "WrongContainer"
    /// - Ошибка проверки схемы запроса
    case invalidSchema = "InvalidSchema"
    /// - Недействительная подпись
    case invalidSignature = "InvalidSignature"
    /// - Некорректный или недействительный сертификат
    case invalidCertificate = "InvalidCertificate"
    /// - Некорректный ответ
    case invalidResponse = "InvalidResponse"
    /// - Ошибка простой подписи
    case simpleSign = "SimpleSign"
    /// - Ошибка крипто-модуля
    case cryptoError = "CryptoError"
    /// - Ошибка шифрования
    case encryptError = "EncryptError"
    /// - Ошибка дешифрования
    case decryptError = "DecryptError"
    /// - Ошибка сети
    case networkError = "NetworkError"
    /// - Ошибка отправки СМС
    case smsError = "SMSError"
    /// - Ошибка создания чата
    case createChatError = "CreateChatError"
    /// - Ошибка создания ключей чата
    case createChatKeysError = "CreateChatKeysError"
    /// - Ошибка получения чатов
    case getChatsError = "GetChatsError"
}

extension APIError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .commonError:
            return NSLocalizedString("Общая (неизвестная) ошибка", comment: "")
        case .dataError:
            return NSLocalizedString("Ошибка при получении данных", comment: "")
        case .methodNotFound:
            return NSLocalizedString("Метод не найден", comment: "")
        case .authenticationFailed:
            return NSLocalizedString("Ошибка аутентификации", comment: "")
        case .needAuthentication:
            return NSLocalizedString("Требуется аутентификация", comment: "")
        case .sessionExpired:
            return NSLocalizedString("Срок действия сессии истек", comment: "")
        case .certificateExpired:
            return NSLocalizedString("Срок действия сертификата истек", comment: "")
        case .incorrectCode:
            return NSLocalizedString("Некорректный код подтверждения", comment: "")
        case .needRegisterPhone:
            return NSLocalizedString("Требуется указать мобильный телефон", comment: "")
        case .phoneAlreadyRegistered:
            return NSLocalizedString("Телефон уже зарегистрирован", comment: "")
        case .wrongPassword:
            return NSLocalizedString("Некорректный пароль", comment: "")
        case .wrongContainer:
            return NSLocalizedString("Некорректный контейнер", comment: "")
        case .invalidSchema:
            return NSLocalizedString("Ошибка проверки схемы запроса", comment: "")
        case .invalidSignature:
            return NSLocalizedString("Недействительная подпись", comment: "")
        case .invalidCertificate:
            return NSLocalizedString("Некорректный или недействительный сертификат", comment: "")
        case .invalidResponse:
            return NSLocalizedString("Некорректный ответ", comment: "")
        case .simpleSign:
            return NSLocalizedString("Ошибка простой подписи", comment: "")
        case .cryptoError:
            return NSLocalizedString("Ошибка крипто-модуля", comment: "")
        case .encryptError:
            return NSLocalizedString("Ошибка шифрования", comment: "")
        case .decryptError:
            return NSLocalizedString("Ошибка дешифрования", comment: "")
        case .networkError:
            return NSLocalizedString("Ошибка сети", comment: "")
        case .smsError:
            return NSLocalizedString("Ошибка отправки СМС", comment: "")
        case .createChatError:
            return NSLocalizedString("Ошибка создания чата", comment: "")
        case .createChatKeysError:
            return NSLocalizedString("Ошибка создания ключей чата", comment: "")
        case .getChatsError:
            return NSLocalizedString("Ошибка получения чатов", comment: "")
        }
    }
}
