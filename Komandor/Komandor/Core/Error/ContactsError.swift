//
//  ContactsError.swift
//  Komandor
//
//  Created by Denis Khlopin on 20/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

enum ContactsError: Error {
    case denied
    case restricted
    case isRefreshing
}

extension ContactsError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .denied:
            return NSLocalizedString("Нет доступа к контактам", comment: "Нет доступа к контактам")
        case .restricted:
            return NSLocalizedString("Доступ к контактам запрещен!", comment: "Доступ к контактам запрещен!")
        case .isRefreshing:
            return NSLocalizedString("Контакты обновляются в данный момент!", comment: "Контакты обновляются в данный момент!")
        }
    }
}
