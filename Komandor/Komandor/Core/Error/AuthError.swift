//
//  AuthError.swift
//  Komandor
//
//  Created by Denis Khlopin on 26/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation

enum AuthError: Error {
    case wrongPassword
    case wrongCertificate(description: String)
}

extension AuthError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .wrongPassword:
            return NSLocalizedString("Неверный пароль!", comment: "Введен неверный пароль")
        case .wrongCertificate(let description):
            return NSLocalizedString("Ошибка сертификата! \(description)", comment: "Введен неверный пароль")
        }
    }
}
