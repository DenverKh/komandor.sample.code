//
//  DataUtils.swift
//  Komandor
//
//  Created by Denis Khlopin on 19/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import UIKit

class DataUtils {
    static func dataToImage(data: Data?, defaultImage: UIImage = #imageLiteral(resourceName: "user-icon") ) -> UIImage {
        if let data = data, let image = UIImage(data: data) {
            return image
        }
        return defaultImage
    }

}
