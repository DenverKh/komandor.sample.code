//
//  MaskUtils.swift
//  Komandor
//
//  Created by Denis Khlopin on 28/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
class MaskUtils {
    static func snils(_ snilsValue: String) -> String {
        var result = snilsValue.filter {
            "01234567890".contains($0)
        }
        if result.count > 11 {
            result = String(result.prefix(11))
        }
        if result.count > 3 {
            result.insert("-", at: result.index(result.startIndex, offsetBy: 3))
        }
        if result.count > 7 {
            result.insert("-", at: result.index(result.startIndex, offsetBy: 7))
        }
        if result.count > 11 {
            result.insert(" ", at: result.index(result.startIndex, offsetBy: 11))
        }
        return result
    }
    // update phone to mask or simple ciphers
    static func phone(_ inPhone: String) -> (mask: String, numbersOnly: String) {
        var result = inPhone.filter {
            "01234567890".contains($0)
        }
        if result.count > 11 {
            result = String(result.prefix(11))
        }
        // check first symbol
        if result.count > 0 {
            if result.prefix(1) != "7" {
                if result.prefix(1) == "8" {
                    result.replaceSubrange(result.startIndex..<result.index(result.startIndex, offsetBy: 1), with: "7")
                } else {
                    result = "7" + result
                }
            }
            result = "+" + result
            if result.count > 2 {
                result.insert(" ", at: result.index(result.startIndex, offsetBy: 2))
            }
            if result.count > 6 {
                result.insert(" ", at: result.index(result.startIndex, offsetBy: 6))
            }
            if result.count > 10 {
                result.insert("-", at: result.index(result.startIndex, offsetBy: 10))
            }
            if result.count > 13 {
                result.insert("-", at: result.index(result.startIndex, offsetBy: 13))
            }
        }
        return (mask: result, numbersOnly: result.filter { "01234567890".contains($0) })
    }
}
