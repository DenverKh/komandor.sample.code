//
//  Memory.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class Memory {
    static func test(testCode: () -> Void, count: Int = 1 ) {
        if count <= 0 {
            return
        }
        let memoryBefore = report()
        for _ in 0..<count {
            testCode()
        }
        let memoryEnd = report()
        let totalMemoryLeak = memoryEnd - memoryBefore
        let memoryLeakPerCycle = totalMemoryLeak/UInt64(count)
        print("total memory leak = \(totalMemoryLeak), per one cycle memory leak = \(memoryLeakPerCycle)")
    }
    static func report() -> UInt64 {
        var info = mach_task_basic_info()
        let matchTaskBasicInfoCount = MemoryLayout<mach_task_basic_info>.stride/MemoryLayout<natural_t>.stride
        var count = mach_msg_type_number_t(matchTaskBasicInfoCount)
        //
        let kerr: kern_return_t = withUnsafeMutablePointer(to: &info) {
            $0.withMemoryRebound(to: integer_t.self, capacity: matchTaskBasicInfoCount) {
                task_info(mach_task_self_,
                          task_flavor_t(MACH_TASK_BASIC_INFO),
                          $0,
                          &count)
            }
        }
        //
        if kerr == KERN_SUCCESS {
            print("Memory in use (in bytes): \(info.resident_size)")
            return info.resident_size
        } else {
            print("Error with task_info(): " +
                (String(cString: mach_error_string(kerr), encoding: String.Encoding.ascii) ?? "unknown error"))
        }
        return 0
    }
}
