//
//  SecureStorage.swift
//  Komandor
//
//  Created by Denis Khlopin on 21/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class SecureStorage {
    let service: String!
    let key: String!
    private var secureData: Data?
    var secureDataValue: Data? {
       return secureData
    }
    var secureStringValue: String? {
        if let secureData = self.secureData, let secureString = String(data: secureData, encoding: .utf8) {
            return secureString
        }
        return nil
    }
    init(service: String, key: String, secureData: Data? = nil) {
        self.service = service
        self.key = key
        self.secureData = secureData
    }
    init(service: String, key: String, secureString: String?) {
        self.service = service
        self.key = key
        self.secureData = nil
        if let secureString = secureString {
            self.secureData = secureString.data(using: .utf8)
        }
    }
    /// сохранение пароля в KeyChain
    func save() -> Bool {
        guard let data = secureData else {
            return false
        }
        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: key,
                                    kSecAttrService as String: service,
                                    kSecValueData as String: data]
        SecItemDelete(query as CFDictionary)
        let status = SecItemAdd(query as CFDictionary, nil)
        return status == errSecSuccess
    }
    /// выгрузка пароля из KeyChain
    func load() throws {
        self.secureData = nil
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: key,
            kSecAttrService as String: service,
            kSecReturnData as String: true,
            kSecMatchLimit as String: kSecMatchLimitOne
        ]
        var item: CFTypeRef?
        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &item)
        guard status != errSecItemNotFound else { throw CustomError.customError(error: "Данные не найдены в KeyChain") }
        guard status == errSecSuccess else { throw CustomError.customError(error: "Ошибка поиска данных в KeyChain с кодом: \(status)") }
        guard let secureData = item as? Data else {
            throw CustomError.customError(error: "Не верный формат пароля в KeyChain")
        }
        self.secureData = secureData
    }
    ///удаление текущего пароля
    func delete() -> Bool {
        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: key,
                                    kSecAttrService as String: service
                                    ]
        let status = SecItemDelete(query as CFDictionary)
        return status == errSecSuccess
    }
}

extension SecureStorage {
    static func setString(service: String, key: String, value: String) -> Bool {
        let storage = SecureStorage(service: service, key: key, secureString: value)
        return storage.save()
    }
    static func setData(service: String, key: String, value: Data) -> Bool {
        let storage = SecureStorage(service: service, key: key, secureData: value)
        return storage.save()
    }
    static func setValue<T>(service: String, key: String, value: T) -> Bool {
        let optData = withUnsafeBytes(of: value) { Data($0) }
        let storage = SecureStorage(service: service, key: key, secureData: optData)
        return storage.save()
    }
    static func string(service: String, key: String) -> String? {
        let storage = SecureStorage(service: service, key: key)
        do {
            try storage.load()
            return storage.secureStringValue
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    static func data(service: String, key: String) -> Data? {
        let storage = SecureStorage(service: service, key: key)
        do {
            try storage.load()
            return storage.secureDataValue
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    static func value<T>(service: String, key: String) -> T? {
        let storage = SecureStorage(service: service, key: key)
        do {
            try storage.load()
            return storage.secureDataValue?.withUnsafeBytes { $0.pointee }
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    static func delete(service: String, key: String) -> Bool {
        let storage = SecureStorage(service: service, key: key)
        return storage.delete()
    }
    /*
    extension Data {
        
        init<T>(from value: T) {
            self = Swift.withUnsafeBytes(of: value) { Data($0) }
        }
        
        func to<T>(type: T.Type) -> T {
            return self.withUnsafeBytes { $0.pointee }
        }
    }*/
}
