//
//  CertificatePasswordStorage.swift
//  Komandor
//
//  Created by Denis Khlopin on 20/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import Security
/// save and recieve password from KeyChayn storage
class PasswordStorage {
    let account: String!
    var password: String?
    init(account: String, password: String? = nil) {
        self.account = account
        self.password = password
    }
    /// сохранение пароля в KeyChain
    func save() -> Bool {
        guard let password = self.password, let data = password.data(using: .utf8) else {
            return false
        }
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword as String,
            kSecAttrAccount as String: account,
            kSecValueData as String: data
        ]
        SecItemDelete(query as CFDictionary)
        let status = SecItemAdd(query as CFDictionary, nil)
        return status == errSecSuccess
    }
    /// выгрузка пароля из KeyChain
    func load() throws {
        self.password = nil
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: account,
            kSecReturnData as String: true,
            kSecMatchLimit as String: kSecMatchLimitOne
        ]
        //var dataTypeRef: Unmanaged<AnyObject>?
        var item: CFTypeRef?
        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &item)
        guard status != errSecItemNotFound else { throw CustomError.customError(error: "Пароль не найден в KeyChain") }
        guard status == errSecSuccess else { throw CustomError.customError(error: "Ошибка поиска пароля в KeyChain с кодом: \(status)") }
        guard let passwordData = item as? Data, let password = String(data: passwordData, encoding: .utf8) else {
            throw CustomError.customError(error: "Не верный формат пароля в KeyChain")
        }
        self.password = password
    }
    ///удаление текущего пароля
    func delete() -> Bool {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: account
        ]
        let status = SecItemDelete(query as CFDictionary)
        return status == errSecSuccess
    }
    ///удаление всех паролей
    func clear() -> Bool {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword
        ]
        let status = SecItemDelete(query as CFDictionary)
        return status == errSecSuccess
    }
}
