//
//  APIClientProtocols.swift
//  Komandor
//
//  Created by Kostya Golenkov on 18/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
// Base API Client protocol
protocol APIClientProtocol {
    var baseApiUrlParameter: String { get }
    func baseApiUrl() -> URL
}
extension APIClientProtocol {
    func baseApiUrl() -> URL {
        guard let infoDictionary = Bundle.main.infoDictionary else {
            fatalError("Info.plist not found")
        }
        guard let baseUrlString = infoDictionary[baseApiUrlParameter] as? String else {
            fatalError("Parameter \(baseApiUrlParameter) not found!")
        }
        guard let url = URL(string: baseUrlString) else {
            fatalError("Invalid value of parameter \(baseApiUrlParameter)")
        }
        return url
    }
}
