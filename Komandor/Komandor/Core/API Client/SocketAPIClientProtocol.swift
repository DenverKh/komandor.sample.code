//
//  SocketAPIClientProtocol.swift
//  Komandor
//
//  Created by Kostya Golenkov on 18/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
// Socket API Client protocol
protocol SocketAPIClientProtocol: APIClientProtocol {
}
extension SocketAPIClientProtocol {
    var baseApiUrlParameter: String {
        return "base_api_url_socket"
    }
}
