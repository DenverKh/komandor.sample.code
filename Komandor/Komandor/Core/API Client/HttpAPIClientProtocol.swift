//
//  HttpAPIClientProtocol.swift
//  Komandor
//
//  Created by Kostya Golenkov on 18/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import Alamofire
// HTTP API Client protocol
typealias CompletionBlock = (Data?, Error?) -> Void
//
protocol HttpAPIClientProtocol: APIClientProtocol {
    //
    func fullUrl(path: String) -> URL
    //
    func sendDataRequest(method: HTTPMethod, path: String, data: Data?, headers: HTTPHeaders?, completion: @escaping CompletionBlock)
    func sendSecuredDataRequest(method: HTTPMethod, path: String, data: Data?, completion: @escaping CompletionBlock)
    //post
    func sendUnSecuredPostRequest(path: String, data: Data?, completion: @escaping CompletionBlock)
    func sendSecuredPostRequest(path: String, data: Data?, completion: @escaping CompletionBlock)
    //get
    func sendUnSecuredGetRequest(path: String, data: Data?, completion: @escaping CompletionBlock)
    func sendSecuredGetRequest(path: String, data: Data?, completion: @escaping CompletionBlock)
}
extension HttpAPIClientProtocol {
    var baseApiUrlParameter: String {
        return "base_api_url_http"
    }
    func fullUrl(path: String) -> URL {
        let baseUrl = self.baseApiUrl()
        return baseUrl.appendingPathComponent(path)
    }
    //
    func sendUnSecuredPostRequest(path: String, data: Data?, completion: @escaping CompletionBlock) {
        self.sendDataRequest(method: .post, path: path, data: data, headers: nil, completion: completion)
    }
    func sendSecuredPostRequest(path: String, data: Data?, completion: @escaping CompletionBlock) {
        self.sendSecuredDataRequest(method: .post, path: path, data: data, completion: completion)
    }
    //
    func sendUnSecuredGetRequest(path: String, data: Data?, completion: @escaping CompletionBlock) {
        self.sendDataRequest(method: .get, path: path, data: data, headers: nil, completion: completion)
    }
    func sendSecuredGetRequest(path: String, data: Data?, completion: @escaping CompletionBlock) {
        self.sendSecuredDataRequest(method: .get, path: path, data: data, completion: completion)
    }
}
