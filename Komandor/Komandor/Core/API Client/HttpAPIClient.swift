//
//  APIHTTPClient.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import Alamofire
//
class HttpAPIClient: HttpAPIClientProtocol {
    static let defaultClient = HttpAPIClient()
    fileprivate var sessionManager: SessionManager!
    fileprivate var queue: DispatchQueue?
    //
    private init() {
        sessionManager = SessionManager(configuration: .ephemeral)
        queue = DispatchQueue(label: "komandor.messager.http.queue.background", qos: .background)
    }
    //
    func sendDataRequest(method: HTTPMethod, path: String, data: Data?, headers: HTTPHeaders?, completion: @escaping CompletionBlock) {
        var request = URLRequest(url: self.fullUrl(path: path))
        request.httpMethod = method.rawValue
        request.httpBody = data
        if let headers = headers {
            for header in headers {
                request.setValue(header.value, forHTTPHeaderField: header.key)
            }
        }
        sessionManager.request(request).responseData(queue: queue) { (response) in
            completion(response.data, response.error)
        }
    }
    func sendSecuredDataRequest(method: HTTPMethod, path: String, data: Data?, completion: @escaping CompletionBlock) {
        //TODO реализовать логику заполнения хедеров с авторизацией и дальнещая передача в метод выше
        fatalError("Not implemented")
    }
}
