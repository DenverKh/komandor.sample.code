//
//  ChatManager.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class ChatManager {
    static let manager = ChatManager()
    //
    var chatLoaderService: ChatLoaderService?
    //
    fileprivate init() {
    }
    //
    func load(for certificate: Certificate) {
        if chatLoaderService == nil || chatLoaderService?.certificate.uniqueName == certificate.uniqueName {
            chatLoaderService = ChatLoaderService(certificate: certificate)
        }
        refresh()
    }
    func refresh() {
        if let chatLoaderService = self.chatLoaderService {
            chatLoaderService.run()
        }
    }
}
