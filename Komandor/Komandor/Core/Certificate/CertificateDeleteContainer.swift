//
//  CertificateDeleteContainer.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class CertificateDeleteContainer {
    var certificate: Certificate!
    init(certificate: Certificate) {
        self.certificate = certificate
    }
    func delete() -> Bool {
        let fileManager = FileManager.default
        guard
            let libraryPath = fileManager.urls(for: .libraryDirectory, in: .userDomainMask).first,
            let name = certificate.name else {
                return false
        }
        var mutableLibraryPath = libraryPath
        mutableLibraryPath.appendPathComponent("Caches/cprocsp/keys/mobile/")
        let paths = name.split(separator: "\\")
        if paths.count >= 3 {
            mutableLibraryPath.appendPathComponent(String(paths[1]))
            do {
                try fileManager.removeItem(at: mutableLibraryPath)
            } catch {
                print(error.localizedDescription)
                return false
            }
            return true
        }
        return false
    }
}
