//
//  CertificateToken.swift
//  Komandor
//
//  Created by Denis Khlopin on 28/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class CertificateToken {
    var certificate: Certificate!
    init(certificate: Certificate) {
        self.certificate = certificate
    }
    func setNewToken(token: String?) {
        guard let certificate = self.certificate else {
            fatalError("некорректный сертификат")
        }
        if let token = token {
            _ = SecureStorage.setString(service: "token", key: certificate.name, value: token)
        } else {
            _ = SecureStorage.delete(service: "token", key: certificate.name)
        }
    }
    func getToken() -> String? {
        guard let certificate = self.certificate else {
            fatalError("некорректный сертификат")
        }
        return SecureStorage.string(service: "token", key: certificate.name)
    }
}
