//
//  CertificatesManager.swift
//  Komandor
//
//  Created by Denis Khlopin on 21/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class CertificatesManager {
    static let manager = CertificatesManager()
    //private var _certificates = [Certificate]()
    fileprivate var _currentCertificate: Certificate?
    var crypto: Crypto?
    var currentCertificate: Certificate? {
        set {
            //_currentCertificate = newValue
            if let cert = newValue {
                UserDefaults.standard.set(cert.name, forKey: "current_certificate_name")
            } else {
                UserDefaults.standard.removeObject(forKey: "current_certificate_name")
            }
        }
        get {
            let certificateName = UserDefaults.standard.string(forKey: "current_certificate_name")
            for cert in certificates where cert.name == certificateName {
                return cert
            }
            // если объекта с таким именем нет в списке сертификатов, удаляем его из 
            UserDefaults.standard.removeObject(forKey: "current_certificate_name")
            return nil
            //return _currentCertificate
        }
    }
    var certificates: [Certificate] {
        if let crypto = self.crypto {
            return crypto.cryptoContainers
        }
        return []
    }
    var sertificatesObserver = BehaviorSubject<[Certificate]>(value: [])
    fileprivate init() {
        initialize()
    }
    func initialize() {
        // init manager here
        load()
    }
    func load() {
        crypto = Crypto()
        sertificatesObserver.onNext(certificates)
    }
    func reLoad() {
        if let crypto = self.crypto {
            crypto.updateContext()
            sertificatesObserver.onNext(certificates)
        }
    }
    func isAvailableCertificates() -> Bool {
        return !certificates.isEmpty
    }
    func delete(certificate: Certificate) {
        let deleteContainer = CertificateDeleteContainer(certificate: certificate)
        _ = deleteContainer.delete()
        reLoad()
    }
    func checkCurrentCertificateAuthed() -> Bool {
        if let currentCert = currentCertificate {
            let passwordService = CertificatePasswordService(certificate: currentCert)
            return passwordService.verifyPassword()
        }
        return false
    }
    func logOut() {
        if let currentCert = currentCertificate {
            let passwordService = CertificatePasswordService(certificate: currentCert)
            _ = passwordService.clearPin()
        }
        currentCertificate = nil
    }
}
