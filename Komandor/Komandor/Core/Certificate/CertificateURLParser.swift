//
//  CertURLParser.swift
//  Komandor
//
//  Created by Denis Khlopin on 20/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
/// класс получает из имени сертификата
class CertificateURLParser {
    let certificate: Certificate!
    init(certificate: Certificate) {
        self.certificate = certificate
    }
    func parse() -> URL? {
        let fileManager = FileManager.default
        guard
            let libraryPath = fileManager.urls(for: .libraryDirectory, in: .userDomainMask).first,
            let name = certificate.name else {
            return nil
        }
        var mutableLibraryPath = libraryPath
        mutableLibraryPath.appendPathComponent("Caches/cprocsp/keys/mobile/")
        let paths = name.split(separator: "\\")
        if paths.count >= 3 {
            mutableLibraryPath.appendPathComponent(String(paths[1]))
            return mutableLibraryPath
        }
        return nil
    }
}
