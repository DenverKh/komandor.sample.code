//
//  UserProfileLoadService.swift
//  Komandor
//
//  Created by Denis Khlopin on 28/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class UserProfileLoadService {
    var user: User!
    var userInfoObserver = BehaviorSubject<UserInfo?>(value: nil)
    var profileService: APIProfileService?
    init(user: User) {
        self.user = user
    }
    func load() {
        // TODO: сначала грузим локально
        loadFromContainer()
        guard let user = self.user, let certificate = user.certificate else {
            fatalError("У пользователя не действительный сертификат!")
        }
        guard let token = CertificateToken(certificate: certificate).getToken() else {
            fatalError("токен отсутствует")
        }
        let profileRequest = APIProfileRequest(token: token)
        profileService = APIProfileService(request: profileRequest, apiClient: HttpAPIClient.defaultClient)
        profileService?.run { [weak self] (response, error) in
            //DispatchQueue.main.async {
                if let error = error {
                    print(error.localizedDescription)
                    self?.userInfoObserver.onNext(nil)
                } else {
                    self?.copyInfo(from: response)
                }
           // }
        }
    }
    fileprivate func copyInfo(from response: APIProfileResponse?) {
        var result: UserInfo?
        if let response = response {
            var userInfo = UserInfo()
            userInfo.pid =  response.pid
            userInfo.cid = response.cid
            userInfo.type = response.type
            userInfo.name = response.name
            userInfo.company = response.company
            userInfo.title = response.title
            userInfo.inn = response.inn
            userInfo.ogrn = response.ogrn
            userInfo.snils = response.snils
            userInfo.phone = response.phone
            userInfo.photo = response.photo
            userInfo.email = response.email
            userInfo.billing = UserBillingInfo()
            userInfo.billing?.balance = response.billing.balance
            userInfo.certificates = response.certificates.map {
                return APICertificate(identifier: $0.identifier, cert: $0.cert, sync: $0.sync, pid: userInfo.pid)
            }
    //userInfo.certificates?.first?.pid = 0
            //userInfo.certificates = [UserCertificateInfo]()
            /*for cert in response.certificates {
                //var certInfo = UserCertificateInfo()
                var certInfo = APICertificate(identifier: cert.identifier, cert: cert.cert)
                certInfo.cert = cert.cert
                certInfo.identifier = cert.identifier
                userInfo.certificates?.append(certInfo)
            }*/
            result = userInfo
        }
        self.user.userInfo = result
        self.userInfoObserver.onNext(result)
    }
    // предварительно берем инфо из контейнера
    fileprivate func loadFromContainer() {
        guard let certificate = user.certificate else {
            fatalError("ошибка контейнера!")
        }
        var userInfo = UserInfo()
        userInfo.pid = 0
        userInfo.cid = 0
        userInfo.type = 0
        userInfo.name = certificate.fullName
        userInfo.company = certificate.companyName
        userInfo.title = ""
        userInfo.inn = certificate.inn
        userInfo.ogrn = certificate.ogrn
        userInfo.snils = certificate.snils
        userInfo.phone = ""
        if self.user.userInfo != nil {
            userInfo.photo = self.user.userInfo?.photo
        } else {
            userInfo.photo = ""
        }
        userInfo.email = certificate.email
        userInfo.billing = nil
        userInfo.certificates = nil
        self.user.userInfo = userInfo
        self.userInfoObserver.onNext(userInfo)
    }
}
