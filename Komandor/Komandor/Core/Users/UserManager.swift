//
//  UserManager.swift
//  Komandor
//
//  Created by Denis Khlopin on 26/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class UserManager {
    static let manager = UserManager()
    var currentUser: User?
    private init() {
        setup()
    }
    func setup() {
    }
    func authCurrentUser(certificate: Certificate) -> User {
        currentUser = createUser(withCertificate: certificate)
        return currentUser!
    }
    func logoutCurrentUser() {
        CertificatesManager.manager.logOut()
        currentUser = nil
    }
    func createUser(withCertificate certificate: Certificate) -> User? {
        let user = User()
        user.certificate = certificate
        user.userInfo = UserInfo()
        // TODO: загрузка полей из кэша
        // TODO: загрузка полей с сервера
        // сначала грузим поля с местного хранилища, затем делаем запрос на сервер и получаем профиль
        return user
    }
}
