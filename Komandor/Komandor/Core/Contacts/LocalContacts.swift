//
//  ContactLoaderService.swift
//  Komandor
//
//  Created by Denis Khlopin on 07/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import Contacts
import UIKit
/// загрузка локальных контактов телефона
class LocalContacts {
    fileprivate func withAccess(completion: @escaping (_ error: Error?, _ authorized: Bool) -> Void) {
        let status = CNContactStore.authorizationStatus(for: .contacts)
        switch status {
        case .authorized: // авторизован
            completion(nil, true)
        case .denied: // ограничен
            completion(ContactsError.denied, false)
        case .notDetermined: // при первом запуске
            let store = CNContactStore()
            //Запросить разрешение у пользователя на доступ к контактам
            store.requestAccess(for: .contacts) { (granted, error) in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    if granted {
                        completion(nil, true)
                    } else {
                        completion(ContactsError.denied, false)
                    }
                }
            }
        case .restricted: // запрещено!!!!
            completion(ContactsError.restricted, false)
        }
    }
    /// получение контактов с телефона
    func getPhoneContacts(completion: @escaping (Error?, [Contact]) -> Void) {
        withAccess { [weak self] (error, authorized) in
            if let error = error {
                completion(error, [])
            } else if authorized {
                // получаем контакты
                if let contacts = self?.getContacts()
                    // отсееваем номера без телефонов
                    .filter({ (contact) -> Bool in
                        return (contact.contactPhones.count != 0 && !(contact.contactName?.isEmpty ?? false))
                    })
                    .sorted(by: {$0.contactName! < $1.contactName!}) {
                    completion(nil, contacts)
                    return
                }
            }
            completion(nil, [])
        }
    }
    /// получение контактов
    fileprivate func getContacts() -> [Contact] {
        let store = CNContactStore()
        guard let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as? [CNKeyDescriptor]
            else {
                fatalError()
        }
        //
        var allContainers: [CNContainer] = []
        do {
            allContainers = try store.containers(matching: nil)
            var results: [CNContact] = []
            for container in allContainers {
                let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                let containerResults = try store.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch )
                results.append(contentsOf: containerResults)
            }
            let contacts = results.map({ (cnContact) -> Contact in
                return self.toContact(from: cnContact)
            })
            // удаление дупликатов ()
            return Array(Set(contacts))
        } catch {
            print(error.localizedDescription)
        }
        return []
    }
    /// конвертируем CNContact -> Contact
    fileprivate func toContact(from cnContact: CNContact) -> Contact {
        var contact = Contact()
        contact.contactType = .phone
        contact.contactName = cnContact.givenName
        if cnContact.imageDataAvailable, let data = cnContact.thumbnailImageData {
            contact.imageData = data
        }
        if !cnContact.familyName.isEmpty {
            if !contact.contactName!.isEmpty {
                contact.contactName = contact.contactName! + " "
            }
            contact.contactName = contact.contactName! + cnContact.familyName
        }
        for phone in cnContact.phoneNumbers {
            let mobilePhone = MaskUtils.phone(phone.value.stringValue).numbersOnly
            // добавляем только сотовые российские номера
            let regexpMobile = "79([0-9]{2})([0-9]{3})([0-9]{2})([0-9]{2})$"
            if mobilePhone.isMatches(regexp: regexpMobile) {
                contact.contactPhones.append(mobilePhone)
            }
        }
        return contact
    }
}
