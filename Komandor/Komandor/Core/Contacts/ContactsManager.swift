//
//  ContactsManager.swift
//  Komandor
//
//  Created by Denis Khlopin on 07/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class ContactsManager {
    /// singelton
    static let manager = ContactsManager()
    /// сервис загрузки и обновления контактов
    fileprivate var contactsLoaderService: ContactsLoaderService?
    /// проброс обсервера контактов
    var contactsObserver: BehaviorSubject<[Contact]?>? {
        if let service = self.contactsLoaderService {
            //return service.contactsObserver
            return service.dataObserver
        }
        return nil
    }
    var contactsErrorObserver: BehaviorSubject<Error?>? {
        if let service = self.contactsLoaderService {
            return service.errorObserver
        }
        return nil
    }
    var contactsRefreshObserver: BehaviorSubject<Bool>? {
        if let service = self.contactsLoaderService {
            return service.refreshObserver
        }
        return nil

    }

    /// private constructor
    fileprivate init() {
    }
    func loadContacts(for certificate: Certificate) {
        if contactsLoaderService == nil || contactsLoaderService?.certificate.uniqueName != certificate.uniqueName {
            contactsLoaderService = ContactsLoaderService(certificate: certificate)
        }
        contactsLoaderService?.run()
    }
    func refreshContacts() {
        contactsLoaderService?.run()
    }
}
