//
//  ContactsLoaderService.swift
//  Komandor
//
//  Created by Denis Khlopin on 12/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import RxSwift

/// сервис загрузки и синхронизации контактов
/// 1) сначала загружаем локальные контакты
/// 2) Затем берем сотовые номера телефонов из локальных контактов
/// -> делаем запрос на сервис для получения информации по зарегисрированным номерам
/// 3)
class ContactsLoaderService: RefreshDataProtocol, DataObserverProtocol, ErrorObserverProtocol {
    var dataObserver = BehaviorSubject<[Contact]?>(value: [])
    // in variables
    var certificate: Certificate!
    // rx observer для контактов
    var errorObserver = BehaviorSubject<Error?>(value: nil)
    var refreshObserver = BehaviorSubject<Bool>(value: false)
    // сервисы
    var contactsService: APIContactsService?
    var localContactsLoader: LocalContacts?
    init(certificate: Certificate) {
        self.certificate = certificate
    }
    func run() {
        // начинаем обновление
        startRefresh()
        // TODO: загрузить контакты из кэша и вызвать обсервер
        localContactsLoader = LocalContacts()
        localContactsLoader?.getPhoneContacts { [weak self] (error, localContacts) in
            if self == nil {
                return
            }
            if let error = error {
                self?.setError(error)
                self?.finishRefresh()
                print(error.localizedDescription)
            } else {

                // получаем мобильные номера и готовим запрос
                let mobilePhones = self?.getMobilePnoneList(contacts: localContacts)
                // получаем токен
                guard let certificate = self?.certificate, let token = CertificateToken(certificate: certificate).getToken() else {
                    fatalError("token не доступен!!!")
                }
                // делаем запрос на сервер
                let contactsRequest = APIContactsRequest(token: token, phones: mobilePhones)
                self?.contactsService = APIContactsService(request: contactsRequest, apiClient: HttpAPIClient.defaultClient)
                self?.contactsService?.run(completion: { [weak self] (response, error) in
                    if let error = error {
                        // если ошибка сервера, то кидаем только локальные контакты
                        print(error.localizedDescription)
                        self?.setData(value: localContacts)
                        self?.setError(error)
                        self?.finishRefresh()
                    } else {
                        if let response = response {
                            // получаем список зарегистрированных в системе контакстов
                            // сводим контакты и зарегистрированный список
                            if let contacts = self?.union(localContacts: localContacts, serverResponseContacts: response) {
                                self?.setData(value: contacts)
                                // TODO: тут нужно сохранить контакты в кэш
                            }
                        }
                        self?.finishRefresh()
                    }
                })
            }//error
        }
    }
    /// объединение контактов
    fileprivate func union(localContacts: [Contact], serverResponseContacts: [APIContactsResponse] ) -> [Contact] {
        var contacts = localContacts
        var serverContacts = [Contact]()
        var serverPhones = [String]()
        for serverContact in serverResponseContacts {
            if let phone = serverContact.phone,
                let index = getIndexByPhone(localContacts: contacts, phone: phone) {
                var contact = contacts[index]
                serverPhones.append(phone)
                contact.contactType = .both
                contact.phone = serverContact.phone
                contact.chatName = serverContact.chatName
                contact.chatId = serverContact.chatId
                contact.chatType = serverContact.chatType
                contact.type = serverContact.type
                contact.name = serverContact.name
                contact.photo = serverContact.photo
                contact.pid = serverContact.pid
                contact.title = serverContact.title
                contact.uid = serverContact.uid
                contact.userName = serverContact.user
                contact.key =  serverContact.key
                contact.certificates = serverContact.certificates
                serverContacts.append(contact)
            }
        }
        // удаляем лишние локальные контакты
        let serverPhonesSet = Set(serverPhones)
        contacts = contacts.filter({ (contact) -> Bool in
            for contactPhone in contact.contactPhones {
                if serverPhonesSet.contains(contactPhone) {
                    return false
                }
            }
            return true
        })
        // объединяем локальные и серверные контакты
        if !serverContacts.isEmpty {
            contacts.insert(contentsOf: serverContacts.sorted(by: {$0.name! < $1.name!}), at: 0)
        }
        return contacts
    }
    /// получение первого подходящего индекса по номеру телефона
    fileprivate func getIndexByPhone(localContacts: [Contact], phone: String ) -> Int? {
        //получение первого подходящего индекса по номеру телефона
        //for contact in localContacts where contact.contactPhones.contains(phone) {
        for index in 0..<localContacts.count {
            let contact = localContacts[index]
            if contact.contactPhones.contains(phone) {
                return index
            }
        }
        return nil
    }
    /// из списка контактов возвращает массив всех мобильных телефоном, удаляя дубликаты
    fileprivate func getMobilePnoneList(contacts: [Contact]) -> [String] {
        var phones = [String]()
        for contact in contacts {
            for mobilePnone in contact.contactPhones {
                phones.append(mobilePnone)
            }
        }
        ///прогоняем через регехп
        let regexp = "7([0-9]{3})([0-9]{3})([0-9]{2})([0-9]{2})$"
        phones = phones.filter { $0.isMatches(regexp: regexp) }
        return Array(Set(phones))
    }
}
