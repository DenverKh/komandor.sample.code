//
//  ChatLoaderService.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import RxSwift
// загрузка списка чатов
class ChatLoaderService: RefreshDataProtocol, DataObserverProtocol, ErrorObserverProtocol {
    // протоколы rx
    var dataObserver = BehaviorSubject<[Chat]?>(value: [])
    var refreshObserver = BehaviorSubject<Bool>(value: false)
    var errorObserver = BehaviorSubject<Error?>(value: nil)
    //
    var certificate: Certificate!
    var chatService: APIContactsService?
//
    init(certificate: Certificate) {
        self.certificate = certificate
    }
    func run() {
        guard let token = CertificateToken(certificate: certificate).getToken() else {
            fatalError("Token not exists")
        }

        if isRefreshing {
            return
        }
        startRefresh()
        let chatRequest = APIContactsRequest(token: token, phones: [])
        chatService = APIContactsService(request: chatRequest, apiClient: HttpAPIClient.defaultClient)
        chatService?.run(completion: { [weak self] (response, error) in
            if let error = error {
                self?.setError(error)
                self?.setData(value: [])
            } else {
                if let contacts = response {
                    let chats = contacts.map({ (contact) -> Chat in
                        var chat = Chat()
                        chat.phone = contact.phone
                        chat.chatName = contact.chatName
                        chat.chatId = contact.chatId
                        chat.chatType = contact.chatType
                        chat.type = contact.type
                        chat.name = contact.name
                        chat.photo = contact.photo
                        chat.pid = contact.pid
                        chat.title = contact.title
                        chat.uid = contact.uid
                        chat.userName = contact.user
                        chat.key =  contact.key
                        chat.certificates = contact.certificates
                        // TODO: заполняем инфо о чате с сервера
                        return chat
                    })
                    self?.setData(value: chats)
                }
            }
            self?.finishRefresh()
        })
    }
}
