//
//  ChatCreatorService.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import RxSwift
/// проверяет наличие чата с контактом и создает его при отсутствии
class ChatCreatorService: DataObserverProtocol, RefreshDataProtocol, ErrorObserverProtocol {
    var errorObserver = BehaviorSubject<Error?>(value: nil)
    var refreshObserver = BehaviorSubject<Bool>(value: false)
    var dataObserver = BehaviorSubject<String?>(value: nil) // chatId
    //
    var userCertificatesObserver = PublishSubject<APICertificatesResponse?>()
    var certificate: Certificate!
    var contact: Contact!
    var user: User!
    fileprivate var certificatesService: APICertificatesService?
    fileprivate var createChatService: APICreateChatService?
    fileprivate var certificateKeyService: CertificateKeyService?
    //
    init(contact: Contact) {
        //получаем текущего юзера
        guard let localUser = UserManager.manager.currentUser else {
            fatalError("user not found")
        }
        self.user = localUser
        self.certificate = user.certificate
        self.contact = contact
    }
    func run() {
        if let chatId = contact.chatId, !chatId.isEmpty {
            setData(value: chatId)
            // TODO: удалить потом
            // удаление чата
            deleteChat(chatId: chatId)
            return
        }
        guard let token = CertificateToken(certificate: certificate).getToken() else {
            fatalError("token not exists")
        }
        startRefresh()
        var certificateList = [APICertificate]()
        guard var userCertificates = user.userInfo?.certificates, var contactCertificates = contact.certificates else {
            fatalError("certificates error!")
        }
        //update pid for users and profiles
        for index in 0..<userCertificates.count {
            userCertificates[index].pid = user.userInfo?.pid
        }
        for index in 0..<contactCertificates.count {
            contactCertificates[index].pid = contact.pid
        }
        certificateList.append(contentsOf: userCertificates)
        certificateList.append(contentsOf: contactCertificates)
        let keyService = CertificateKeyService(certificate: certificate)
        guard let sessionKey = keyService.createNewKey() else {
            finishRefresh()
            return
        }
        let apiKeys = certificateList.map { (apiCertificate) -> APIChatKey? in
            if let cert = apiCertificate.cert, let pid = apiCertificate.pid, let key = keyService.exportKeyEphem(key: sessionKey, certBase64: cert) {
                let apiKey = APIChatKey(cid: apiCertificate.identifier, pid: pid, key: key)
                return apiKey
            }
            return nil
        }.filter {$0 != nil}.map {$0!}
        let request = APICreateChatRequest(token: token, type: 0, name: "тест", keys: apiKeys)
        createChatService = APICreateChatService(request: request, apiClient: HttpAPIClient.defaultClient)
        //
        createChatService?.run(completion: { [weak self] (response, error) in
            if let error = error {
                self?.setError(error)
            } else {
                if let response = response {
                    self?.setData(value: response.chatId)
                }
            }
            self?.finishRefresh()
        })
    }
    //для тестов, удаление чата по ид
    func deleteChat(chatId: String) {
        guard let token = CertificateToken(certificate: certificate).getToken() else {
            fatalError("token not exists")
        }
        //
        let deleteChatRequest = APIDeleteChatRequest(token: token, chat: chatId)
        let deleteService = APIDeleteChatService(request: deleteChatRequest, apiClient: HttpAPIClient.defaultClient)
        deleteService.run { (response, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                print(response ?? "")
            }
        }
    }
    // временно не будет использоваться
    // загрузка сертификатов пользователя
    fileprivate func loadUserCertificates() {
        guard let token = CertificateToken(certificate: certificate).getToken() else {
            fatalError("token not exists")
        }
        guard let pid = user.userInfo?.pid else {
            fatalError("wrong user pid")
        }
        //обновим сертификаты
        let certificatesRequest = APICertificatesRequest(token: token, pid: pid)
        certificatesService = APICertificatesService(request: certificatesRequest, apiClient: HttpAPIClient.defaultClient)
        certificatesService?.run(completion: { [weak self] (response, error) in
            if let error = error {
                self?.setError(error)
                self?.finishRefresh()
            } else {
                if let response = response {
                    self?.userCertificatesObserver.onNext(response)
                }
            }
        })
    }
}
