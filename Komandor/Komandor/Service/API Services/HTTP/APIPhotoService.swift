//
//  APIPhotoService.swift
//  Komandor
//
//  Created by Denis Khlopin on 22/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class APIPhotoService: APIService {
    var request: APIPhotoRequest?
    var apiPath: String = "/api/photo/"
    var apiClient: HttpAPIClientProtocol?
    typealias RequestType = APIPhotoRequest
    typealias ResponseType = APIPhotoResponse
    required init() {
    }
}
