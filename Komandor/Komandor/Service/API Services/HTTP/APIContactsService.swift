//
//  APIContactsService.swift
//  Komandor
//
//  Created by Denis Khlopin on 05/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class APIContactsService: APIService {
    var request: APIContactsRequest?
    var apiPath: String = "/api/contacts/"
    var apiClient: HttpAPIClientProtocol?
    typealias RequestType = APIContactsRequest
    typealias ResponseType = [APIContactsResponse]
    required init() {
    }
}

/*
 // test service
 guard let token = CertificateToken(certificate: certificate!).getToken() else {
 fatalError()
 }
 let request = APIContactsRequest(token: token, phones: ["79174070966"])
 let contactsService = APIContactsService(request: request, apiClient: HttpAPIClient.defaultClient)
 contactsService.run { (response, error) in
 if let error = error {
 print(error.localizedDescription)
 } else {
 print(response)
 }
 }
 */
