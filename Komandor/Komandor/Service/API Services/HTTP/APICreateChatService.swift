//
//  APICreateChat.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class APICreateChatService: APIService {
    var request: APICreateChatRequest?
    var apiPath: String = "/api/createChat/"
    var apiClient: HttpAPIClientProtocol?
    typealias RequestType = APICreateChatRequest
    typealias ResponseType = APICreateChatResponse
    required init() {
    }
}
