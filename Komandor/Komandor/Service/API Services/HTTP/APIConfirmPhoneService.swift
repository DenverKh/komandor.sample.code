//
//  APIConfirmPhoneService.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//
import Foundation
///  API запрос на проверку сертификата
class APIConfirmPhoneService: APIService {
    var apiClient: HttpAPIClientProtocol?
    typealias RequestType = APIConfirmPhoneReguest
    typealias ResponseType = APIConfirmPhoneResponse
    var apiPath: String = "/api/confirmPhone/"
    var request: APIConfirmPhoneReguest?
    required init() {
    }
}
