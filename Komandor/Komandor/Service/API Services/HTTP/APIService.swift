//
//  APIService.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
//
protocol APIService: class {
    associatedtype RequestType: Codable
    associatedtype ResponseType: Codable
    var request: RequestType? { get set }
    var apiPath: String { get set }
    var apiClient: HttpAPIClientProtocol? { get set }
    //func run(completion: @escaping (ResponseType?, Error?) -> Void)
    init()
    //init(request: RequestType)
}

extension APIService {
    func getResponseType() -> ResponseType.Type {
        return ResponseType.self
    }
    func getRequestType() -> RequestType.Type {
        return RequestType.self
    }
    /// default constructor
    init(request req: RequestType, apiClient client: HttpAPIClientProtocol) {
        self.init()
        request = req
        apiClient = client
    }
    /// запуск запроса
    func run(completion: @escaping (ResponseType?, Error?) -> Void) {
        guard let apiClient = self.apiClient else {
            completion(nil, CustomError.customError(error: "Неверная инициализация параметров запроса"))
            return
        }
        let encoder = JSONEncoder()
        var jsonData: Data?
        do {
            jsonData = try encoder.encode(request)
        } catch {
            print(error.localizedDescription)
            return
        }
        let headers = ["Content-Type": "application/json; charset=UTF-8"]
        apiClient.sendDataRequest(method: .post, path: self.apiPath, data: jsonData, headers: headers) { (responseData, error) in
            let decoder = JSONDecoder()
            do {
                if let error = error {
                    print(error.localizedDescription)
                    completion(nil, error)
                } else {
                    if let responseData = responseData {
                        let response = try decoder.decode(APIResponse<ResponseType>.self, from: responseData)
                        guard let success = response.success else {
                            completion(nil, CustomError.customError(error: "Неверный формат ответа от сервера"))
                            return
                        }
                        if success { // запрос выполнен успешно, данные получены
                            /*guard let data = response.data else {
                                completion(nil, CustomError.customError(error: "Отсутствуют дополнительные данные"))
                                return
                            }*/
                            completion(response.data, nil)
                        } else { // получена серверная ошибка APIError
                            guard let error = response.error, let apiError = APIError(rawValue: error) else {
                                completion(nil, CustomError.customError(error: "Неизвестная ошибка сервера!"))
                                return
                            }
                            completion(nil, apiError)
                        }
                    } else {
                        completion(nil, CustomError.customError(error: "Невозможно получить ответ от сервера!"))
                    }
                }
            } catch {
                print(error.localizedDescription)
                completion(nil, error)
            }
        }
    }
}
