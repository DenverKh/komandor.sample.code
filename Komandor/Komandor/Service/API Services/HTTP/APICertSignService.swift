//
//  APICertSignCervice.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
///  API запрос на проверку сертификата
class APICertSignService: APIService {
    var apiClient: HttpAPIClientProtocol?
    typealias RequestType = APICertSignRequest
    typealias ResponseType = APICertSignResponse
    var apiPath: String = "/api/auth/"
    var request: APICertSignRequest?
    required init() {
    }
}
