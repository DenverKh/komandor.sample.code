//
//  APIAuthService.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
import RxSwift

/// сервис авторизациии на сервере
/// авторизация происходит в 4 этапа
/// 1 этап) отправляется запрос на сервер где указывается сертификат и подпись сертификата
/// -> если запрос верный, то в ответе мы получаем токен и строку,
/// 2 этап) эту строку нужно подписать и отправить на сервер вторым этапом
/// если все ОК, получаем значение, зарегистрирован телефон для данного сертификата или нет
/// если телефон не зарегистрирован
/// 3 этап) отправляем запрос на регисрацию телефона -> получаем id запроса
/// 4 этап) отправляем код из телефона и id запроса
/// если телефон зарегистрирован переходим в состояние авторизован
class APIAuthService {
    let certificate: Certificate!
    var certSignService: APICertSignService?
    var authConfirmService: APIAuthConfirmService?
    var registerPhoneService: APIRegisterPhoneService?
    var confirmPhoneService: APIConfirmPhoneService?
    init(certificate: Certificate) {
        self.certificate = certificate
    }
    /// step 1
    func auth(completion: @escaping (_ Error: Error?, _ token: String?, _ needToRegisterPhone: Bool?) -> Void) {
        var token: String?
        // получаем сертификат закодированные в строку base64
        guard let certEncodedDataString = certificate.certEncodedData?.base64EncodedString() else {
            completion( CustomError.customError(error: "Сертификат имеет не верный формат!"), nil, nil)
            return
        }
        let simpleSignCertificateService = CertificateSimpleSignService(certificate: certificate, base64String: certEncodedDataString)
        // подписываем сертификат
        let signBase64 = simpleSignCertificateService.sign()
        let certSignRequest = APICertSignRequest(cert: certEncodedDataString, sign: signBase64)
        certSignService = APICertSignService(request: certSignRequest, apiClient: HttpAPIClient.defaultClient)
        certSignService!.run { [unowned self] (response, error) in
            if let error = error {
                completion(error, nil, nil)
            } else {
                if let response = response {
                    token = response.token
                    // запускаем второй этап
                    let simpleSignService = CertificateSimpleSignService(certificate: self.certificate, base64String: response.nonce)
                    let sign = simpleSignService.sign()
                    let authConfirmRequest = APIAuthConfirmRequest(token: token!, sign: sign)
                    self.authConfirmService = APIAuthConfirmService(request: authConfirmRequest, apiClient: HttpAPIClient.defaultClient)
                    self.authConfirmService!.run {(response, error) in
                        if let error = error {
                            completion(error, nil, nil)
                        } else {
                            if let response = response {
                                // если требуется регистрация телефона
                                completion(nil, token, response.needRegisterPhone)
                            }
                        }
                    }
                }
            }
        }
    }
    // регистрация телефона
    func registerPhone(phone: String, token: String, completion: @escaping (_ error: Error?, _ phoneIdentifier: Int?) -> Void) {
        // register phone request
        let registerPhoneRequest = APIRegisterPhoneRequest(token: token, phone: phone)
        registerPhoneService = APIRegisterPhoneService(request: registerPhoneRequest, apiClient: HttpAPIClient.defaultClient)
        registerPhoneService!.run { (response, error) in
            if let error = error {
                completion(error, nil)
            } else {
                if let response = response {
                    completion(nil, response.identifier)
                }
            }
        }
    }
    // подтверждение кода регистрации
    func confirmPhone(code: String, phoneIdentifier: Int, token: String, completion: @escaping (_ error: Error?) -> Void) {
        // confirm code phone request
        let confirmPhoneReguest = APIConfirmPhoneReguest(token: token, identifier: phoneIdentifier, code: code)
        confirmPhoneService = APIConfirmPhoneService(request: confirmPhoneReguest, apiClient: HttpAPIClient.defaultClient)
        confirmPhoneService!.run { (_, error) in
            completion(error)
        }
    }
}
