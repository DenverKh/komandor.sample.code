//
//  APIProfileService.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class APIProfileService: APIService {
    var request: APIProfileRequest?
    var apiPath: String = "/api/profile/"
    var apiClient: HttpAPIClientProtocol?
    typealias RequestType = APIProfileRequest
    typealias ResponseType = APIProfileResponse
    required init() {
    }
}
