//
//  APIAuthConfirmService.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//
import Foundation
///  API запрос на проверку сертификата
class APIAuthConfirmService: APIService {
    var apiClient: HttpAPIClientProtocol?
    typealias RequestType = APIAuthConfirmRequest
    typealias ResponseType = APIAuthConfirmResponse
    var apiPath: String = "/api/confirmAuth/"
    var request: APIAuthConfirmRequest?
    required init() {
    }
}
