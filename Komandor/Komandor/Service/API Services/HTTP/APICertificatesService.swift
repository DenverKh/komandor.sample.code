//
//  APICertificatesService.swift
//  Komandor
//
//  Created by Denis Khlopin on 28/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class APICertificatesService: APIService {
    var request: APICertificatesRequest?
    var apiPath: String = "/api/certificates/"
    var apiClient: HttpAPIClientProtocol?
    typealias RequestType = APICertificatesRequest
    typealias ResponseType = APICertificatesResponse
    required init() {
    }
}
