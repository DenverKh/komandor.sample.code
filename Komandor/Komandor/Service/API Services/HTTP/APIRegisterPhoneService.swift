//
//  APIRegisterPhoneService.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//
import Foundation
///  API запрос на проверку сертификата
class APIRegisterPhoneService: APIService {
    var apiClient: HttpAPIClientProtocol?
    typealias RequestType = APIRegisterPhoneRequest
    typealias ResponseType = APIRegisterPhoneResponse
    var apiPath: String = "/api/registryPhone/"
    var request: APIRegisterPhoneRequest?
    required init() {
    }
}
