//
//  APIDeleteChatService.swift
//  Komandor
//
//  Created by Denis Khlopin on 01/04/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class APIDeleteChatService: APIService {
    var request: APIDeleteChatRequest?
    var apiPath: String = "/api/deleteChatToId/"
    var apiClient: HttpAPIClientProtocol?
    typealias RequestType = APIDeleteChatRequest
    typealias ResponseType = APIDeleteChatResponse
    required init() {
    }
}
