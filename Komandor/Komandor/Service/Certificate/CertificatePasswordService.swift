//
//  CertificatePasswordService.swift
//  Komandor
//
//  Created by Denis Khlopin on 20/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
/// сервис ввода / хранения пароля для сертификата
class CertificatePasswordService {
    let certificate: Certificate!
    fileprivate var password: String = ""
    init(certificate: Certificate) {
        self.certificate = certificate
        // пытаемся получить сохраненный пароль из хранилища (Связка Ключей)
        if let password = SecureStorage.string(service: "password", key: certificate.name) {
            if !verifyPassword(password: password) {
                // если проверка не проходит, сбрасываем пароль и удаляем
                _ = SecureStorage.delete(service: "password", key: certificate.name)
                self.password = ""
            }
            self.password = password
        }
    }
    /// установка и проверка правильности ПИН кода
    func verifyPassword(password: String) -> Bool {
        self.password = password
        if password.count > 0 {
            if setPin() {
                if verifyPassword() {
                    // удаляем все предыдущие записи
                    _ = SecureStorage.delete(service: "password", key: certificate.name)
                    _ = SecureStorage.setString(service: "password", key: certificate.name, value: self.password)
                    return true
                }
            }
        } else {
            // если вводим в качестве пароля "", то сбрасываем ПИН код
            _ = clearPin()
        }
        return false
    }
    /// проверка, true - если ранее введен правильный ПИН
    /// false - или ПИН неправильный или ПИН не был введен
    func verifyPassword() -> Bool {
        var result = false
        certificate.name.withCString { (pName) in
            result = cppCryptoCheckPin(pName)
        }
        return result
    }
    /// установка пароля в контейнер для проверки!
    fileprivate func setPin() -> Bool {
        var result = false
        certificate.name.withCString { (pName) in
            self.password.withCString { (pPassword) in
                result = cppCryptoSetPin(pName, pPassword)
            }
        }
        return result
    }
    ///сброс пароля контейнера
    func clearPin() -> Bool {
        var result = false
        certificate.name.withCString { (pName) in
            result = cppCryptoSetPin(pName, nil)
        }
        return result
    }
    // получение текущего пина
    func getCurrentPassword() -> String {
        return password
    }
}
