//
//  CertificateSignService.swift
//  Komandor
//
//  Created by Denis Khlopin on 13/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

class CertificateSimpleSignService {
    let certificate: Certificate!
    let base64String: String!
    init(certificate: Certificate, base64String: String) {
        self.certificate = certificate
        self.base64String = base64String
    }
    var signature: String = ""
    var hash: String?
    var errorCode: UInt32 = 0
    /// простая подпись сертификатом
    func sign() -> String {
        guard let oidAlgoSignature = certificate.oidAlgoSignature else {
            print("не определен алгоритм шифрования!")
            return ""
        }
        certificate.name.withCString { (pName) in
            base64String.withCString { pBase64String in
                oidAlgoSignature.withCString { pAlgoSignature in
                    let simpleSign = cppCryptoSimpleSign(pName, nil, pAlgoSignature, pBase64String)
                    self.errorCode = simpleSign.errorCode
                    if simpleSign.errorCode == 0 {
                        let signPointer = UnsafeMutablePointer<CChar>.allocate(capacity: Int(simpleSign.b64SignSize))
                        signPointer.moveAssign(from: simpleSign.b64Sign, count: simpleSign.b64SignSize)
                        let dataSign = Data(buffer: UnsafeBufferPointer<Int8>.init(
                            start: UnsafePointer<Int8>(simpleSign.b64Sign),
                            count: simpleSign.b64SignSize))
                        let dataHash = Data(buffer: UnsafeBufferPointer<Int8>.init(
                            start: UnsafePointer<Int8>(simpleSign.b64Hash),
                            count: simpleSign.b64HashSize))
                        self.signature = String(data: dataSign, encoding: .utf8)!
                        self.hash = String(data: dataHash, encoding: .utf8)!
                    }
                    cppCryptoFreeSimpleSign(simpleSign)
                }
            }
        }
        return self.signature
    }
    /// проверка простой подписи
    func verify(b64Data: String, b65Sign: String) -> Bool {
        guard   let encodedCertificate = certificate.certEncodedData?.base64EncodedString(),
            let oidAlgoSignature = certificate.oidAlgoSignature
            else { return false}
        let result = cppCryptoVerifySimpleSign(UnsafePointer<Int8>.init(encodedCertificate),
                                               UnsafePointer<Int8>.init(oidAlgoSignature),
                                               UnsafePointer<Int8>.init(b64Data),
                                               UnsafePointer<Int8>.init(b65Sign))
        return result
    }
}
