//
//  CertificateLoadContainerService.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/02/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation
/// загрузка данных из сертификата
class CertificateLoadContainerService {
    var certificate: Certificate!
    init(certificate: Certificate) {
        self.certificate = certificate
    }
    ///  загрузка данных сертификата из контейнера
    final func load() {
        loadDataFromContainer()
        loadKeys()
    }
    /// получение данных о контейнере
    final func loadDataFromContainer() {
        //certificate.urlContainer = (CertificateURLParser(certificate: self)).parse()
        if let name = certificate.name, let pName = (name as NSString).utf8String {
            let buffer: UnsafeMutablePointer<Int8> = UnsafeMutablePointer<Int8>.init(mutating: pName)
            certificate.containerContextHandle = cppCryptoGetContext(buffer)
            // получаем уникальное имя контейнера!
            certificate.uniqueName = getContainerParam(param: .ppUniqueContainer, containerEnum: .none).param
        }
    }
    /// получение данных по ключу контейнера(сертификата)
    final func loadKeys() {
        // если контейнер инициализирован, получаем ключи
        if certificate.containerContext > 0 {
            if certificate.isExchangeKey {
                certificate.keyHandle = cppCryptoGetUserKeyExchange(certificate.containerContext)
            } else {
                // ключ публичного ключа (почему то одинаковый с exchange!!!!)
                certificate.keyHandle = cppCryptoGetUserKeySignature(certificate.containerContext)
            }
            // получаем данные сертификата
            if let pCertificateContext = cppCryptoCreateCertificateContextFromKey(certificate.keyHandle),
                let pCertInfo = pCertificateContext.pointee.pCertInfo {
                // нераскодированный сертификат
                let certificateContext = pCertificateContext.pointee
                print("размер сертификата: \(certificateContext.cbCertEncoded)")
                certificate.certEncodedSize = certificateContext.cbCertEncoded
                certificate.certEncoded = UnsafeMutablePointer<BYTE>.allocate(capacity: Int(certificateContext.cbCertEncoded))
                certificate.certEncoded?.assign(
                    from: UnsafePointer<BYTE>(certificateContext.pbCertEncoded),
                    count: Int(certificateContext.cbCertEncoded))
                certificate.certEncodedData = Data(
                    buffer: UnsafeMutableBufferPointer<BYTE>(
                        start: certificate.certEncoded,
                        count: Int(certificateContext.cbCertEncoded)))
                //"
                let certInfo = pCertInfo.pointee
                certificate.version = certInfo.dwVersion
                certificate.serialNumber = Data(bytes: certInfo.SerialNumber.pbData, count: Int(certInfo.SerialNumber.cbData))
                certificate.oidAlgoSignature = String(cString: certInfo.SignatureAlgorithm.pszObjId)
                let dblStartDate = timeToDouble(certInfo.NotBefore)
                let dblEndDate = timeToDouble(certInfo.NotAfter)
                certificate.startDate = Date(timeIntervalSince1970: dblStartDate)
                certificate.endDate = Date(timeIntervalSince1970: dblEndDate)
                // free memory
                cppCryptoFreeCertificateContext(pCertificateContext)
            }
            loadIssuerInfo()
            loadSubjectInfo()
            parseSubject()
            test()
        }
    }
    /// получение структуры данных об органе выдавшем сертификат
    fileprivate func loadIssuerInfo() {
        // получаем параметры органа, выдавшего сертификат
        let value = getBlobParam(param: .cdIssuer)
        for str in value.split(separator: "\r\n") {
            let result = OIDDictionary.parse(from: String(str))
            if result.oid != .unknown {
                certificate.issuerValues[result.oid] = result.value
            }
        }
    }
    /// получение структуры данных о владельце
    fileprivate func loadSubjectInfo() {
        // получаем параметры владельца сертификата
        let value = getBlobParam(param: .cdSubject)
        for str in value.split(separator: "\r\n") {
            let result = OIDDictionary.parse(from: String(str))
            if result.oid != .unknown {
                certificate.subjectValues[result.oid] = result.value
            }
        }
    }
    fileprivate func parseSubject() {
        // parse FIO 2.5.4.3
        if let fio = certificate.subjectValues[.commonName], fio.count > 0 {
            var temp = fio
            if let secondName = temp.split(separator: " ").first, secondName.count > 0 {
                certificate.secondName = String(secondName)
                temp = String(temp.suffix(temp.count - secondName.count - 1))
                if let firstName = temp.split(separator: " ").first, firstName.count > 0 {
                    certificate.firstName = String(firstName)
                    certificate.middleName = String(temp.suffix(temp.count - firstName.count - 1))
                }
            }
        }
        // определяем тип сертификата
        //"INN=7710862988\\KPP=771001001\\OGRN=1107746111164"
        if let unstructured = certificate.subjectValues[.unstructured] {
            certificate.type = .personal
            let splitUnstructuredData = unstructured.split(separator: "\\")
            var unstructuredDict = [String: String]()
            for unstructedItem in splitUnstructuredData {
                let dictItem = unstructedItem.split(separator: "=")
                if dictItem.count > 1 {
                    unstructuredDict[String(dictItem[0]).uppercased()] = String(dictItem[1])
                }
            }
            if let post = certificate.subjectValues[.post], !post.isEmpty {
                if nil != unstructuredDict["OGRN"] ?? unstructuredDict["ОГРН"] {
                    certificate.type = .company
                } else if nil != unstructuredDict["KPP"] ?? unstructuredDict["КПП"] {
                    certificate.type = .individual
                }
            }
        }
    }
    // получение параметра для контейнера
    func getContainerParam(param: ContainerParam, containerEnum: ContainerEnum = .none ) -> (param: String, errorCode: UInt32) {
        var paramInfo = (param: "", errorCode: UInt32(0))
        let size = cppCryptoContainerGetSize(certificate.containerContext, param.rawValue, containerEnum.rawValue)
        if size == 0 {
            return paramInfo
        }
        let buffer: UnsafeMutablePointer<BYTE> = UnsafeMutablePointer<BYTE>.allocate(capacity: Int(size))
        let result = cppCryptoContainerGetParam(certificate.containerContext, param.rawValue, containerEnum.rawValue, size, buffer)
        paramInfo.param = String(cString: buffer)
        paramInfo.errorCode = result
        buffer.deallocate()
        return paramInfo
    }
    // получение параметра для контейнера
    func getKeyParam(param: KeyParam) -> KeyParamResult {
        var paramInfo = KeyParamResult(paramStr: "", paramInt: UInt32(0), errorCode: UInt32(0), size: UInt32(0))
        let size = cppCryptoUserKeyGetSize(certificate.keyHandle, param.rawValue, 0)
        paramInfo.size = size
        if size == 0 {
            /// если празмер равен 0, возвращаем пустые значения
            return paramInfo
        }
        /// выделяем память для буфера размером size
        let buffer: UnsafeMutablePointer<BYTE> = UnsafeMutablePointer<BYTE>.allocate(capacity: Int(size))
        let result = cppCryptoUserKeyGetParam(certificate.keyHandle, param.rawValue, 0, size, buffer)
        // копируем из буфера в строку
        paramInfo.paramStr = String(cString: buffer)
        paramInfo.errorCode = result
        // приводим буфер к типу DWORD
        let uint32Pointer = UnsafeMutableRawPointer(buffer).bindMemory(to: UInt32.self, capacity: 1)
        paramInfo.paramInt = uint32Pointer.pointee
        /// освобождаем память буфера
        buffer.deallocate()
        return paramInfo
    }
    func getBlobParam(param: BlobParam) -> String {
        var result = ""
        let size = cppCryptoGetCertificateBlobInfo(certificate.keyHandle, nil, param.rawValue)
        if size > 0 {
            let buffer: UnsafeMutablePointer<Int8> = UnsafeMutablePointer<Int8>.allocate(capacity: Int(size))
            cppCryptoGetCertificateBlobInfo(certificate.keyHandle, buffer, param.rawValue)
            result = String(cString: buffer)
            buffer.deallocate()
        }
        return result
    }
    // TODO: remove test function
    // test func
    func test() {
        /*
        if let onekey =  CertificateKeyService.createNewKey() {
            print(onekey)
            let service = CertificateKeyService(certificate: certificate)
            let base64 = certificate.getEncodedCertificateBase64()
            //for _ in 0..<1 {
                if let sessionKey = service.exportKey(key: onekey, certBase64: base64) {
                    if let key = service.importKey(sessionKey: sessionKey, certBase64: base64) {
                        print("sessionKey = \(sessionKey)")
                        print("getting key  = \(key)")
                        if let encrypted = service.encrypt(key: onekey, string: "") {
                            print("encrypted: \(encrypted)")
                            if let decrypted = service.decrypt(key: onekey, encryptedBase64: encrypted) {
                                print("decrypted: \(decrypted)")
                            }
                        }
                    }
                }
            //}
        }
        //test()
         */
    }
}

struct KeyParamResult {
    var paramStr: String
    var paramInt: UInt32
    var errorCode: UInt32
    var size: UInt32
}
