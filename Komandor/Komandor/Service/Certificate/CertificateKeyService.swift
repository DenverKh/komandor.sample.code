//
//  CertificateKeyService.swift
//  Komandor
//
//  Created by Denis Khlopin on 25/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import Foundation

// сервис работы с ключами
class CertificateKeyService {
    var certificate: Certificate!
    //
    init(certificate: Certificate) {
        self.certificate = certificate
    }
    // static
    /// создание нового ключ сессии
    func createNewKey() -> String? {
        if let cCharString = cppCreateProvKey() {
            let key = String(cString: cCharString)
            cCharString.deallocate()
            return key
        }
        return nil
    }
    /// удаление из памяти ключа ()
    static func deleteKey(key: String) {
        key.withCString { cCharString in
            freeProvKeyId(cCharString)
        }
    }
    // functions
    func exportKey(key: String, certBase64: String) -> String? {
        var result: String?
        let pinService = CertificatePasswordService(certificate: certificate)
        let pin = pinService.getCurrentPassword()
        if let certificate = self.certificate, let name = certificate.name {
            key.withCString { keyId in
                name.withCString { containerName in
                    pin.withCString { pinCode in
                        certBase64.withCString { cert in
                            // получаем структуру
                            if let resultDataPointer = cppExportSessionKey(keyId, containerName, pinCode, cert) {
                            //if let resultDataPointer = cppExportSessionKeyEphem(keyId, cert) {
                                let data = resultDataPointer.pointee
                                result = String(cString: data.sessionKey)
                                // освобождаем лишнюю память
                                freeSessionKeyData(resultDataPointer.pointee)
                                resultDataPointer.deallocate()
                            }
                        }
                    }
                }
            }
        }
        return result
    }
    func importKey(sessionKey: String, certBase64: String) -> String? {
        var result: String?
        let pinService = CertificatePasswordService(certificate: certificate)
        let pin = pinService.getCurrentPassword()
        if let certificate = self.certificate, let name = certificate.name {
            sessionKey.withCString { sessionKey in
                name.withCString { containerName in
                    pin.withCString { pinCode in
                        certBase64.withCString { cert in
                            // получаем структуру
                            //if let resultDataPointer = ExportSessionKey(keyId, containerName, pinCode, cert) {
                            if let key = cppImportSessionKey(containerName, pinCode, sessionKey, cert) {
                                result = String(cString: key)
                                key.deallocate()
                            }
                        }
                    }
                }
            }
        }
        return result
    }
    // работа с эфемерными ключами
    ///
    func exportKeyEphem(key: String, certBase64: String) -> String? {
        var result: String?
        key.withCString { keyId in
            certBase64.withCString { cert in
                // получаем структуру
                if let resultDataPointer = cppExportSessionKeyEphem(keyId, cert) {
                    let data = resultDataPointer.pointee
                    result = String(cString: data.sessionKey)
                    // освобождаем лишнюю память
                    freeSessionKeyData(resultDataPointer.pointee)
                    resultDataPointer.deallocate()
                }
            }
        }
        return result
    }
    func importKeyEphem(sessionKey: String/*, certBase64: String*/) -> String? {
        var result: String?
        let pinService = CertificatePasswordService(certificate: certificate)
        let pin = pinService.getCurrentPassword()
        if let certificate = self.certificate, let name = certificate.name {
            sessionKey.withCString { sessionKey in
                name.withCString { containerName in
                    pin.withCString { pinCode in
                        //certBase64.withCString { cert in
                            // получаем структуру
                            if let key = cppImportSessionKeyEphem(containerName, pinCode, sessionKey) {
                                result = String(cString: key)
                                key.deallocate()
                            }
                        //}
                    }
                }
            }
        }
        return result
    }
    // шифрование
    //
    func encrypt(key: String, string: String) -> String? {
        guard let base64String = string.data(using: .utf8)?.base64EncodedString() else {
            return nil
        }
        var result: String?
        key.withCString { keyId in
            base64String.withCString { base64 in
                if let encryptedString = cppEncrypt(keyId, base64) {
                    result = String(cString: encryptedString)
                    encryptedString.deallocate()
                }
            }
        }
        return result
    }
    //
    func decrypt(key: String, encryptedBase64: String) -> String? {
        var result: String?
        key.withCString { keyId in
            encryptedBase64.withCString { base64 in
                if let string64 = cppDecrypt(keyId, base64) {
                    let result64 = String(cString: string64)
                    if let data = Data(base64Encoded: result64), let string = String(data: data, encoding: .utf8) {
                        result = string
                    }
                    string64.deallocate()
                }
            }
        }
        return result
    }
}
