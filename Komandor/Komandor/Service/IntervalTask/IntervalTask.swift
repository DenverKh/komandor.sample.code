//
//  IntervalTask.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation
import RxSwift

class IntervalTask {
    let name: String!
    var interval: Double!
    private var timer: Timer?
    private var startTimerDate: Date?
    var runCode: ((IntervalTask) -> Bool)?
    var timerObserver = PublishSubject<Double>()
    var availableObserver = BehaviorSubject<Bool>(value: true)
    ///
    init(name: String, interval: Double, runCode: @escaping(IntervalTask) -> Bool) {
        self.name = name
        self.interval = interval
        self.runCode = runCode
    }
    ///
    func isAvailable() -> Bool {
        guard self.timer != nil else {
            return true
        }
        do {
            return try availableObserver.value()
        } catch {
            return true
        }
    }
    ///
    func execute() -> Bool {
        if !isAvailable() {
            return false
        }
        //start timer
        if let timer = self.timer {
            timer.invalidate()
            self.timer = nil
        }
        self.availableObserver.onNext(false)
        self.startTimerDate = Date()
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
            let elapsedInterval = Date().timeIntervalSince(self.startTimerDate!)
            let left = self.interval - elapsedInterval
            self.timerObserver.onNext(left)
            if left <= 0 {
                timer.invalidate()
                self.timer = nil
                self.availableObserver.onNext(true)
            }
        })
        //start task
        if let runCode = self.runCode {
            _ = runCode (self)
            // maybe add some logic
            return true
        } else {
            return false
        }
    }
    // отмена текущей задачи, сброс таймера
    func cancel() {
        if let timer = self.timer {
            timer.invalidate()
            self.timer = nil
            self.availableObserver.onNext(true)
        }
    }
}
