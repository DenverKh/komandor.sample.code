//
//  IntervalTaskManager.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/12/2018.
//  Copyright © 2018 Komandor. All rights reserved.
//

import Foundation

class IntervalTaskManager {
    static let manager = IntervalTaskManager()
    fileprivate init() {
        initialize()
    }
    // variables
    var tasks = [String: IntervalTask]()
    // init here
    final func initialize() {
    }
    //
    func getTask(withName name: String, interval: Double, runCode: @escaping(IntervalTask) -> Bool) -> IntervalTask? {
        if name.isEmpty {
            return nil
        }
        if tasks.keys.contains(name) {
            tasks[name]?.runCode = runCode
            return tasks[name]
        }
        let task = IntervalTask(name: name, interval: interval, runCode: runCode)
        tasks[name] = task
        return task
    }
    //
    func getTask(withName name: String) -> IntervalTask? {
        if name.isEmpty {
            return nil
        }
        if tasks.keys.contains(name) {
            return tasks[name]
        }
        return nil
    }
}
