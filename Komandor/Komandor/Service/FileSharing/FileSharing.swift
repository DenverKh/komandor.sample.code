//
//  FileSharing.swift
//  
//
//  Created by Denis Khlopin on 14/01/2019.
//  Copyright © 2019 Denis Khlopin. All rights reserved.
//

import UIKit
import Zip

class FileSharing {
    static let keyFilesCount = 6
    fileprivate let parentController: UIViewController!
    init(parentController: UIViewController) {
        self.parentController = parentController
    }
    // MARK: Можно сделать вывод ошибки на экран!
    /// показ ошибки
    fileprivate func showError(_ items: Any...) {
        print(items)
    }
    /// установка контейнера сертификата из выбранных файлов .key
    func installFromURLs(urls: [URL], containerName: String, completion: @escaping () -> Void) -> Bool {
        if urls.count != FileSharing.keyFilesCount {
            showAlert(  title: "Количество файлов не верное!",
                        message: "Количество выбранных файлов должно быть равно \(FileSharing.keyFilesCount)",
                        actions: [
                            UIAlertAction(title: "Закрыть", style: .cancel, handler: { (_) in
                    })
                ])
            return false
        }
        let fileManager = FileManager.default
        // копируем файлы в защищенную папку
        // /private/var/root/Library/Caches/cprocsp/keys/UID
        guard let libraryPath = fileManager.urls(for: .libraryDirectory, in: .userDomainMask).first else {
            showError("Library folder does not exists!")
            return false
        }
        // генерируем путь к контейнеру
        //let generatedContainerName = NSUUID().uuidString
        //let generatedContainerName = "UCgSOUZg"
        var mutableLibraryPath = libraryPath
        mutableLibraryPath.appendPathComponent("Caches/cprocsp/keys/mobile/")
        mutableLibraryPath.appendPathComponent(containerName)
        // проверяем наличие уже установленного сертификата с таким именем
        var isDirectory = ObjCBool(false)
        if fileManager.fileExists(atPath: mutableLibraryPath.path, isDirectory: &isDirectory) {
            if let url = urls.first?.deletingLastPathComponent() {
                containerExists(mutableLibraryPath: mutableLibraryPath, url: url) {
                    completion()
                }
            }
            return true
        }
        do {
            // создаем папку для нового контейнера
            try fileManager.createDirectory(at: mutableLibraryPath, withIntermediateDirectories: true, attributes: nil)
            // пробегаемся по файлам и копируем их в папку
            for fileUrl in urls {
                try fileManager.copyItem(at: fileUrl, to: mutableLibraryPath.appendingPathComponent(fileUrl.lastPathComponent))
            }
            completion()
        } catch {
            showError(error.localizedDescription)
            completion()
            return false
        }
        return true
    }
    /// установка контейнера сертификата из шаринга iTunes
    func deleteContainers() {
        let fileManager = FileManager.default
        guard let libraryPath = fileManager.urls(for: .libraryDirectory, in: .userDomainMask).first else {
            return
        }
        var mutableLibraryPath = libraryPath
        mutableLibraryPath.appendPathComponent("Caches/cprocsp/keys/mobile/")
        do {
            if fileManager.fileExists(atPath: mutableLibraryPath.path) {
                try fileManager.removeItem(at: mutableLibraryPath)
                print("certificates was removed successfully")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    /// установка контейнера сертификата из шаринга iTunes
    /// если через iTunes был залит контейнер, то устанавливаем его и возвращаем true
    func installFromITunes(completion: @escaping () -> Void) -> Bool {
        let fileManager = FileManager.default
        guard let documentsPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            showError("documents folder does not exists!")
            return false
        }
        do {
            let folderURLs = try fileManager.contentsOfDirectory(at: documentsPath, includingPropertiesForKeys: nil)
            for url in folderURLs where isThreeNumericExtention(url: url)/*url.pathExtension.prefix(2) == "00"*/ {
                let keysURLs = try fileManager.contentsOfDirectory(at: url, includingPropertiesForKeys: nil)
                // проверка на количество файлов
                var count = 0
                for keyFile in keysURLs where keyFile.pathExtension == "key" {
                    count += 1
                }
                if count == FileSharing.keyFilesCount {
                    // проверяем количество .key файлов, их должно быть ровно 6
                    showError("Найден контейнер с именем \(url.lastPathComponent)! Импортировать его?")
                    showAlert(  title: "Новый контейнер!",
                                message: "Обнаружен новый контейнер с именем \(url.lastPathComponent)! Хотите импортировать его?",
                                actions: [
                            // MARK: действие Импортировать
                            UIAlertAction(title: "Импортировать!", style: .default, handler: { [weak self] (_) in
                                print(self ?? "is dead")
                                self?.importFromITunes(fromURL: url) {
                                    completion()
                                }
                            }),
                            // MARK: действие Отменить
                            UIAlertAction(title: "Отменить", style: .cancel, handler: { (_) in
                                completion()
                            }),
                            // MARK: действие Удалить!
                            UIAlertAction(title: "Удалить!", style: .destructive, handler: { [weak self] (_) in
                                // удалить
                                do {
                                    try fileManager.removeItem(at: url)
                                } catch {
                                    self?.showError(error.localizedDescription)
                                }
                                completion()
                            })
                        ])
                    return true
                } else {
                    showAlert(  title: "Найден контейнер!",
                                message: "Найден контейнер \(url.lastPathComponent)! Но количество файлов внутри не равно 6! Проверьте правильность котейнера!",
                                actions: [
                            // MARK: действие "Закрыть"
                            UIAlertAction(title: "Закрыть", style: .cancel, handler: { (_) in
                            })
                        ])
                    showError("в контейнере должно быть 6 файлов с расширением .key!")
                    completion()
                }
            }
        } catch {
            showError(error.localizedDescription)
            completion()
        }
        return false
    }
    /// импорт контейнера из папки с документами с урлом в защищенную папку контейнера
    fileprivate func importFromITunes(fromURL url: URL, completion: @escaping () -> Void) {
        let fileManager = FileManager.default
        do {
            // копируем контейнер в защищенную папку
            // /private/var/root/Library/Caches/cprocsp/keys/
            guard let libraryPath = fileManager.urls(for: .libraryDirectory, in: .userDomainMask).first else {
                showError("Library folder does not exists!")
                return
            }
            var mutableLibraryPath = libraryPath
            mutableLibraryPath.appendPathComponent("Caches/cprocsp/keys/mobile/")
            // проверяем существует ли директория с ключами?
            var isDirectory = ObjCBool(false)
            if !fileManager.fileExists(atPath: mutableLibraryPath.path, isDirectory: &isDirectory) {
                // если нет -> создаем, хотя фреймворк сам должен создать
                try fileManager.createDirectory(at: mutableLibraryPath, withIntermediateDirectories: true, attributes: nil)
            } else {
                // если это не директория а файл -> удаляем и создаем директорию (мало ли)
                if !isDirectory.boolValue {
                    try fileManager.removeItem(at: mutableLibraryPath)
                    try fileManager.createDirectory(at: mutableLibraryPath, withIntermediateDirectories: true, attributes: nil)
                }
            }
            // копируем файлы в директорию,
            // создаем новый путь для контейнера
            mutableLibraryPath.appendPathComponent(url.lastPathComponent)
            // если вдруг такой контейнер уже есть, задаем вопрос пользователю
            if fileManager.fileExists(atPath: mutableLibraryPath.path, isDirectory: &isDirectory) {
                containerExists(mutableLibraryPath: mutableLibraryPath, url: url) {
                    completion()
                }
            } else {
                try fileManager.moveItem(at: url, to: mutableLibraryPath)
                completion()
            }
        } catch {
            showError(error.localizedDescription)
            completion()
        }
    }
    /// запускается, если контейнер существует,
    /// задает вопрос, заменить, дублировать или удалить!
    func containerExists(mutableLibraryPath: URL, url: URL, completion: @escaping () -> Void) {
        let fileManager = FileManager.default
        var isDirectory = ObjCBool(false)
        showAlert(  title: "Контейнер уже существует!",
                    message: "Контейнер с именем \(mutableLibraryPath.lastPathComponent) уже существует! Заменить его?",
            actions: [
                // MARK: действие ЗАМЕНИТЬ
                UIAlertAction(title: "Заменить", style: .default, handler: { [weak self] (_) in
                    do {
                        try fileManager.removeItem(at: mutableLibraryPath)
                        try fileManager.moveItem(at: url, to: mutableLibraryPath)
                        completion()
                    } catch {
                        self?.showError(error.localizedDescription)
                    }
                }),
                // MARK: действие Дублировать
                UIAlertAction(title: "Дублировать", style: .default, handler: { [weak self] (_) in
                    // Дублирование каталога
                    let tempUrl = mutableLibraryPath.deletingPathExtension()
                    let extention = mutableLibraryPath.pathExtension
                    var counterExtention: Int = Int(extention) ?? 0
                    while true {
                        counterExtention += 1
                        let newUrl = tempUrl.appendingPathExtension(String(format: "%03d", counterExtention))
                        if !fileManager.fileExists(atPath: newUrl.path, isDirectory: &isDirectory) {
                            do {
                                try fileManager.moveItem(at: url, to: newUrl)
                                completion()
                            } catch {
                                self?.showError(error.localizedDescription)
                            }
                            break
                        }
                    }
                }),
                // MARK: действие Отменить
                UIAlertAction(title: "Отменить", style: .cancel, handler: { (_) in
                    completion()
                }),
                // MARK: действие "Отменить и удалить контейнер"
                UIAlertAction(title: "Отменить и удалить контейнер", style: .destructive, handler: { [weak self] (_) in
                    do {
                        try fileManager.removeItem(at: url)
                        completion()
                    } catch {
                        self?.showError(error.localizedDescription)
                    }
                })
            ])
    }
    /// вывод сообщения алерта на экран
    func showAlert(title: String, message: String, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actions {
            alertController.addAction(action)
        }
        parentController.present(alertController, animated: true) {}
    }
    //zip
    func installFromZipUrl(zipUrl: URL, completion: @escaping () -> Void) {
        // установка контейнера из архива!
        let fileManager = FileManager.default
        let zipFileName = zipUrl.deletingPathExtension().lastPathComponent
        var keyFiles: [URL]?
        do {
            let tempDirectory = fileManager.temporaryDirectory.appendingPathComponent(zipFileName)
            try Zip.unzipFile(zipUrl, destination: tempDirectory, overwrite: true, password: nil, progress: { (progress) in
                print(progress)
            }, fileOutputHandler: { (finishUrl) in
                print(finishUrl)
            })
            print("finished")
            // ищем поддиректорию в архиве с расширенеем 00X
            var tempDirMutable = tempDirectory
            while true {
                let urls = try fileManager.contentsOfDirectory(at: tempDirMutable, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                if let subKeyFolder = (urls.filter { isThreeNumericExtention(url: $0) }.first) {
                    tempDirMutable = subKeyFolder
                    continue
                } else {
                    keyFiles = urls.filter { $0.pathExtension == "key" }
                    break
                }
            }
            // check key files
            if let keyFiles = keyFiles, keyFiles.count == FileSharing.keyFilesCount {
                let containerFolder = tempDirMutable.lastPathComponent
                _ = installFromURLs(urls: keyFiles, containerName: containerFolder) {
                    completion()
                }
            } else {
                print("неверный формат архива")
                completion()
            }
        } catch {
            print(error.localizedDescription)
            completion()
        }
    }
    /// проверка директории на расширение .000 - .999
    func isThreeNumericExtention(url: URL) -> Bool {
        let extention = url.pathExtension
        if extention.count != 3 {
            return false
        }
        if nil == Int(extention) {
            return false
        }
        return true
    }
}
