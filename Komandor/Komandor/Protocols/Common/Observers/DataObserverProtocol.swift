//
//  DataObserverProtocol.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import RxSwift

protocol DataObserverProtocol {
    associatedtype ObserverDataType
    var dataObserver: BehaviorSubject<ObserverDataType?> { get }
}

extension DataObserverProtocol {
    var data: ObserverDataType? {
        do {
            if let data = try dataObserver.value() {
                return data
            }
            return nil
        } catch {
            return nil
        }
    }
    func setData(value: ObserverDataType?) {
        dataObserver.onNext(value)
    }
}
