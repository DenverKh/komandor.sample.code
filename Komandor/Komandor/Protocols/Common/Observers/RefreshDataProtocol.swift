//
//  RefreshDataProtocol.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import RxSwift
// протокол обновления данных
protocol RefreshDataProtocol {
    var refreshObserver: BehaviorSubject<Bool> { get }
}
extension RefreshDataProtocol {
    var isRefreshing: Bool {
        do {
            return try refreshObserver.value()
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    func startRefresh() {
        refreshObserver.onNext(true)
    }
    func finishRefresh() {
        refreshObserver.onNext(false)
    }

}
