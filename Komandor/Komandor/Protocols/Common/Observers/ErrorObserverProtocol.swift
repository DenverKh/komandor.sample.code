//
//  ErrorObserverProtocol.swift
//  Komandor
//
//  Created by Denis Khlopin on 27/03/2019.
//  Copyright © 2019 Komandor. All rights reserved.
//

import RxSwift

protocol ErrorObserverProtocol {
    var errorObserver: BehaviorSubject<Error?> { get }
}

extension ErrorObserverProtocol {
    var error: Error? {
        do {
            if let data = try errorObserver.value() {
                return data
            }
            return nil
        } catch {
            return nil
        }
    }
    func setError(_ error: Error?) {
        errorObserver.onNext(error)
    }
}
